// Copyright (c) 2013-2014 Alexandre Grigorovitch (alexezh@gmail.com).
//
// Permission is hereby granted, free of charge, to any person obtaining a
// copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit
// persons to whom the Software is furnished to do so, subject to the
// following conditions:
//
// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
// OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN
// NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
// USE OR OTHER DEALINGS IN THE SOFTWARE.
#pragma once

#pragma pack(push, 1)

enum class OpCode : char
{
    Undefined,
    Nope,
    Stop, // stop the program
    Jump,
    FunctionProlog,
    Call,
    Return,
    AllocObject,
    AllocTuple,
    GetTupleField,
    GetTupleFields,
    Store,
    InitObject,
    StoreObject,
    ReleaseObject,
    Br,  // branch t/f
    Cmp, // compare 2 values
    Not,
    AddInt32,
    SubInt32,
    MulInt32,
    IncInt32,
    DecInt32,
    AddDouble,
    SubDouble,
    DivDouble,
    Lor,
    Land,
    ReadEventBoolean,
    ReadEventInt32,
    ReadEventInt64,
    ReadEventDouble,
    ReadEventString,
    Send,
    GetKey,
    AddKey,
    RemoveKey,
    SetKey,
};

static_assert(sizeof(OpCode) == 1, "OpCode must be 8 bit size");

enum class CmpCode : char
{
    Equal,
    NotEqual,
    Greater,
    GreaterEqual,
    Less,
    LessEqual,
    Max = LessEqual
};

static_assert(static_cast<int>(CmpCode::Max) < 16, "cmp code should contain less than 16 codes");
static_assert(sizeof(CmpCode) == 1, "If condition must be 8 bit size");

enum class DataType : char
{
    Undefined = 0,
    Void = Undefined,
    Boolean = 1,
    Int32 = 2,
    Int64 = 3,
    Double = 4,
    Object = 5,
    String = Object,
    Tuple = 6,
    // internal types
    List = 8,
    Map = 8,
    KeyStore = 9,
    Max = KeyStore
};

static_assert(static_cast<int>(DataType::Max) < 16, "data type should contain less than 16 types");
static_assert(sizeof(DataType) == 1, "data type must be 8 bit size");

// offset for a program
// who needs programs bigger than 65K :)
typedef uint16_t OpOffset;
typedef uint16_t OpAddress;

enum class DataAddressType : uint16_t
{
    Undefined = 0,
    Accumulator = 1,
    StackBase = 2,
    Constant = 3,
    Global = 4,
};


// represents data address for operations
// for constand and global the actual data is located after operation
// while Offset represents an offset within data
struct DataAddress
{
    union
    {
        struct
        {
			DataAddressType Mode : 4;
            uint16_t Offset : 12;
        } Addr;
        uint16_t Value;
    } u;
};

inline DataAddress MakeStackBaseAddress(uint16_t offset)
{
	DataAddress da;
	
	da.u.Addr.Mode = DataAddressType::StackBase;
    da.u.Addr.Offset = offset;
    return da;
}

inline DataAddress MakeDataAddress(DataAddressType type, uint16_t offset)
{
    DataAddress da;
    da.u.Addr.Mode = type;
    da.u.Addr.Offset = offset;
    return da;
}

inline DataAddress MakeDataAddress(DataAddressType type, int offset)
{
    if(offset < 0 || offset > UINT16_MAX)
        throw std::runtime_error("Offset value is too big");
    
    DataAddress da;
    da.u.Addr.Mode = type;
    da.u.Addr.Offset = static_cast<uint16_t>(offset);
    return da;
}

static_assert(sizeof(DataAddress) == 2, "data address must be 16 bit size");

// Value types
struct Handle
{
    union
    {
        struct
        {
            int16_t Bucket;
            int16_t Index;
        };
        int32_t Value;
    } u;
};

static_assert(sizeof(Handle) == 4, "handle must be 16 bit size");

// TypeId identifies types within TypeStore
typedef uint16_t TypeId;

TypeId FromDataType(DataType t) { return static_cast<TypeId>(t); }

// VM represents all data as Value object
struct Value
{
    union
    {
        int64_t None;
        bool Bool;
        int32_t Int32;
        int64_t Int64;
        double Double;
        Handle Handle;
    } u;
};

inline Value MakeValue()
{
    Value val;
    val.u.None = 0;
    return val;
}

inline Value MakeValue(bool v)
{
    Value val;
    val.u.Bool = v;
    return val;
}

inline Value MakeValue(int32_t v)
{
    Value val;
    val.u.Int32 = v;
    return val;
}

inline Value MakeValue(int64_t v)
{
    Value val;
    val.u.Int64 = v;
    return val;
}

inline Value MakeValue(double v)
{
    Value val;
    val.u.Double = v;
    return val;
}

inline Value MakeValue(Handle v)
{
    Value val;
    val.u.Handle = v;
    return val;
}


static_assert(sizeof(Value) == 8, "Value must be 16 bit size");

// value with type. used for external calls
struct TypedValue
{
    Value v;
    TypeId t;
};

// Op represents instruction for VM
struct Op
{
    union
    {
        struct
        {
            union
            {
                struct
                {
                    OpAddress Address;
                } Jump;
                struct
                {
                    uint16_t cVars;
                } FunctionProlog;
                
                // call expects cParam parameters pushed on stack
                struct
                {
                    OpAddress Address;
                    DataAddress Param1;
                    uint8_t cParams; // 255 parameters max
                } Call;
                struct
                {
                    DataAddress Result;
                } Return;
                struct
                {
                    DataAddress Source;
                    DataAddress Destination;
                } Store;
                struct
                {
                    DataAddress Operand;
                    OpOffset Else;
                } Br;
                struct
                {
                    OpOffset Offset;
                } IfCont;
                struct
                {
                    union
                    {
                        uint8_t Type : 4;
                        uint8_t Code : 4;
                    } u;
                    // DataType Type;
                    // CmpCode Code;
                    DataAddress Operand1;
                    DataAddress Operand2;
                    DataAddress Result;
                } Cmp;
                struct
                {
                    DataAddress Operand;
                } Single;
                struct
                {
                    DataAddress Operand;
                    DataAddress Result;
                } Unary;
                struct
                {
                    DataAddress Operand;
                    DataAddress Result;
                    DataType Type;
                } UnaryTyped;
                struct
                {
                    DataAddress Operand1;
                    DataAddress Operand2;
                    DataAddress Result;
                } Binary;
                struct
                {
                    TypeId Tid;
                    DataAddress Result;
                    DataAddress Param1;
                    uint8_t cParams; // 255 parameters max
                } AllocTuple;
                struct
                {
                    DataAddress Tuple;
                    DataAddress Index;
                    DataAddress Result;
                } GetTupleField;
                struct
                {
                    DataAddress Tuple;
                    DataAddress Param1;
                    DataAddress Param2;
                    uint8_t cParams; // Parameters are stored as ParamPack
                } GetTupleFields;
                
                // native methods
                struct
                {
                    uint16_t Offset; // offset in event block
                    uint16_t Size;
                    DataAddress Result;
                } ReadEvent;
                
                // invoke call expects cParam parameters on stack in form
                // [(type, value), (type, value)] where type is one of primitive types
                struct
                {
                    DataAddress Param1;
                    TypeId Param1Type;
                    uint8_t cParams;
                } Send;

                struct
                {
                    DataAddress Store;
                    DataAddress Key;
                    DataAddress Value;
                } AddKey;

                struct
                {
                    DataAddress Store;
                    DataAddress Key;
                } RemoveKey;
                
                struct
                {
                    DataAddress Store;
                    DataAddress Key;
                } GetKey;

                // place holder to ensure that sizeof of structure is 8
                unsigned char Value[7];
            } u;
            OpCode Code;
        } insr;
        
        struct
        {
            DataAddress Param1;
            DataAddress Param2;
            TypeId Param1Type;
            TypeId Param2Type;
        } TypedParamPack;

        struct
        {
            DataAddress Param1;
            DataAddress Param2;
            DataAddress Param3;
            DataAddress Param4;
        } ParamPack;

        Value value;
    } u;
};

// test only function to verify sizes of Op parts
inline void VerifyOpSizes()
{
    Op op;
    int p;
    p = sizeof(op.u);
    p = sizeof(op.u.insr.u.Jump);
    p = sizeof(op.u.insr.u.FunctionProlog);
    p = sizeof(op.u.insr.u.Call);
    p = sizeof(op.u.ParamPack);
    p = sizeof(op.u.insr.u.Return);
    p = sizeof(op.u.insr.u.Store);
    p = sizeof(op.u.insr.u.Br);
    p = sizeof(op.u.insr.u.Cmp);
    p = sizeof(op.u.insr.u.IfCont);
    p = sizeof(op.u.insr.u.Unary);
    p = sizeof(op.u.insr.u.Binary);
    p = sizeof(op.u.insr.u.AllocTuple);
    p = sizeof(op.u.insr.u.ReadEvent);
    p = sizeof(op.u.insr.u.Send);
    p = sizeof(op.u.TypedParamPack);
    p = sizeof(op.u.insr.u.AddKey);
    p = sizeof(op.u.insr.u.RemoveKey);
}

#pragma pack(pop)

static_assert(sizeof(Op) == 8, "op structure must be 64 bit size");



