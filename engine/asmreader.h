// Copyright (c) 2013-2014 Alexandre Grigorovitch (alexezh@gmail.com).
//
// Permission is hereby granted, free of charge, to any person obtaining a
// copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit
// persons to whom the Software is furnished to do so, subject to the
// following conditions:
//
// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
// OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN
// NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
// USE OR OTHER DEALINGS IN THE SOFTWARE.
#pragma once

#include <string>

// parses asm into sequence of
class AsmReader
{
public:
	AsmReader(const std::string& data)
	{
		m_Current = data.cbegin();
		m_End = data.cend();
	}

    bool ReadLine(std::vector<std::string>& res)
    {
        // read until we get something
        do
        {
            res.clear();
            
            if(m_Current == m_End)
                return false;
            
            NextTokenResult r;
            do
            {
                r = NextToken();
                if(m_Token.size() > 0)
                    res.push_back(std::move(m_Token));
            } while(r != NextTokenResult::EOL);
        } while(res.size() == 0);
        
        return true;
    }

private:
    enum class NextTokenResult
    {
        Comma,
        EOL,
    };
    
    // returns separator between tokens
	NextTokenResult NextToken()
	{
        m_Value.empty();

        // skip white space
        while(m_Current != m_End && *m_Current == ' ')
        {
            ++m_Current;
        }
        
        // get next token
        while(m_Current != m_End)
        {
            char c = *m_Current;
            ++m_Current;
            
            if(c == '\r' || c == '\n')
            {
                // skip over another \n
                if(m_Current != m_End)
                {
                    c = *m_Current;
                    if(c == '\n')
                        ++m_Current;
                }
                
                return NextTokenResult::EOL;
            }
            else if(c == ',' || isspace(c))
            {
                return NextTokenResult::Comma;
            }
            else
            {
                m_Token.push_back(c);
            }
        }
        
		return NextTokenResult::EOL;
	}

private:
	std::string::const_iterator m_Current;
	std::string::const_iterator m_End;
	std::string m_Value;
    std::string m_Token;
};

