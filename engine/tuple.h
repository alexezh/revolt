// Copyright (c) 2013-2014 Alexandre Grigorovitch (alexezh@gmail.com).
//
// Permission is hereby granted, free of charge, to any person obtaining a
// copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit
// persons to whom the Software is furnished to do so, subject to the
// following conditions:
//
// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
// OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN
// NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
// USE OR OTHER DEALINGS IN THE SOFTWARE.
#pragma once

#include "object.h"
#include "runtime.h"

class Tuple : public Object
{
public:
    Tuple(const TypeInfo& type)
        : m_Type(type)
    {
        memset(&m_Values[0], 0, type.GetChildrenCount()*sizeof(Value));
    }

    static const DataType Type = DataType::Tuple;

    Value Get(Runtime& runtime, int idx) const
    {
        auto& ti = m_Type.GetChildAt(idx);
        
        const Value& v = m_Values[idx];
        if(ti.IsObject())
        {
            runtime.AddRefObject(v.u.Handle);
        }
        
        return v;
    }

    void Set(Runtime& runtime, int idx, Value v)
    {
        if(m_Type.GetChildAt(idx).IsObject())
        {
            runtime.AssignHandle(v.u.Handle, m_Values[idx].u.Handle);
        }
        else
        {
            m_Values[idx] = v;
        }
    }
    
    void FreeValues(Runtime& rt)
    {
        if(!m_Type.HasChildObjects())
            return;
        
        uint16_t fields = m_Type.GetChildrenCount();
        for(uint16_t i = 0; i < fields; i++)
        {
            auto& type = m_Type.GetChildAt(i);
            if(type.IsObject())
            {
                rt.ReleaseObject(m_Values[i].u.Handle);
            }
        }
    }
    
private:
    const TypeInfo& m_Type;
    Value m_Values[1];
};

void ConstructTuple(const TypeInfo& type, Object* pObj, size_t cbObj)
{
	if (type.Base() != DataType::Tuple)
		throw std::runtime_error("invalid type info");

	if (type.Children().size() == 0)
		throw std::runtime_error("tuple must contain at least one field");

	if (cbObj != sizeof(Tuple) + sizeof(Value) * (type.Children().size() - 1))
		throw std::runtime_error("invalid object size");

	// call constructor
	new(pObj)Tuple(type);
}

void DestructTuple(Runtime& rt, Object* p)
{
    Tuple* pObj = reinterpret_cast<Tuple*>(p);
    pObj->FreeValues(rt);
	pObj->~Tuple();
}

uint16_t GetSizeTuple(const TypeInfo& ti)
{
	return sizeof(Tuple) + sizeof(Value) * (ti.Children().size() - 1);
}

struct TupleHash
{
	std::size_t operator()(Tuple const& t) const
	{
		return 0;
	}
};

struct TuplePtrHash
{
	std::size_t operator()(Tuple const* t) const
	{
		return 0;
	}
};