// Copyright (c) 2013-2014 Alexandre Grigorovitch (alexezh@gmail.com).
//
// Permission is hereby granted, free of charge, to any person obtaining a
// copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit
// persons to whom the Software is furnished to do so, subject to the
// following conditions:
//
// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
// OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN
// NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
// USE OR OTHER DEALINGS IN THE SOFTWARE.
#pragma once

#include <vector>
#include <assert.h>
#include "op.h"

// data stack stores all values as int64_t to avoid alingment issues
class DataStack
{
public:
    DataStack(size_t maxSize)
    {
        m_Size = maxSize;
        m_DataStack = new Value[maxSize];
    }

    template <typename T>
    inline void Push(T v)
    {
        if(m_Head == m_Size)
            throw std::runtime_error("data stack overflow");
        
        m_DataStack[m_Head] = MakeValue(v);
        ++m_Head;
    }

    inline void Push(Value v)
    {
        if(m_Head == m_Size)
            throw std::runtime_error("data stack overflow");
        
        m_DataStack[m_Head] = v;
        ++m_Head;
    }

    size_t GetBase()
    {
        return m_Base;
    }
    
    size_t GetHead()
    {
        return m_Head;
    }

    void AllocStack(uint16_t size)
    {
        if(m_Head + size >= m_Size)
            throw std::runtime_error("data stack overflow");

        m_Head += size;
    }
    
    // offset in positive direction from base
    FORCEINLINE Value GetBaseValue(size_t offset)
    {
        if(m_Base + offset > m_Size || m_Base + offset < m_Base)
            throw std::runtime_error("incorrect address");
        
        return m_DataStack[m_Base + offset];
    }

    FORCEINLINE Value& GetBaseValueRef(size_t offset)
    {
        if(m_Base + offset > m_Size || m_Base + offset < m_Base)
            throw std::runtime_error("incorrect address");
        
        return m_DataStack[m_Base + offset];
    }

    template <typename T>
    FORCEINLINE void SetBaseValue(size_t offset, T value)
    {
        auto addr = m_Base + offset;
        if(addr >= m_Size || addr < m_Base)
            throw std::runtime_error("incorrect address");
        
        m_DataStack[m_Base + offset] = value;
    }

    FORCEINLINE Value GetHeadValue(size_t offset)
    {
        if(m_Head - offset < m_Base || m_Head - offset > m_Head)
            throw std::runtime_error("incorrect address");
        
        return m_DataStack[m_Head - offset];
    }

    template <typename T>
    FORCEINLINE void SetHeadValue(size_t offset, T value)
    {
        if(m_Head - offset < m_Base || m_Head - offset > m_Head)
            throw std::runtime_error("incorrect address");
        
        m_DataStack[m_Head - offset] = value;
    }

    //
    void GetHeadInt32(size_t offset)
    {
        assert(false);
    }
    
    // change base and head position
    inline void SetPos(size_t base, size_t head)
    {
        assert(base <= head);
        assert(head < m_Size);
        m_Base = base;
        m_Head = head;
    }
    
private:
    size_t m_Base = 0;
    size_t m_Head = 0;
    Value* m_DataStack;
    size_t m_Size = 0;
};
