// Copyright (c) 2013-2014 Alexandre Grigorovitch (alexezh@gmail.com).
//
// Permission is hereby granted, free of charge, to any person obtaining a
// copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit
// persons to whom the Software is furnished to do so, subject to the
// following conditions:
//
// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
// OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN
// NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
// USE OR OTHER DEALINGS IN THE SOFTWARE.
#pragma once

#include <assert.h>
#include <vector>
#include <functional>
#include "op.h"
#include "typeinfo.h"
#include "object.h"
#include "heap.h"

class Runtime;

class Tuple;
class String;
class KeyStore;

// manages in memory objects (such as types, strings etc) for Runner
class Runtime
{
    friend class Tuple;
    friend class String;
    friend class KeyStore;
public:
    Runtime()
    {
		m_Heap[0] = { 0, 32, 32, 1024 };
		m_Heap[1] = { 1, 64, 16, 512 };
		m_Heap[2] = { 2, 128, 16, 512 };
		m_Heap[3] = { 3, 256, 8, 256 };
	}
    
    Runtime& operator=(Runtime&& rt)
    {
        for(size_t i = 0; i < BucketCount; i++)
            m_Heap[i] = std::move(m_Heap[i]);
        
        return *this;
    }
    
    Handle AllocObject(const TypeInfo& ti)
    {
		return AllocObject<Object>(ti).first;
    }

    template <class T>
    std::pair<Handle, T*> AllocObject(const TypeInfo& ti)
    {
        auto size = ti.Size();
        assert(size > 0);

        // we can use tricks from http://graphics.stanford.edu/~seander/bithacks.html#IntegerLog
        // to compute bucket faster but since we only have 4 buckets does not matter
        uint16_t bucket;
        if(size <= 32)
        {
            bucket = 0;
        }
        else if(size <= 64)
        {
            bucket = 1;
        }
        else if(size <= 128)
        {
            bucket = 2;
        }
        else if(size <= 256)
        {
            bucket = 3;
        }
        else
        {
            throw std::runtime_error("size is too big");
        }

        return m_Heap[bucket].AllocObject<T>(ti);
    }
    
	template <class T>
	T* GetObject(Handle h)
	{
        return GetHeap(h.u.Bucket)->GetObject<T>(h);
	}

	Handle AllocString(uint32_t size)
	{
		return m_Heap[0].AllocString(size);
	}
    
    // test only methods
    size_t AllocatedObjects()
    {
        size_t total = 0;
        for(auto& h : m_Heap)
        {
            total += h.AllocatedObjects();
        }
        return total;
    }
    
    void AssignHandle(Handle src, Handle& dest)
    {
        if(dest.u.Value != 0)
        {
            Heap* pHeap = GetHeap(dest.u.Bucket);
            pHeap->ReleaseObject(*this, dest);
        }
        
        dest = src;
        if(src.u.Value != 0)
        {
            Heap* pHeap = GetHeap(src.u.Bucket);
            pHeap->AddRefObject(src);
        }
    }
    
    void AddRefObject(Handle src)
    {
        if(src.u.Value != 0)
        {
            Heap* pHeap = GetHeap(src.u.Bucket);
            pHeap->AddRefObject(src);
        }
    }

    void ReleaseObject(Handle src)
    {
        if(src.u.Value != 0)
        {
            Heap* pHeap = GetHeap(src.u.Bucket);
            pHeap->ReleaseObject(*this, src);
        }
    }
    
private:
    FORCEINLINE Heap* GetHeap(uint16_t idx)
    {
        if(idx >= BucketCount)
            throw std::runtime_error("incorrect bucket");
        
        return &m_Heap[idx];
    }
    
    // 32, 64, 128 and 256 bytes
    enum
    {
        BucketCount = 4,
    };
	//    Heap m_Heap[BucketCount] {{ 0, 32, 32, 1024 }, { 1, 64, 16, 512 }, { 2, 128, 16, 512 }, { 3, 256, 8, 256 }};
	Heap m_Heap[BucketCount];
};
