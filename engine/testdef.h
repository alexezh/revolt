#pragma once

#include <sstream>
#include <string>
#include <vector>
#include <functional>

class Test;
class TestLibrary;

struct TestError : public std::exception
{
    TestError(const char* psz)
        : m_Message(psz)
    {
    }

    TestError(std::string&& sz)
        : m_Message(std::move(sz))
    {
    }
    
    std::string m_Message;
};

struct TestAssert
{
    static void IsTrue(bool val)
    {
        if(!val)
            throw TestError("Value is incorrect");
    }
};

class Test
{
    friend class TestLibrary;
public:
    typedef std::function<void(Test&)> TestMethod;
    
    Test()
    {
    }
    
    Test(Test&& t)
    {
        m_Methods.swap(t.m_Methods);
        m_Name.swap(t.m_Name);
    }

    Test(std::string&& name)
        : m_Name(std::move(name))
    {
    }

    virtual void Setup()
    {
    }

    virtual void Teardown()
    {
    }
    
    virtual void Run()
    {
        for(auto& m : m_Methods)
        {
            std::cout << m.first << "\n";
            m.second(*this);
        }
    }
    
    const std::string& Name()
    {
        return m_Name;
    }

protected:

protected:
    std::string m_Name;
    std::vector<std::pair<std::string, TestMethod>> m_Methods;
};

class TestLibrary
{
public:
    TestLibrary& AddTest(std::unique_ptr<Test>&& test)
    {
        m_Tests.push_back(std::move(test));
        return *this;
    }

    TestLibrary& AddTest(std::string&& name)
    {
        m_Tests.push_back(std::make_unique<Test>(std::move(name)));
        return *this;
    }

    template <class TTest>
    TestLibrary& AddTest(TTest&& test)
    {
        m_Tests.push_back(std::make_unique<TTest>(std::move(test)));
        return *this;
    }

    template <class TTest>
    TestLibrary& AddMethod(std::string&& name, std::function<void(TTest& test)>&& func)
    {
        m_Tests.back()->m_Methods.push_back(std::make_pair(std::move(name), [func](Test& test)
        {
            func(static_cast<TTest&>(test));
        }));
        return *this;
    }

    template <class TTest>
    TestLibrary& AddMethod(std::function<void(TTest& test)>&& func)
    {
        m_Tests.back()->m_Methods.push_back(std::make_tuple("", [func](Test& test)
        {
            func(static_cast<TTest&>(test));
        }));
        return *this;
    }
    
    const std::vector<std::unique_ptr<Test>>& Tests()
    {
        return m_Tests;
    }
    
private:
    std::vector<std::unique_ptr<Test>> m_Tests;
};



