#pragma once

#include "op.h"
#include "typeinfo.h"

bool CompareValues(TypeId t, Value v1, Value v2)
{
    switch(t)
    {
        case static_cast<TypeId>(DataType::Boolean):
            return v1.u.Int32 == v2.u.Bool;
        case static_cast<TypeId>(DataType::Int32):
            return v1.u.Int32 == v2.u.Int32;
        case static_cast<TypeId>(DataType::Double):
            return v1.u.Double == v2.u.Double;
        default:
            throw std::runtime_error("unknown type");
    }
}

uint32_t HashValue(const TypeInfo& ti, const Value& v)
{
    switch(ti.Base())
    {
        case DataType::Boolean:
            return static_cast<uint32_t>(v.u.Bool);
        case DataType::Int32:
            return static_cast<uint32_t>(std::hash<uint32_t>()(v.u.Int32));
        case DataType::Int64:
            return static_cast<uint32_t>(std::hash<uint64_t>()(v.u.Int64));
        default:
            throw std::runtime_error("not supported");
    }
    return false;
}
