// Copyright (c) 2013-2014 Alexandre Grigorovitch (alexezh@gmail.com).
//
// Permission is hereby granted, free of charge, to any person obtaining a
// copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit
// persons to whom the Software is furnished to do so, subject to the
// following conditions:
//
// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
// OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN
// NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
// USE OR OTHER DEALINGS IN THE SOFTWARE.
#pragma once
#include <unordered_map>
#include <algorithm>

typedef uint16_t GlobalId;

class BindingTable
{
public:
    typedef std::vector<std::pair<std::string, OpAddress>> EntryVec;

    BindingTable()
    {
    }
    
    BindingTable(BindingTable&& t)
    {
        swap(t);
    }

    BindingTable(EntryVec&& entries)
    {
        m_Entries.swap(entries);
        std::sort(m_Entries.begin(), m_Entries.end(), Comp());
    }
    
    std::pair<bool, OpAddress> GetInitEntry() const
    {
        auto it = std::lower_bound(m_Entries.begin(), m_Entries.end(), "__initGlobals", Comp());
        if(it == m_Entries.end())
            return std::make_pair(false, 0);
        
        return std::make_pair(true, it->second);
    }
    inline const OpOffset GetEventEntry(const std::string& id) const
    {
        auto it = std::lower_bound(m_Entries.begin(), m_Entries.end(), id, Comp());
        if(it == m_Entries.end())
            throw std::runtime_error("entry not found");
        
        return it->second;
    }
    const EntryVec& Events() const
    {
        return m_Entries;
    }

    
    void swap(BindingTable& t)
    {
        m_Entries.swap(t.m_Entries);
    }
    
private:
    struct Comp
    {
        bool operator()(const std::pair<std::string, OpAddress>& v1, const std::pair<std::string, OpAddress>& v2) const
        {
            return v1.first.compare(v2.first) < 0;
        }
        bool operator()(const std::pair<std::string, OpAddress>& v1, const std::string& v2) const
        {
            return v1.first.compare(v2) < 0;
        }
    };
    EntryVec m_Entries;
};

