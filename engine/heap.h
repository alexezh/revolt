// Copyright (c) 2013-2014 Alexandre Grigorovitch (alexezh@gmail.com).
//
// Permission is hereby granted, free of charge, to any person obtaining a
// copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit
// persons to whom the Software is furnished to do so, subject to the
// following conditions:
//
// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
// OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN
// NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
// USE OR OTHER DEALINGS IN THE SOFTWARE.
#pragma once

#include <assert.h>
#include <vector>
#include "op.h"
#include "typeinfo.h"
#include "object.h"

class Runtime;

class Tuple;
class String;
class KeyStore;

class Heap
{
private:
    void MoveFrom(Heap&& src)
    {
		m_Bucket = src.m_Bucket;
		m_BlockSize = src.m_BlockSize;
		m_ObjectSize = src.m_ObjectSize;
		m_MaxObjects = src.m_MaxObjects;
    }

public:
	Heap()
	{
	}
	
	Heap(uint16_t bucket, uint16_t objectSize, uint16_t blockSize, uint16_t maxObjects)
    {
        assert(maxObjects > 0);
        
        m_Bucket = bucket;
        m_BlockSize = blockSize;
        m_ObjectSize = objectSize;
        m_MaxObjects = maxObjects;
    }
    
	Heap(Heap&& src)
	{
        MoveFrom(std::move(src));
	}

	Heap& operator=(Heap&& src)
	{
        MoveFrom(std::move(src));
        return *this;
	}
	
	template <class T>
	std::pair<Handle, T*> AllocObject(const TypeInfo& ti)
	{
        if(m_FreeHead == InvalidHandle)
            AddBlock();
        
        Handle hObj;
        hObj.u.Index = m_FreeHead;
        hObj.u.Bucket = m_Bucket;
        Object* pObj = GetObjectByIndex(m_FreeHead);
        m_FreeHead = pObj->u.m_Next;
        
        pObj->m_pTi = &ti;
        
        // initial refcount is 1
        // caller is responsible for releasing
        pObj->u.m_cRef = 1;
        
        ti.Construct(ti, pObj, ti.Size());
        m_cUsed++;

        return std::make_pair(hObj, static_cast<T*>(pObj));
	}

	Handle AllocString(uint32_t size)
	{
		Handle v;
		v.u.Bucket = 0;
		v.u.Index = 0;
		return v;
	}

	template <class T>
	T* GetObject(Handle h)
	{
        if(h.u.Index >= m_nObjects)
            throw std::runtime_error("invalid index");
            
        Object* pObj = GetObjectByIndex(h.u.Index);

        if(pObj->m_pTi == nullptr)
			throw std::runtime_error("access to free objects");

		return reinterpret_cast<T*>(pObj);
	}

    FORCEINLINE void AddRefObject(Handle h)
    {
        Object* pObj = GetObjectByIndex(h.u.Index);
        ++pObj->u.m_cRef;
    }

    void ReleaseObject(Runtime& rt, Handle h)
    {
        Object* pObj = GetObjectByIndex(h.u.Index);
        if(pObj->m_pTi == nullptr)
            throw std::runtime_error("Object already released");
        
        --pObj->u.m_cRef;
        if(pObj->u.m_cRef == 0)
        {
            FreeObject(rt, h, pObj);
        }
    }
    
    uint32_t AllocatedObjects()
    {
        return m_cUsed;
    }
    
private:
    FORCEINLINE void FreeObject(Runtime& rt, Handle h, Object* pObj)
    {
        pObj->m_pTi->Destruct(rt, pObj);
        pObj->m_pTi = nullptr;
        pObj->u.m_Next = m_FreeHead;
        m_FreeHead = h.u.Index;
        assert(m_cUsed != 0);
        --m_cUsed;
    }
    
    // block of objects to minimize CRT memory cost
    struct ObjectBlock
    {
        ObjectBlock* pNext;
        // make 8 bytes since it is default alingment anyway
        unsigned char Data[8];
    };


    FORCEINLINE Object* GetObjectByIndex(uint16_t idx)
    {
        return m_pObjects[idx];
    }
    
    void AddBlock()
    {
        static_assert(sizeof(ObjectBlock) == sizeof(char*) + 8, "Object block is a pointer and a char");
        ObjectBlock* p = reinterpret_cast<ObjectBlock*>(new char[sizeof(ObjectBlock) - 8 + m_ObjectSize*m_BlockSize]);
        p->pNext = m_pHead;
        m_pHead = p;
        
        auto nObjects = m_nObjects;
        Object** pObjects = m_pObjects;
        m_nObjects = nObjects + m_BlockSize;
        if(m_nObjects < nObjects || m_nObjects > m_MaxObjects)
            throw std::runtime_error("too many objects");
        
        m_pObjects = new Object*[m_nObjects];
        if(pObjects != nullptr)
        {
            memcpy(m_pObjects, pObjects, sizeof(Object*)*nObjects);
            delete pObjects;
        }
        
        // initialize handle array with new objects
        // add objects to free chain
        uint32_t freeHead = m_FreeHead;
        
        for(uint16_t i = 0; i < m_BlockSize; i++)
        {
            Object* pObj = reinterpret_cast<Object*>(&p->Data[i * m_ObjectSize]);
            m_pObjects[nObjects+i] = pObj;

            pObj->u.m_Next = freeHead;
            freeHead = nObjects+i;
        }
        
        m_FreeHead = freeHead;
    }
    
private:
    uint16_t m_MaxObjects = 0;
    uint16_t m_cUsed = 0;

    // bucket for this heap
    uint16_t m_Bucket;
    
    // maximum object size
    uint16_t m_ObjectSize = 0;

    // number of items to allocate in each block
    uint16_t m_BlockSize = 0;
    
    enum
    {
        // outside int16_t range
        InvalidHandle = 0x10000
    };
    uint32_t m_FreeHead = InvalidHandle;
    
    // pointers to object (both allocated and free)
    Object** m_pObjects = nullptr;
    uint16_t m_nObjects = 0;
    
    ObjectBlock* m_pHead = nullptr;
};
