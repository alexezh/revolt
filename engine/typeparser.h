// Copyright (c) 2013-2014 Alexandre Grigorovitch (alexezh@gmail.com).
//
// Permission is hereby granted, free of charge, to any person obtaining a
// copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit
// persons to whom the Software is furnished to do so, subject to the
// following conditions:
//
// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
// OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN
// NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
// USE OR OTHER DEALINGS IN THE SOFTWARE.
#pragma once

#include <assert.h>
#include <vector>
#include <functional>
#include <map>
#include <array>
#include <regex>
#include <utility>
#include "op.h"
#include "program.h"
#include "typeinfo.h"
#include "jsonreader.h"
#include "symboltable.h"
#include "asmerror.h"

void InitKeyStoreType(TypeStore& store, std::vector<const TypeInfo*>&);
void ConstructKeyStore(const TypeInfo& type, Object* pObj, size_t cbObj);
void DestructKeyStore(Runtime&, Object* p);
uint16_t GetSizeKeyStore(const TypeInfo& type);

void ConstructTuple(const TypeInfo& type, Object* pObj, size_t cbObj);
void DestructTuple(Runtime&, Object* p);
uint16_t GetSizeTuple(const TypeInfo& type);

/*
    creates Type based on Json representation of a
    {
        name: <name>
        type: {
            base : <name>
            fields : [
                typename | {
                }
            ]
        }
    }
*/

class TypeParser
{
public:
    static TypeId Parse(JsonReader& rdr, TypeStore& ts, const SymbolTable& sym)
    {
        TypeParser p(rdr, ts, sym);
        return p.Process();
    }
    
private:
    TypeParser(JsonReader& rdr, TypeStore& ts, const SymbolTable& sym)
        : m_Rdr(rdr)
        , m_TypeStore(ts)
        , m_Symbols(sym)
    {
    }

    TypeId Process()
    {
        std::string base;
        std::vector<TypeId> fields;
        
    	while(m_Rdr.ReadNext())
    	{
            if(m_Rdr.Type() == JsonReader::NodeType::EndObject)
            {
                break;
            }
            
            if(m_Rdr.Type() == JsonReader::NodeType::String)
            {
            	if(m_Rdr.Name() == "base")
            		base = m_Rdr.TakeValue();
            }
            else if(m_Rdr.Type() == JsonReader::NodeType::Array)
            {
            	if(m_Rdr.Name() == "fields")
                    fields = ProcessFields();
            }
            else
            {
                throw JsonParseError();
            }
        }
    
        auto type = StringToDataType(base);
        auto tf = TypeFunctions(type);
        return m_TypeStore.CreateType(type, fields, std::get<0>(tf), std::get<1>(tf), std::get<2>(tf), std::get<3>(tf));
    }
    
    std::vector<TypeId> ProcessFields()
    {
        std::vector<TypeId> fields;
        
    	while(m_Rdr.ReadNext())
    	{
            if(m_Rdr.Type() == JsonReader::NodeType::EndArray)
            {
                break;
            }
            
            if(m_Rdr.Type() == JsonReader::NodeType::String)
            {
                fields.push_back(m_Symbols.LookupType(m_Rdr.Value()));
            }
            else if(m_Rdr.Type() == JsonReader::NodeType::Object)
            {
                fields.push_back(Process());
            }
            else
            {
                throw JsonParseError();
            }
        }
        
        return std::move(fields);
    }
    
    DataType StringToDataType(const std::string& type)
    {
        if(type == "keystore")
        {
            return DataType::KeyStore;
        }
        else if(type == "tuple" || type == "nullable")
        {
            return DataType::Tuple;
        }
        else
        {
            throw JsonParseError();
        }
    }
    
    std::tuple<TypeInfo::InitType, TypeInfo::ConstructHandler, TypeInfo::DestructHandler, TypeInfo::GetSizeHandler> TypeFunctions(DataType dt)
    {
        switch(dt)
        {
            case DataType::Tuple:
                return std::tuple<TypeInfo::InitType, TypeInfo::ConstructHandler, TypeInfo::DestructHandler, TypeInfo::GetSizeHandler>(nullptr, ConstructTuple, DestructTuple, GetSizeTuple);
            case DataType::KeyStore:
                return std::tuple<TypeInfo::InitType, TypeInfo::ConstructHandler, TypeInfo::DestructHandler, TypeInfo::GetSizeHandler>(InitKeyStoreType, ConstructKeyStore, DestructKeyStore, GetSizeKeyStore);
            default:
                throw JsonParseError();
        }
    }

    JsonReader& m_Rdr;
    TypeStore& m_TypeStore;
    const SymbolTable& m_Symbols;
};
