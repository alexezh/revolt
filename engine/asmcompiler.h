// Copyright (c) 2013-2014 Alexandre Grigorovitch (alexezh@gmail.com).
//
// Permission is hereby granted, free of charge, to any person obtaining a
// copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit
// persons to whom the Software is furnished to do so, subject to the
// following conditions:
//
// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
// OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN
// NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
// USE OR OTHER DEALINGS IN THE SOFTWARE.
#pragma once

#include "symboltable.h"
#include "asmreader.h"
#include "dataaddressparser.h"

typedef std::vector<std::pair<std::string, OpAddress>> PatchCollection;

class AsmCompiler
{
public:
    static OpAddress Parse(SymbolTable& st, PatchCollection& pc, const std::string& data, std::vector<Op>& ops)
    {
        AsmCompiler p(st, pc, data, ops);
        return p.Parse();
    }
    
private:
    class AutoParam
    {
    public:
        AutoParam(std::vector<Op>& ops)
            : m_Ops(ops)
        {
        }
        
        ~AutoParam()
        {
            for(auto v : m_Params)
            {
                m_Ops.push_back(v);
            }
        }
        
        void Push(Op op)
        {
            m_Params.push_back(op);
        }

        void Push(Value val)
        {
            Op v;
            v.u.value = val;
            m_Params.push_back(v);
        }
        
    private:
        std::vector<Op>& m_Ops;
        std::vector<Op> m_Params;
    };
    
    AsmCompiler(SymbolTable& st, PatchCollection& pc, const std::string& data, std::vector<Op>& ops)
        : m_Symbols(st)
        , m_Dap(st)
        , m_PatchExternal(pc)
        , m_Rdr(data)
        , m_Ops(ops)
    {
    }
    
    OpAddress Parse()
    {
        OpAddress startAddress = static_cast<OpAddress>(m_Ops.size());
        std::vector<std::string> lineTokens;
        
    	while(m_Rdr.ReadLine(lineTokens))
    	{
            assert(lineTokens.size() != 0);
            
            // either op or label
            auto code = FindOp(lineTokens[0].c_str());
            if(code != OpCode::Undefined)
            {
                Op op;
                op.u.insr.Code = code;
                
                switch (code) {
                    case OpCode::Stop:
                        m_Ops.push_back(op);
                        break;
                    case OpCode::Jump:
                        ValidateParamCountEQ(lineTokens, 1);
                        op.u.insr.u.Jump.Address = 0;
                        m_PatchLabels.push_back(std::make_pair(lineTokens[1], m_Ops.size()));
                        m_Ops.push_back(op);
                        break;
                    case OpCode::FunctionProlog:
                        {
                            AutoParam ap(m_Ops);
                            ValidateParamCountEQ(lineTokens, 1);
                            op.u.insr.u.FunctionProlog.cVars = stoi(lineTokens[1]);
                            m_Ops.push_back(op);
                        }
                        break;
                    case OpCode::Call:
                        HandleCall(lineTokens, op);
                        break;
                    case OpCode::Return:
                        {
                            AutoParam ap(m_Ops);
                            ValidateParamCountLE(lineTokens, 1);
                            if(lineTokens.size() == 2)
                                ParseDataAddress(lineTokens[1], op.u.insr.u.Return.Result, ap);
                            else
                                op.u.insr.u.Return.Result.u.Addr.Mode = DataAddressType::Undefined;
                            m_Ops.push_back(op);
                        }
                        break;
                    case OpCode::Store:
                        HandleStore(lineTokens, op);
                        break;
                    case OpCode::StoreObject:
                        HandleStoreObject(lineTokens, op);
                        break;
                    case OpCode::InitObject:
                        HandleStoreObject(lineTokens, op);
                        break;
                    case OpCode::ReleaseObject:
                        HandleReleaseObject(lineTokens, op);
                        break;
                    case OpCode::AllocObject:
                        HandleAllocObject(lineTokens, op);
                        break;
                    case OpCode::AllocTuple:
                        HandleAllocTuple(lineTokens, op);
                        break;
                    case OpCode::GetTupleField:
                        HandleGetTupleField(lineTokens, op);
                        break;
                    case OpCode::GetTupleFields:
                        HandleGetTupleFields(lineTokens, op);
                        break;
                    case OpCode::Br:
                        HandleBr(lineTokens, op);
                        break;
                    case OpCode::AddInt32:
                    case OpCode::SubInt32:
                    case OpCode::MulInt32:
                    case OpCode::AddDouble:
                    case OpCode::SubDouble:
                    case OpCode::DivDouble:
                        HandleBinaryOp(lineTokens, op);
                        break;
                    case OpCode::Cmp:
                        HandleCmp(lineTokens, op);
                        break;
                    case OpCode::Not:
                    case OpCode::IncInt32:
                    case OpCode::DecInt32:
                        HandleUnaryOp(lineTokens, op);
                        break;
                    case OpCode::Land:
                        HandleBinaryOp(lineTokens, op);
                        break;
                    case OpCode::Lor:
                        HandleBinaryOp(lineTokens, op);
                        break;
                    case OpCode::ReadEventBoolean:
                        HandleReadEvent(lineTokens, op, DataType::Boolean);
                        break;
                    case OpCode::ReadEventInt32:
                        HandleReadEvent(lineTokens, op, DataType::Int32);
                        break;
                    case OpCode::ReadEventInt64:
                        HandleReadEvent(lineTokens, op, DataType::Int64);
                        break;
                    case OpCode::ReadEventDouble:
                        HandleReadEvent(lineTokens, op, DataType::Double);
                        break;
                    case OpCode::ReadEventString:
                        HandleReadEvent(lineTokens, op, DataType::String);
                        break;
                    case OpCode::Send:
                        HandleSend(lineTokens, op);
                        break;
                    case OpCode::GetKey:
                        HandleGetKey(lineTokens, op);
                        break;
                    case OpCode::AddKey:
                        HandleAddKey(lineTokens, op);
                        break;
                    case OpCode::RemoveKey:
                        HandleRemoveKey(lineTokens, op);
                        break;

                    default:
                        throw std::runtime_error("unsupported op code");
                }
            }
            else if(lineTokens.size() == 1 && lineTokens[0].length() > 0 && lineTokens[0][lineTokens[0].length()-1] == ':')
            {
                std::string s = lineTokens[0].substr(0, lineTokens[0].length()-1);
                m_Symbols.AddLabel(std::move(s), static_cast<uint16_t>(m_Ops.size()));
            }
            else
            {
                throw std::runtime_error("unknown token");
            }
    	}
        
        PatchLabels();

        return startAddress;
    }
    
    void ValidateParamCountEQ(const std::vector<std::string>& lineTokens, int nParams)
    {
        if(lineTokens.size() == nParams + 1)
            return;
        
        throw std::runtime_error("invalid parameter count");
    }

    void ValidateParamCountLE(const std::vector<std::string>& lineTokens, int nParams)
    {
        if(lineTokens.size() <= nParams + 1)
            return;
        
        throw std::runtime_error("invalid parameter count");
    }

    void ValidateParamCountGE(const std::vector<std::string>& lineTokens, int nParams)
    {
        if(lineTokens.size() > 255)
            throw std::runtime_error("Only support 255 parameters");
        
        if(lineTokens.size() >= nParams + 1)
            return;
        
        throw std::runtime_error("invalid parameter count");
    }

    DataAddress ParseTarget(const std::string& token)
    {
        auto v = m_Dap.Parse(token);
        if(v.first.u.Addr.Mode == DataAddressType::Constant)
        {
            throw std::runtime_error("invalid address mode for return parameter");
        }
        return v.first;
    }
    
    // writes optional value at position pointer by iterator
    void ParseDataAddress(const std::string& param, DataAddress& dest, AutoParam& ap)
    {
        auto v = m_Dap.Parse(param);
        dest = v.first;
        if(dest.u.Addr.Mode == DataAddressType::Constant)
        {
            ap.Push(v.second);
        }
    }

    void ParseTypedDataAddress(const std::string& param, DataAddress& dest, TypeId& type, AutoParam& ap)
    {
        auto v = m_Dap.ParseTyped(param);
        dest = std::get<0>(v);
        type = std::get<2>(v);
        if(dest.u.Addr.Mode == DataAddressType::Constant)
        {
            ap.Push(std::get<1>(v));
        }
    }
    
    void PatchLabels()
    {
        for(auto& p : m_PatchLabels)
        {
            Op& op = m_Ops[p.second];
            auto symbol = m_Symbols.Lookup(p.first);
            
            switch(op.u.insr.Code)
            {
                case OpCode::Br:
                    if(symbol.Kind != SymbolKind::Label)
                        throw std::runtime_error("symbol is not a function");
                    
                    op.u.insr.u.Br.Else = symbol.u.Function.Offset;
                break;
                case OpCode::Jump:
                    if(symbol.Kind != SymbolKind::Label)
                        throw std::runtime_error("symbol is not a function");
                    
                    op.u.insr.u.Jump.Address = symbol.u.Function.Offset;
                break;
                default:
                    throw std::runtime_error("unable to patch instruction");
            }
        }
    }
    
    // Format for store instruction:
    // store Source, Dest
    void HandleStore(const std::vector<std::string>& lineTokens, Op& op)
    {
        AutoParam ap(m_Ops);
        ValidateParamCountEQ(lineTokens, 2);
        
        ParseDataAddress(lineTokens[2], op.u.insr.u.Store.Source, ap);
        op.u.insr.u.Store.Destination = ParseTarget(lineTokens[1]);

        m_Ops.push_back(op);
    }

    // TODO. add option to tranfer refcount
    void HandleStoreObject(const std::vector<std::string>& lineTokens, Op& op)
    {
        AutoParam ap(m_Ops);
        ValidateParamCountEQ(lineTokens, 2);
        
        ParseDataAddress(lineTokens[2], op.u.insr.u.Store.Source, ap);
        op.u.insr.u.Store.Destination = ParseTarget(lineTokens[1]);

        m_Ops.push_back(op);
    }
    
    void HandleReleaseObject(const std::vector<std::string>& lineTokens, Op& op)
    {
        AutoParam ap(m_Ops);
        ValidateParamCountEQ(lineTokens, 1);
        
        ParseDataAddress(lineTokens[1], op.u.insr.u.Single.Operand, ap);

        m_Ops.push_back(op);
    }
    
    void HandleAllocObject(const std::vector<std::string>& lineTokens, Op& op)
    {
        ValidateParamCountGE(lineTokens, 2);
        op.u.insr.u.AllocTuple.Result = ParseTarget(lineTokens[1]);

        auto symbol = m_Symbols.Lookup(lineTokens[2]);
        if(symbol.Kind != SymbolKind::Type)
            throw std::runtime_error("unknown type");
        op.u.insr.u.AllocTuple.Tid = symbol.u.Type.Tid;
        op.u.insr.u.AllocTuple.cParams = 0;
        m_Ops.push_back(op);
    }
    
    void HandleAllocTuple(const std::vector<std::string>& lineTokens, Op& op)
    {
        ValidateParamCountGE(lineTokens, 3);
        op.u.insr.u.AllocTuple.Result = ParseTarget(lineTokens[1]);

        auto symbol = m_Symbols.Lookup(lineTokens[2]);
        if(symbol.Kind != SymbolKind::Type)
            throw std::runtime_error("unknown type");
        op.u.insr.u.AllocTuple.Tid = symbol.u.Type.Tid;
        op.u.insr.u.AllocTuple.cParams = lineTokens.size()-3;
        
        auto itToken = lineTokens.begin()+3;
        HandleParamOnePlusList(op, op.u.insr.u.AllocTuple.Param1, itToken, lineTokens.end());
    }
    
    void HandleGetTupleField(const std::vector<std::string>& lineTokens, Op& op)
    {
        AutoParam ap(m_Ops);
        ValidateParamCountEQ(lineTokens, 3);
        
        ParseDataAddress(lineTokens[1], op.u.insr.u.GetTupleField.Tuple, ap);
        ParseDataAddress(lineTokens[2], op.u.insr.u.GetTupleField.Index, ap);
        op.u.insr.u.GetTupleField.Result = ParseTarget(lineTokens[3]);

        m_Ops.push_back(op);
    }

    void HandleGetTupleFields(const std::vector<std::string>& lineTokens, Op& op)
    {
        AutoParam ap(m_Ops);
        ValidateParamCountGE(lineTokens, 2);
        
        ParseDataAddress(lineTokens[1], op.u.insr.u.GetTupleFields.Tuple, ap);
        op.u.insr.u.GetTupleFields.cParams = lineTokens.size()-2;

        HandleParamTwoPlusList(op, op.u.insr.u.GetTupleFields.Param1, op.u.insr.u.GetTupleFields.Param2, lineTokens.begin()+2, lineTokens.end());
    }

    void HandleCmp(const std::vector<std::string>& lineTokens, Op& op)
    {
        AutoParam ap(m_Ops);
        ValidateParamCountEQ(lineTokens, 5);
        auto& code = lineTokens[2];
        if(code == "eq")
        {
            op.u.insr.u.Cmp.u.Code = static_cast<uint8_t>(CmpCode::Equal);
        }
        else if(code == "lt")
        {
            op.u.insr.u.Cmp.u.Code = static_cast<uint8_t>(CmpCode::Less);
        }
        else if(code == "le")
        {
            op.u.insr.u.Cmp.u.Code = static_cast<uint8_t>(CmpCode::LessEqual);
        }
        else if(code == "gt")
        {
            op.u.insr.u.Cmp.u.Code = static_cast<uint8_t>(CmpCode::Greater);
        }
        else if(code == "ge")
        {
            op.u.insr.u.Cmp.u.Code = static_cast<uint8_t>(CmpCode::GreaterEqual);
        }
        else if(code == "ne")
        {
            op.u.insr.u.Cmp.u.Code = static_cast<uint8_t>(CmpCode::NotEqual);
        }
        else
        {
            throw std::runtime_error("incorrect comparison code");
        }
        
        auto tid = m_Symbols.LookupType(lineTokens[1]);
        if(static_cast<int>(tid) > static_cast<int>(DataType::Double))
            throw std::runtime_error("incorrect type");
        
        ParseDataAddress(lineTokens[3], op.u.insr.u.Cmp.Operand1, ap);
        ParseDataAddress(lineTokens[4], op.u.insr.u.Cmp.Operand2, ap);
        op.u.insr.u.Cmp.Result = ParseTarget(lineTokens[5]);
        
        m_Ops.push_back(op);
    }

    void HandleBr(const std::vector<std::string>& lineTokens, Op& op)
    {
        AutoParam ap(m_Ops);
        ValidateParamCountEQ(lineTokens, 2);
        
        ParseDataAddress(lineTokens[1], op.u.insr.u.Br.Operand, ap);

        // we might not have symbol yet so for now add symbol to patch list
        op.u.insr.u.Br.Else = 0;
        m_PatchExternal.push_back(std::make_pair(lineTokens[2], m_Ops.size()));

        m_Ops.push_back(op);
    }
    
    // Format for call instruction:
    // call Name, Param1, Param2, ... ParamN
    void HandleCall(const std::vector<std::string>& lineTokens, Op& op)
    {
        ValidateParamCountGE(lineTokens, 1);
        
        op.u.insr.u.Call.cParams = lineTokens.size() - 2;
 
        // we might not have symbol yet so for now add symbol to patch list
        m_PatchExternal.push_back(std::make_pair(lineTokens[1], m_Ops.size()));

        auto itToken = lineTokens.begin()+2;
        HandleParamOnePlusList(op, op.u.insr.u.Call.Param1, itToken, lineTokens.end());
    }
    
    // writes param list with first parameter going to main op
    // and the rest into ParamPack
    void HandleParamOnePlusList(Op& op, DataAddress& firstParam, std::vector<std::string>::const_iterator itToken, std::vector<std::string>::const_iterator itEnd)
    {
        {
            AutoParam ap(m_Ops);
            
            // put first param into op itself, other into additional CallParamPack blocks
            if(itToken != itEnd)
            {
                ParseDataAddress(*itToken, firstParam, ap);
                ++itToken;
            }

            m_Ops.push_back(op);
        }
        
        HandleParamPack(itToken, itEnd);
    }
    
    // writes param list with first two parameters going to main op
    // and the rest into ParamPack
    void HandleParamTwoPlusList(Op& op,
                    DataAddress& firstParam,
                    DataAddress& secondParam,
                    std::vector<std::string>::const_iterator itToken, std::vector<std::string>::const_iterator itEnd)
    {
        {
            AutoParam ap(m_Ops);
            
            // put first param into op itself, other into additional CallParamPack blocks
            if(itToken != itEnd)
            {
                ParseDataAddress(*itToken, firstParam, ap);
                ++itToken;
            }

            if(itToken != itEnd)
            {
                ParseDataAddress(*itToken, secondParam, ap);
                ++itToken;
            }

            m_Ops.push_back(op);
        }
        
        HandleParamPack(itToken, itEnd);
    }

    void HandleParamPack(std::vector<std::string>::const_iterator itToken, std::vector<std::string>::const_iterator itEnd)
    {
        while(itToken != itEnd)
        {
            AutoParam ap(m_Ops);
            Op opPack;
            
            if(itToken != itEnd)
            {
                ParseDataAddress(*itToken, opPack.u.ParamPack.Param1, ap);
                ++itToken;
            }
            if(itToken != itEnd)
            {
                ParseDataAddress(*itToken, opPack.u.ParamPack.Param2, ap);
                ++itToken;
            }
            if(itToken != itEnd)
            {
                ParseDataAddress(*itToken, opPack.u.ParamPack.Param3, ap);
                ++itToken;
            }
            
            m_Ops.push_back(opPack);
        }
    }

    void HandleBinaryOp(const std::vector<std::string>& lineTokens, Op& op)
    {
        AutoParam ap(m_Ops);
        ValidateParamCountEQ(lineTokens, 3);
        ParseDataAddress(lineTokens[1], op.u.insr.u.Binary.Operand1, ap);
        ParseDataAddress(lineTokens[2], op.u.insr.u.Binary.Operand2, ap);
        op.u.insr.u.Binary.Result = ParseTarget(lineTokens[3]);

        m_Ops.push_back(op);
    }
    
    void HandleUnaryOp(const std::vector<std::string>& lineTokens, Op& op)
    {
        AutoParam ap(m_Ops);
        ValidateParamCountEQ(lineTokens, 2);
        ParseDataAddress(lineTokens[1], op.u.insr.u.Unary.Operand, ap);
        op.u.insr.u.Unary.Result = ParseTarget(lineTokens[2]);

        m_Ops.push_back(op);
    }
    
    void HandleUnaryTypedOp(const std::vector<std::string>& lineTokens, Op& op)
    {
        AutoParam ap(m_Ops);
        ValidateParamCountEQ(lineTokens, 3);
        ParseDataAddress(lineTokens[1], op.u.insr.u.UnaryTyped.Operand, ap);
        op.u.insr.u.UnaryTyped.Type = DataAddressParser::StringToType(lineTokens[2]);
        op.u.insr.u.UnaryTyped.Result = ParseTarget(lineTokens[3]);

        m_Ops.push_back(op);
    }

    // send accepts variable number of parameters of unknown types
    // each parameter carries type info which is packaged into op
    void HandleSend(const std::vector<std::string>& lineTokens, Op& op)
    {
        // send should have at least one parameter
        ValidateParamCountGE(lineTokens, 1);
        
        auto itToken = lineTokens.begin() + 1;
        
        {
            AutoParam ap(m_Ops);
            // put first param into op itself, other into additional CallParamPack blocks
            if(itToken != lineTokens.end())
            {
                ParseTypedDataAddress(*itToken, op.u.insr.u.Send.Param1, op.u.insr.u.Send.Param1Type, ap);
                ++itToken;
            }

            op.u.insr.u.Send.cParams = lineTokens.size() - 1;
            m_Ops.push_back(op);
        }
        
        while(itToken != lineTokens.end())
        {
            AutoParam ap(m_Ops);
            Op opPack;
            
            if(itToken != lineTokens.end())
            {
                ParseTypedDataAddress(*itToken, opPack.u.TypedParamPack.Param1, opPack.u.TypedParamPack.Param1Type, ap);
                ++itToken;
            }
            if(itToken != lineTokens.end())
            {
                ParseTypedDataAddress(*itToken, opPack.u.TypedParamPack.Param2, opPack.u.TypedParamPack.Param2Type, ap);
                ++itToken;
            }
            
            m_Ops.push_back(opPack);
        }
    }

    void HandleReadEvent(const std::vector<std::string>& lineTokens, Op& op, DataType dt)
    {
        ValidateParamCountEQ(lineTokens, 2);
        auto symbol = m_Symbols.Lookup(lineTokens[1]);
        if(symbol.Kind != SymbolKind::EventField)
            throw std::runtime_error("invalid symbol used as event field");

        op.u.insr.u.ReadEvent.Offset = symbol.u.EventField.Offset;
        op.u.insr.u.ReadEvent.Size = 4;
        op.u.insr.u.ReadEvent.Result = ParseTarget(lineTokens[2]);

        m_Ops.push_back(op);
    }

    void HandleAddKey(const std::vector<std::string>& lineTokens, Op& op)
    {
        AutoParam ap(m_Ops);
        ValidateParamCountEQ(lineTokens, 3);
        ParseDataAddress(lineTokens[1], op.u.insr.u.AddKey.Store, ap);
        ParseDataAddress(lineTokens[2], op.u.insr.u.AddKey.Key, ap);
        ParseDataAddress(lineTokens[3], op.u.insr.u.AddKey.Value, ap);

        m_Ops.push_back(op);
    }

    void HandleRemoveKey(const std::vector<std::string>& lineTokens, Op& op)
    {
        AutoParam ap(m_Ops);
        ValidateParamCountEQ(lineTokens, 2);
        ParseDataAddress(lineTokens[1], op.u.insr.u.RemoveKey.Store, ap);
        ParseDataAddress(lineTokens[2], op.u.insr.u.RemoveKey.Key, ap);

        m_Ops.push_back(op);
    }

    void HandleGetKey(const std::vector<std::string>& lineTokens, Op& op)
    {
        AutoParam ap(m_Ops);
        ValidateParamCountEQ(lineTokens, 2);
        ParseDataAddress(lineTokens[1], op.u.insr.u.GetKey.Store, ap);
        ParseDataAddress(lineTokens[2], op.u.insr.u.GetKey.Key, ap);

        m_Ops.push_back(op);
    }

    struct OpEntry
    {
        const char* psz;
        OpCode code;
    };
    
    struct CmpOp : std::binary_function <const OpEntry&, const char*, bool>
    {
        bool operator()(const OpEntry& a, const char *b) const
        {
            return std::strcmp(a.psz, b) < 0;
        }
    };

    OpCode FindOp(const char* psz)
    {
        // array is sorted by name for lower_bound to work
        static std::array<OpEntry, 31> ops = {{
            { "addDouble", OpCode::AddDouble },
            { "addInt32", OpCode::AddInt32 },
            { "addKey", OpCode::AddKey },
            { "allocObject", OpCode::AllocObject },
            { "allocTuple", OpCode::AllocTuple },
            { "br", OpCode::Br },
            { "call", OpCode::Call },
            { "cmp", OpCode::Cmp },
            { "decInt32", OpCode::DecInt32 },
            { "divDouble", OpCode::DivDouble },
            { "getKey", OpCode::GetKey },
            { "getTupleField", OpCode::GetTupleField },
            { "getTupleFields", OpCode::GetTupleFields },
            { "incInt32", OpCode::IncInt32 },
            { "initObject", OpCode::InitObject },
            { "jump", OpCode::Jump },
            { "land", OpCode::Land },
            { "lor", OpCode::Lor },
            { "mulInt32", OpCode::MulInt32 },
            { "not", OpCode::Not },
            { "readEventInt32", OpCode::ReadEventInt32 },
            { "readEventInt64", OpCode::ReadEventInt64 },
            { "releaseObject", OpCode::ReleaseObject },
            { "removeKey", OpCode::RemoveKey },
            { "return", OpCode::Return },
            { "send", OpCode::Send },
            { "stop", OpCode::Stop },
            { "store", OpCode::Store },
            { "storeObject", OpCode::StoreObject },
            { "subDouble", OpCode::SubDouble },
            { "subInt32", OpCode::SubInt32 },
        }};
        
        auto it = std::lower_bound(ops.begin(), ops.end(), psz, CmpOp());
        if(it == ops.end())
            return OpCode::Undefined;

        return (std::strcmp(psz, it->psz) == 0) ? it->code : OpCode::Undefined;
    }

    AsmReader m_Rdr;
    SymbolTable& m_Symbols;
    std::vector<std::pair<std::string, OpAddress>>& m_PatchExternal;
    std::vector<std::pair<std::string, OpAddress>> m_PatchLabels;
    std::vector<Op>& m_Ops;
    DataAddressParser m_Dap;
};
