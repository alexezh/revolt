// Copyright (c) 2013-2014 Alexandre Grigorovitch (alexezh@gmail.com).
//
// Permission is hereby granted, free of charge, to any person obtaining a
// copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit
// persons to whom the Software is furnished to do so, subject to the
// following conditions:
//
// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
// OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN
// NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
// USE OR OTHER DEALINGS IN THE SOFTWARE.
#pragma once

#include <assert.h>
#include <vector>
#include <functional>
#include <map>
#include <array>
#include <regex>
#include <utility>
#include "op.h"
#include "program.h"
#include "typeinfo.h"
#include "jsonreader.h"
#include "symboltable.h"
#include "asmreader.h"
#include "asmcompiler.h"
#include "asmerror.h"
#include "vartable.h"
#include "typeparser.h"


/*
Asm::Parse takes asm stream as input and produces 3 objects
    Program - contains code and binding info
    TypeStore - contains types used by code
    VarStore - contains global variables references by the code

Asm stream is split into four sections: bindings, types and code.
Bindings section defines input events which script processes
[
	{bindings : [
		{
			input : <event>
			label: <code>
		}
	]},

	{types : [
		{
			name : <name>
			fields : [
                type
			]
		}
	]},

	{globals : [
		{
			name : <name>
			type : <type>
		}
	]},

	{functions : [
		{
			name : <name>
			code : <asm as line>
		}
	]}
]

*/

class Asm
{
public:
    static std::tuple<TypeStore, Program> Load(SymbolTable& st, const std::vector<char>& input)
    {
    	Asm a(st, input);
    	return a.Process();
    }

private:
	Asm(SymbolTable& st, const std::vector<char>& input)
        : m_Rdr(input)
		, m_Symbols(st)
	{
        m_Symbols.AddType("bool", (TypeId)DataType::Boolean);
        m_Symbols.AddType("int32", (TypeId)DataType::Int32);
        m_Symbols.AddType("int64", (TypeId)DataType::Int64);
        m_Symbols.AddType("double", (TypeId)DataType::Double);
        m_Symbols.AddType("string", (TypeId)DataType::String);
	}

    std::tuple<TypeStore, Program> Process()
    {
        Parse();
        PatchNames();
        
        std::vector<std::pair<std::string, OpAddress>> bindings;
        bindings.reserve(m_Bindings.size());
        for(auto& b : m_Bindings)
        {
            auto sym = m_Symbols.Lookup(b.Label);
            if(sym.Kind != SymbolKind::Function)
                throw std::runtime_error("binding must be a function");
            
            bindings.emplace_back(std::make_pair(std::move(b.Symbol), sym.u.Function.Offset));
        }
        
    	return std::make_tuple<TypeStore, Program>(std::move(m_TS),
            Program(std::move(m_Ops), BindingTable(std::move(bindings)), std::move(m_GlobalVars)));
    }
    
    void Parse()
    {
    	if(!(m_Rdr.ReadNext() || m_Rdr.Type() == JsonReader::NodeType::Array))
            throw JsonParseError();

    	while(m_Rdr.ReadNext())
    	{
            if(m_Rdr.Type() == JsonReader::NodeType::EndArray)
                break;
            
            if(m_Rdr.Type() != JsonReader::NodeType::Object)
                throw JsonParseError();
            
            if(!m_Rdr.ReadNext())
                throw JsonParseError();
            
    		if(m_Rdr.Name() == "bindings")
    		{
    			ParseArray([this]() { ParseBinding(); });
    		}
    		else if(m_Rdr.Name() == "types")
    		{
    			ParseArray([this]() { ParseType(); });
    		}
    		else if(m_Rdr.Name() == "globals")
    		{
    			ParseArray([this]() { ParseGlobal(); });
    		}
    		else if(m_Rdr.Name() == "functions")
    		{
    			ParseArray([this]() { ParseFunction(); });
    		}
            else
            {
                throw JsonParseError();
            }

            if(!(m_Rdr.ReadNext() && m_Rdr.Type() == JsonReader::NodeType::EndObject))
                throw JsonParseError();
    	}
    }

    template <typename TElem, class TFunc>
    std::vector<TElem> ParseArray(const TFunc& elemFunc)
    {
    	std::vector<TElem> result;

    	while(m_Rdr.ReadNext())
    	{
    		if(m_Rdr.Type() == JsonReader::NodeType::EndArray)
    			break;
            
            if(m_Rdr.Type() == JsonReader::NodeType::Object)
            {
                result.push_back(elemFunc());
            }
    	}

    	return std::move(result);
    }

    template <class TFunc>
    void ParseArray(const TFunc& elemFunc)
    {
    	while(m_Rdr.ReadNext())
    	{
    		if(m_Rdr.Type() == JsonReader::NodeType::EndArray)
    			return;
            
            if(m_Rdr.Type() == JsonReader::NodeType::Object)
            {
                elemFunc();
            }
    	}
    }

    void ParseBinding()
    {
    	std::string symbol;
    	std::string label;

    	while(m_Rdr.ReadNext())
    	{
            if(m_Rdr.Type() == JsonReader::NodeType::EndObject)
            {
                break;
            }
            
            if(m_Rdr.Type() == JsonReader::NodeType::String)
            {
            	if(m_Rdr.Name() == "symbol")
            		symbol = m_Rdr.TakeValue();
            	else if(m_Rdr.Name() == "label")
            		label = m_Rdr.TakeValue();
            }
        }

        if(symbol.length() == 0 || label.length() == 0)
        	ThrowParsingError();

        m_Bindings.push_back(Binding(std::move(symbol), std::move(label)));
    }
    
    void ParseGlobal()
    {
    	std::string name;
    	TypeId tid = FromDataType(DataType::Undefined);

    	while(m_Rdr.ReadNext())
    	{
            if(m_Rdr.Type() == JsonReader::NodeType::EndObject)
            {
                break;
            }
            
            if(m_Rdr.Type() == JsonReader::NodeType::String)
            {
            	if(m_Rdr.Name() == "name")
            		name = m_Rdr.TakeValue();
            	else if(m_Rdr.Name() == "type")
                {
                    tid = m_Symbols.LookupType(m_Rdr.Value());
                }
            }
            else if(m_Rdr.Type() == JsonReader::NodeType::Object)
            {
            	if(m_Rdr.Name() == "type")
                    tid = TypeParser::Parse(m_Rdr, m_TS, m_Symbols);
            }
        }

        if(name.length() == 0 || tid == FromDataType(DataType::Undefined))
        	ThrowParsingError();
        
        auto varOffset = m_GlobalVars.Add(tid, std::string(name));

        m_Symbols.AddGlobalVar(name.c_str(), varOffset);
    }
    
    void ParseType()
    {
    	std::string name;
        TypeId tid = FromDataType(DataType::Undefined);

    	while(m_Rdr.ReadNext())
    	{
            if(m_Rdr.Type() == JsonReader::NodeType::EndObject)
            {
                break;
            }
            
            if(m_Rdr.Type() == JsonReader::NodeType::String)
            {
                if(m_Rdr.Name() == "name")
                    name = m_Rdr.TakeValue();
            }
            else if(m_Rdr.Type() == JsonReader::NodeType::Object)
            {
            	if(m_Rdr.Name() == "type")
                    tid = TypeParser::Parse(m_Rdr, m_TS, m_Symbols);
            }
        }

        if(name.length() == 0 || tid == FromDataType(DataType::Undefined))
            ThrowParsingError();

        m_Symbols.AddType(std::move(name), tid);
    }
    
    void ParseFunction()
    {
    	std::string name;
    	std::string code;

    	while(m_Rdr.ReadNext())
    	{
            if(m_Rdr.Type() == JsonReader::NodeType::EndObject)
            {
                break;
            }
            
            if(m_Rdr.Type() == JsonReader::NodeType::String)
            {
            	if(m_Rdr.Name() == "name")
            		name = m_Rdr.TakeValue();
            	else if(m_Rdr.Name() == "code")
            		code = m_Rdr.TakeValue();
            }
    	}

    	auto startAddress = ParseAsm(code);
        
        // add symbol to our function table
        m_Symbols.AddFunction(name.c_str(), startAddress);
    }
    
    OpAddress ParseAsm(const std::string& code)
    {
        OpAddress startAddress = AsmCompiler::Parse(m_Symbols, m_PatchNames, code, m_Ops);
        return startAddress;
    }

    void PatchNames()
    {
        for(auto& p : m_PatchNames)
        {
            Op& op = m_Ops[p.second];
            auto symbol = m_Symbols.Lookup(p.first);
            
            switch(op.u.insr.Code)
            {
                case OpCode::Call:
                    if(symbol.Kind != SymbolKind::Function)
                        throw std::runtime_error("symbol is not a function");
                    
                    op.u.insr.u.Call.Address = symbol.u.Function.Offset;
                break;
                case OpCode::Br:
                    if(symbol.Kind != SymbolKind::Label)
                        throw std::runtime_error("symbol is not a label");
                    
                    op.u.insr.u.Br.Else = symbol.u.Label.Offset;
                break;
                default:
                    throw std::runtime_error("unable to patch instruction");
            }
        }
    }
    
    void ThrowParsingError()
    {
		throw JsonParseError();
    }

private:
	struct Binding
	{
		Binding()
		{
		}
		
		Binding(Binding&& b)
		{
			swap(b);
		}

		Binding(std::string&& symbol, std::string&& label)
			: Symbol(std::move(symbol))
			, Label(std::move(label))
		{
		}

		Binding& operator=(Binding&& b)
		{
			swap(b);
            return *this;
		}

		void swap(Binding& b)
		{
			Symbol.swap(b.Symbol);
			Label.swap(b.Label);
		}

		std::string Symbol;
		std::string Label;
		uint16_t LabelOffset;
	};
    
    SymbolTable& m_Symbols;
    JsonReader m_Rdr;
    TypeStore m_TS;
    VarTable m_GlobalVars;
    std::vector<Binding> m_Bindings;

    std::vector<Op> m_Ops;
    PatchCollection m_PatchNames;
};
