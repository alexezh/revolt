// Copyright (c) 2013-2014 Alexandre Grigorovitch (alexezh@gmail.com).
//
// Permission is hereby granted, free of charge, to any person obtaining a
// copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit
// persons to whom the Software is furnished to do so, subject to the
// following conditions:
//
// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
// OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN
// NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
// USE OR OTHER DEALINGS IN THE SOFTWARE.
#pragma once

#include "runtime.h"
#include "datastack.h"
#include "callstack.h"
#include "keystore.h"

class Runner
{
public:
    // we want to allow multiple runners to run the same program
    // so the program is const and passed by ref
    Runner()
        : m_CallStack(16)
        , m_DataStack(4096)
    {
    }
    
    Runner& operator=(Runner&& r)
    {
        m_Runtime = std::move(r.m_Runtime);
        m_CurrentOp = r.m_CurrentOp;
        m_DataStack = std::move(r.m_DataStack);
        m_CallStack = std::move(r.m_CallStack);
        m_Prg = r.m_Prg;
        m_TypeStore = r.m_TypeStore;
        
        m_Globals = std::move(r.m_Globals);
        m_Send = std::move(r.m_Send);
        
        return *this;
    }
    
    // set method to be invoked by @Send call
    void SetSendHandler(std::function<void(const std::vector<TypedValue>&)>&& func)
    {
        m_Send = std::move(func);
    }
    
    // initializes program within runtime
    // - creates globals
    // - calls init handler if provided
    void Init(const Program& prg, const TypeStore& typeStore)
    {
        m_Prg = &prg;
        m_TypeStore = &typeStore;
        m_Globals.resize(m_Prg->GlobalVars().end() - m_Prg->GlobalVars().begin());

        auto init = m_Prg->Bindings().GetInitEntry();
        if(init.first)
        {
            Run(init.second, nullptr, 0);
        }
    }
    
    // returns runtime object which can be used to access memory for global variables
    Runtime& Runtime()
    {
        return m_Runtime;
    }

    // returns current value of global which is guaranteed to be accessible
    // until next Run call. Should not be called from Run
    Value GetGlobalValue(uint16_t offset)
    {
        (void)m_Prg->GlobalVars().Get(offset);
        
        return m_Globals[offset];
    }

    // replaces global value with new value
    void SetGlobalValue(uint16_t offset, TypeId tid, Value v)
    {
        if(m_Prg->GlobalVars().Get(offset) != tid)
            throw std::runtime_error("non-matching type for global");
        
        // TODO: AddRef if type is object
        m_Globals[offset] = v;
    }

    void Run(const char* id, unsigned char* pbEvent, uint32_t cbEvent)
    {
        Run(m_Prg->Bindings().GetEventEntry(id), pbEvent, cbEvent);
    }
    
    void Run(OpOffset entryOffset, unsigned char* pbEvent, uint32_t cbEvent)
    {
        bool fContinue = true;
        m_pEvent = pbEvent;
        m_cbEvent = cbEvent;
        
        // manufacture an entry to match return from top function
        m_CallStack.Push(-1, 0, 0);
        SetCurrentOp(entryOffset);
        
        for(;fContinue;)
        {
            const auto& op = m_Prg->GetOp(m_CurrentOp);
            NextOp();
            
            // keep order the same as structure so compiler can generate optimal assembler
            switch(op.u.insr.Code)
            {
                case OpCode::Undefined:
                    throw std::runtime_error("incorrect error code");
                case OpCode::Nope:
                    break;
                case OpCode::Stop:
                    break;
                case OpCode::Jump:
                    SetCurrentOp(op.u.insr.u.Jump.Address);
                    break;
                case OpCode::FunctionProlog:
                    m_DataStack.AllocStack(op.u.insr.u.FunctionProlog.cVars);
                    break;
                case OpCode::Call:
                    DoCall(op);
                    break;
                case OpCode::Return:
                    if(!DoReturn(op))
                        fContinue = false;
                    break;
                case OpCode::AllocObject:
                    DoAllocObject(op);
                    break;
                case OpCode::AllocTuple:
                    DoAllocTuple(op);
                    break;
                case OpCode::GetTupleField:
                    DoGetTupleField(op);
                    break;
                case OpCode::GetTupleFields:
                    DoGetTupleFields(op);
                    break;
                case OpCode::Store:
                    StoreValue(op.u.insr.u.Store.Destination, LoadValue(op.u.insr.u.Store.Source));
                    break;
                case OpCode::InitObject:
                    DoInitObject(op);
                    break;
                case OpCode::StoreObject:
                    DoStoreObject(op);
                    break;
                case OpCode::ReleaseObject:
                    DoReleaseObject(op);
                    break;
                case OpCode::Br:
                    {
                        auto value = LoadValue(op.u.insr.u.Br.Operand);
                        if(!value.u.Bool)
                            SetCurrentOp(op.u.insr.u.Br.Else);
                    }
                    break;
                case OpCode::Cmp:
                    DoCmp(op);
                    break;
                case OpCode::Not:
                    StoreValue(op.u.insr.u.Unary.Result, !LoadValue(op.u.insr.u.Unary.Operand).u.Bool);
                    break;
                case OpCode::AddInt32:
                    StoreValue(op.u.insr.u.Binary.Result, LoadValue(op.u.insr.u.Binary.Operand1).u.Int32 + LoadValue(op.u.insr.u.Binary.Operand2).u.Int32);
                    break;
                case OpCode::SubInt32:
                    StoreValue(op.u.insr.u.Binary.Result, LoadValue(op.u.insr.u.Binary.Operand1).u.Int32 - LoadValue(op.u.insr.u.Binary.Operand2).u.Int32);
                    break;
                case OpCode::MulInt32:
                    StoreValue(op.u.insr.u.Binary.Result, LoadValue(op.u.insr.u.Binary.Operand1).u.Int32 * LoadValue(op.u.insr.u.Binary.Operand2).u.Int32);
                    break;
                case OpCode::IncInt32:
                    StoreValue(op.u.insr.u.Unary.Operand, LoadValue(op.u.insr.u.Unary.Operand).u.Int32 + 1);
                    break;

                case OpCode::DecInt32:
                    StoreValue(op.u.insr.u.Unary.Operand, LoadValue(op.u.insr.u.Unary.Operand).u.Int32 - 1);
                    break;
                    
                case OpCode::AddDouble:
                    StoreValue(op.u.insr.u.Binary.Result, LoadValue(op.u.insr.u.Binary.Operand1).u.Double + LoadValue(op.u.insr.u.Binary.Operand2).u.Double);
                    break;

                case OpCode::SubDouble:
                    StoreValue(op.u.insr.u.Binary.Result, LoadValue(op.u.insr.u.Binary.Operand1).u.Double - LoadValue(op.u.insr.u.Binary.Operand2).u.Double);
                    break;

                case OpCode::DivDouble:
                    StoreValue(op.u.insr.u.Binary.Result, LoadValue(op.u.insr.u.Binary.Operand1).u.Double / LoadValue(op.u.insr.u.Binary.Operand2).u.Double);
                    break;

                case OpCode::Lor:
                    StoreValue(op.u.insr.u.Binary.Result, LoadValue(op.u.insr.u.Binary.Operand1).u.Bool | LoadValue(op.u.insr.u.Binary.Operand2).u.Bool);
                    break;

                case OpCode::Land:
                    StoreValue(op.u.insr.u.Binary.Result, LoadValue(op.u.insr.u.Binary.Operand1).u.Bool & LoadValue(op.u.insr.u.Binary.Operand2).u.Bool);
                    break;

                case OpCode::ReadEventBoolean:
                case OpCode::ReadEventInt32:
                case OpCode::ReadEventInt64:
                case OpCode::ReadEventDouble:
                case OpCode::ReadEventString:
                    DoReadEvent(op);
                    break;
                case OpCode::Send:
                    DoSend(op);
                    break;

                case OpCode::GetKey:
                    DoGetKey(op);
                    break;
                case OpCode::AddKey:
                    DoAddKey(op);
                    break;
                case OpCode::RemoveKey:
                    DoRemoveKey(op);
                    break;
                case OpCode::SetKey:
                    DoRemoveKey(op);
                    break;

                default:
                    throw std::runtime_error("invalid instruction");
            }
        }
    }

private:
    void DoCall(const Op& op)
    {
        auto oldHead = m_DataStack.GetHead();
        
        // push params on stack
        uint8_t cParams = op.u.insr.u.Call.cParams;
        
        if(cParams > 0)
        {
            m_DataStack.Push(LoadValue(op.u.insr.u.Call.Param1));
            cParams--;
        }
        
        while(cParams > 0)
        {
            const Op& opPack = m_Prg->GetOp(m_CurrentOp);
            NextOp();
            
            if(cParams > 0)
            {
                m_DataStack.Push(LoadValue(opPack.u.ParamPack.Param1));
                cParams--;
            }
            if(cParams > 0)
            {
                m_DataStack.Push(LoadValue(opPack.u.ParamPack.Param2));
                cParams--;
            }
            if(cParams > 0)
            {
                m_DataStack.Push(LoadValue(opPack.u.ParamPack.Param3));
                cParams--;
            }
        }
        
        // restore stack as it was before we pushed parameters
        m_CallStack.Push(m_CurrentOp, m_DataStack.GetBase(), oldHead);

        // set position of first parameter as new base
        m_DataStack.SetPos(oldHead, m_DataStack.GetHead());
        
        SetCurrentOp(op.u.insr.u.Call.Address);
    }
    
    bool DoReturn(const Op& op)
    {
        if(op.u.insr.u.Return.Result.u.Addr.Mode != DataAddressType::Undefined)
        {
            m_Accum = LoadValue(op.u.insr.u.Return.Result);
        }
    
        auto ret = m_CallStack.Pop();
        if(ret.HeadCode == static_cast<uint16_t>(-1))
            return false;

        SetCurrentOp(ret.HeadCode);
        m_DataStack.SetPos(ret.BaseData, ret.HeadData);
        return true;
    }
    
    void DoSend(const Op& op)
    {
        if(!m_Send)
            throw std::runtime_error("Send handler is not set");
        
        m_InvokeData.resize(op.u.insr.u.Send.cParams);
        auto itParam = m_InvokeData.begin();
        if(itParam != m_InvokeData.end())
        {
            itParam->t = op.u.insr.u.Send.Param1Type;
            itParam->v = LoadValue(op.u.insr.u.Send.Param1);
            ++itParam;
        }
        
        while(itParam != m_InvokeData.end())
        {
            const Op& opPack = m_Prg->GetOp(m_CurrentOp);
            NextOp();
            
            if(itParam != m_InvokeData.end())
            {
                itParam->t = opPack.u.TypedParamPack.Param1Type;
                itParam->v = LoadValue(opPack.u.TypedParamPack.Param1);
                ++itParam;
            }
            
            if(itParam != m_InvokeData.end())
            {
                itParam->t = opPack.u.TypedParamPack.Param2Type;
                itParam->v = LoadValue(opPack.u.TypedParamPack.Param2);
                ++itParam;
            }
        }
        

        m_Send(m_InvokeData);
    }
    
    void DoReadEvent(const Op& op)
    {
        uint16_t end = op.u.insr.u.ReadEvent.Offset + op.u.insr.u.ReadEvent.Size;
        if(end < op.u.insr.u.ReadEvent.Offset || end > m_cbEvent)
            throw std::runtime_error("invalid offset for event data");
        
        Value v;
		v.u.Int32 = *reinterpret_cast<int32_t*>(&m_pEvent[op.u.insr.u.ReadEvent.Offset]);
        
        StoreValue(op.u.insr.u.ReadEvent.Result, v);
    }
    
    void DoGetTupleField(const Op& op)
    {
        auto tuple = LoadValue(op.u.insr.u.GetTupleField.Tuple);
        auto index = LoadValue(op.u.insr.u.GetTupleField.Index);
        
        auto tupleObj = m_Runtime.GetObject<Tuple>(tuple.u.Handle);
        
        // Get returns refcounted
        StoreValue(op.u.insr.u.GetTupleField.Result, tupleObj->Get(m_Runtime, index.u.Int32));
    }

    void DoGetTupleFields(const Op& op)
    {
        auto tuple = LoadValue(op.u.insr.u.GetTupleFields.Tuple);
        
        auto tupleObj = m_Runtime.GetObject<Tuple>(tuple.u.Handle);
        uint8_t cParams = op.u.insr.u.GetTupleFields.cParams;
        uint16_t idx = 0;
        
        if(cParams > 0)
        {
            StoreValue(op.u.insr.u.GetTupleFields.Param1, tupleObj->Get(m_Runtime, idx));
            cParams--;
            idx++;
        }

        if(cParams > 0)
        {
            StoreValue(op.u.insr.u.GetTupleFields.Param2, tupleObj->Get(m_Runtime, idx));
            cParams--;
            idx++;
        }
        
        while(cParams > 0)
        {
            const Op& opPack = m_Prg->GetOp(m_CurrentOp);
            NextOp();
            
            if(cParams > 0)
            {
                StoreValue(opPack.u.ParamPack.Param1, tupleObj->Get(m_Runtime, idx));
                cParams--;
                idx++;
            }
            if(cParams > 0)
            {
                StoreValue(opPack.u.ParamPack.Param2, tupleObj->Get(m_Runtime, idx));
                cParams--;
                idx++;
            }
            if(cParams > 0)
            {
                StoreValue(opPack.u.ParamPack.Param3, tupleObj->Get(m_Runtime, idx));
                cParams--;
                idx++;
            }
        }
    }

    void DoAllocObject(const Op& op)
    {
        auto& ti = m_TypeStore->GetType(op.u.insr.u.AllocTuple.Tid);
        auto objectP = m_Runtime.AllocObject<Object>(ti);
        StoreValue(op.u.insr.u.AllocTuple.Result, MakeValue(objectP.first));
    }
    
    void DoAllocTuple(const Op& op)
    {
        auto& ti = m_TypeStore->GetType(op.u.insr.u.AllocTuple.Tid);
        auto tupleP = m_Runtime.AllocObject<Tuple>(ti);
        auto tuple = tupleP.second;
        
        // push params on stack
        uint8_t cParams = op.u.insr.u.AllocTuple.cParams;
        uint16_t idx = 0;
        
        if(cParams > 0)
        {
            tuple->Set(m_Runtime, idx, LoadValue(op.u.insr.u.AllocTuple.Param1));
            cParams--;
            idx++;
        }
        
        while(cParams > 0)
        {
            const Op& opPack = m_Prg->GetOp(m_CurrentOp);
            NextOp();
            
            if(cParams > 0)
            {
                tuple->Set(m_Runtime, idx, LoadValue(opPack.u.ParamPack.Param1));
                cParams--;
                idx++;
            }
            if(cParams > 0)
            {
                tuple->Set(m_Runtime, idx, LoadValue(opPack.u.ParamPack.Param2));
                cParams--;
                idx++;
            }
            if(cParams > 0)
            {
                tuple->Set(m_Runtime, idx, LoadValue(opPack.u.ParamPack.Param3));
                cParams--;
                idx++;
            }
        }
        
        StoreValue(op.u.insr.u.AllocTuple.Result, MakeValue(tupleP.first));
    }

    FORCEINLINE void DoInitObject(const Op& op)
    {
        auto val = LoadValue(op.u.insr.u.Store.Source);
        m_Runtime.AddRefObject(val.u.Handle);
        StoreValue(op.u.insr.u.Store.Destination, val);
    }

    FORCEINLINE void DoStoreObject(const Op& op)
    {
        auto val = LoadValue(op.u.insr.u.Store.Source);
        
        switch(op.u.insr.u.Store.Destination.u.Addr.Mode)
        {
            case DataAddressType::StackBase:
                m_Runtime.AssignHandle(val.u.Handle, m_DataStack.GetBaseValueRef(op.u.insr.u.Store.Destination.u.Addr.Offset).u.Handle);
                return;
            case DataAddressType::Global:
                if(op.u.insr.u.Store.Destination.u.Addr.Offset >= m_Globals.size())
                    throw std::runtime_error("incorrect index");
                m_Runtime.AssignHandle(val.u.Handle, m_Globals[op.u.insr.u.Store.Destination.u.Addr.Offset].u.Handle);
                return;
            case DataAddressType::Accumulator:
            case DataAddressType::Constant:
                throw std::runtime_error("cannot set global or constant");
            default:
                throw std::runtime_error("unknown address format");
        }
    }

    FORCEINLINE void DoReleaseObject(const Op& op)
    {
        auto val = LoadValue(op.u.insr.u.Single.Operand);
        m_Runtime.ReleaseObject(val.u.Handle);
    }
    
    void DoAddKey(const Op& op)
    {
        auto store = LoadValue(op.u.insr.u.AddKey.Store);
        auto key = LoadValue(op.u.insr.u.AddKey.Key);
        auto val = LoadValue(op.u.insr.u.AddKey.Value);
        
        // we assume that value is handle. for normal programs compiler will enforce this
        // for garbage we will execute and fail once something does not match
        auto ks = m_Runtime.GetObject<KeyStore>(store.u.Handle);
        ks->AddKey(m_Runtime, key, val);
    }
    
    void DoRemoveKey(const Op& op)
    {
        auto store = LoadValue(op.u.insr.u.RemoveKey.Store);
        auto key = LoadValue(op.u.insr.u.RemoveKey.Key);
        
        // we assume that value is handle. for normal programs compiler will enforce this
        // for garbage we will execute and fail once something does not match
        auto ks = m_Runtime.GetObject<KeyStore>(store.u.Handle);
        auto val = ks->RemoveKey(m_Runtime, key);
        m_Accum = val;
    }

    void DoGetKey(const Op& op)
    {
        auto store = LoadValue(op.u.insr.u.RemoveKey.Store);
        auto key = LoadValue(op.u.insr.u.RemoveKey.Key);
        
        // we assume that value is handle. for normal programs compiler will enforce this
        // for garbage we will execute and fail once something does not match
        auto ks = m_Runtime.GetObject<KeyStore>(store.u.Handle);
        auto val = ks->GetKey(m_Runtime, key);
        m_Accum = val;
    }

    inline void DoCmp(const Op& op)
    {
        bool res = false;
        
        auto v1 = LoadValue(op.u.insr.u.Cmp.Operand1);
        auto v2 = LoadValue(op.u.insr.u.Cmp.Operand2);
        
        switch(static_cast<DataType>(op.u.insr.u.Cmp.u.Type))
        {
            case DataType::Boolean:
                switch(static_cast<CmpCode>(op.u.insr.u.Cmp.u.Code))
                {
                    case CmpCode::Equal: res = v1.u.Bool == v2.u.Bool; break;
                    case CmpCode::NotEqual: res = v1.u.Bool != v2.u.Bool; break;
                    case CmpCode::Greater: res = v1.u.Bool > v2.u.Bool; break;
                    case CmpCode::GreaterEqual: res = v1.u.Bool >= v2.u.Bool; break;
                    case CmpCode::Less: res = v1.u.Bool < v2.u.Bool; break;
                    case CmpCode::LessEqual: res = v1.u.Bool <= v2.u.Bool; break;
                    default: throw std::runtime_error("incorrect operand");
                }
            break;
            case DataType::Int32:
                switch(static_cast<CmpCode>(op.u.insr.u.Cmp.u.Code))
                {
                    case CmpCode::Equal: res = v1.u.Int32 == v2.u.Int32; break;
                    case CmpCode::NotEqual: res = v1.u.Int32 != v2.u.Int32; break;
                    case CmpCode::Greater: res = v1.u.Int32 > v2.u.Int32; break;
                    case CmpCode::GreaterEqual: res = v1.u.Int32 >= v2.u.Int32; break;
                    case CmpCode::Less: res = v1.u.Int32 < v2.u.Int32; break;
                    case CmpCode::LessEqual: res = v1.u.Int32 <= v2.u.Int32; break;
                    default: throw std::runtime_error("incorrect operand");
                }
            break;
            case DataType::Int64:
                switch(static_cast<CmpCode>(op.u.insr.u.Cmp.u.Code))
                {
                    case CmpCode::Equal: res = v1.u.Int64 == v2.u.Int64; break;
                    case CmpCode::NotEqual: res = v1.u.Int64 != v2.u.Int64; break;
                    case CmpCode::Greater: res = v1.u.Int64 > v2.u.Int64; break;
                    case CmpCode::GreaterEqual: res = v1.u.Int64 >= v2.u.Int64; break;
                    case CmpCode::Less: res = v1.u.Int64 < v2.u.Int64; break;
                    case CmpCode::LessEqual: res = v1.u.Int64 <= v2.u.Int64; break;
                    default: throw std::runtime_error("incorrect operand");
                }
            break;
            case DataType::Double:
                switch(static_cast<CmpCode>(op.u.insr.u.Cmp.u.Code))
                {
                    case CmpCode::Equal: res = v1.u.Double == v2.u.Double; break;
                    case CmpCode::NotEqual: res = v1.u.Double != v2.u.Double; break;
                    case CmpCode::Greater: res = v1.u.Double > v2.u.Double; break;
                    case CmpCode::GreaterEqual: res = v1.u.Double >= v2.u.Double; break;
                    case CmpCode::Less: res = v1.u.Double < v2.u.Double; break;
                    case CmpCode::LessEqual: res = v1.u.Double <= v2.u.Double; break;
                    default: throw std::runtime_error("incorrect operand");
                }
            break;
            default:
                throw std::runtime_error("unsupported type");
        }
        
        StoreValue(op.u.insr.u.Cmp.Result, res);
    }

    FORCEINLINE void SetCurrentOp(size_t co)
    {
        m_CurrentOp = co;
    }
    
    // loads value used on address
    FORCEINLINE Value LoadValue(DataAddress addr)
    {
        switch(addr.u.Addr.Mode)
        {
            case DataAddressType::Accumulator:
                return m_Accum;
            case DataAddressType::StackBase:
                return m_DataStack.GetBaseValue(addr.u.Addr.Offset);
            case DataAddressType::Constant:
                return NextValue();
            case DataAddressType::Global:
                if(addr.u.Addr.Offset >= m_Globals.size())
                    throw std::runtime_error("incorrect offset to global");
                
                return m_Globals[addr.u.Addr.Offset];
            default:
                throw std::runtime_error("invalid address mode");
        }
    }
    
    FORCEINLINE void StoreValue(DataAddress addr, bool val)
    {
        Value v;
        v.u.Bool = val;
        StoreValue(addr, v);
    }

    FORCEINLINE void StoreValue(DataAddress addr, int32_t val)
    {
        Value v;
        v.u.Int32 = val;
        StoreValue(addr, v);
    }

    FORCEINLINE void StoreValue(DataAddress addr, int64_t val)
    {
        Value v;
        v.u.Int64 = val;
        StoreValue(addr, v);
    }
    
    FORCEINLINE void StoreValue(DataAddress addr, double val)
    {
        Value v;
        v.u.Double = val;
        StoreValue(addr, v);
    }

    FORCEINLINE void StoreValue(DataAddress addr, Value val)
    {
        switch(addr.u.Addr.Mode)
        {
            case DataAddressType::StackBase:
                m_DataStack.SetBaseValue(addr.u.Addr.Offset, val);
                return;
            case DataAddressType::Global:
                if(addr.u.Addr.Offset >= m_Globals.size())
                    throw std::runtime_error("incorrect index");
                m_Globals[addr.u.Addr.Offset] = val;
                return;
            case DataAddressType::Accumulator:
            case DataAddressType::Constant:
                throw std::runtime_error("cannot set global or constant");
            default:
                throw std::runtime_error("unknown address format");
        }
    }

    FORCEINLINE void NextOp()
    {
        m_CurrentOp++;
    }
    
    FORCEINLINE Value NextValue()
    {
        return m_Prg->GetValue(m_CurrentOp++);
    }
    
private:
    ::Runtime m_Runtime;
    size_t m_CurrentOp = 0;
    DataStack m_DataStack;
    CallStack m_CallStack;
    const Program* m_Prg;
    const TypeStore* m_TypeStore;
    
    unsigned char* m_pEvent = nullptr;
    uint32_t m_cbEvent = 0;
    
    // global vars accessible by index
    std::vector<Value> m_Globals;
    
    // data for invoke operations
    std::vector<TypedValue> m_InvokeData;
    
    // accumulator value used for storing
    // temporary result of computations
    Value m_Accum;
    
    std::function<void(const std::vector<TypedValue>&)> m_Send;
};
