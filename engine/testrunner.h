#pragma once

#include <iostream>
#include <fstream>
#include <chrono>
#include "testdef.h"

void RunTests(TestLibrary& lib, const char* pszTest = nullptr)
{
    auto& tests = lib.Tests();
    
    for(auto& test : tests)
    {
        if(pszTest != nullptr)
        {
            if(test->Name() != pszTest)
                continue;
        }
        
        test->Setup();
        test->Run();
        test->Teardown();
    }
}
