// Copyright (c) 2013-2014 Alexandre Grigorovitch (alexezh@gmail.com).
//
// Permission is hereby granted, free of charge, to any person obtaining a
// copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit
// persons to whom the Software is furnished to do so, subject to the
// following conditions:
//
// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
// OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN
// NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
// USE OR OTHER DEALINGS IN THE SOFTWARE.
#pragma once

#include <unordered_map>

enum class SymbolKind
{
    Function,
    GlobalVar,
    EventField,
    Label,
    Type,
};

struct Symbol
{
    SymbolKind Kind;
    union
    {
        struct
        {
            uint16_t Offset;
        } GlobalVar;
        struct
        {
            uint16_t Offset;
        } Function;
        struct
        {
            uint16_t Offset;
        } EventField;
        struct
        {
            uint16_t Offset;
        } Label;
        struct
        {
            TypeId Tid;
        } Type;
    } u;
};

class SymbolTable
{
public:
    SymbolTable()
    {
    }

    SymbolTable(SymbolTable&& t)
        : m_Symbols(std::move(t.m_Symbols))
        , m_Parent(std::move(t.m_Parent))
    {
    }
    
    SymbolTable& operator=(SymbolTable&& src)
    {
        m_Symbols = std::move(src.m_Symbols);
        m_Parent = std::move(src.m_Parent);
        return *this;
    }

    // nested symbol table used for labels in functions
    // to avoid labels from one function to leak into other
    SymbolTable(const SymbolTable* parent)
        : m_Parent(parent)
    {
    }

    void AddGlobalVar(const char* name, uint16_t offset)
    {
        Symbol s;
        s.Kind = SymbolKind::GlobalVar;
        s.u.GlobalVar.Offset = offset;
        m_Symbols.emplace(name, s);
    }
    void AddFunction(const char* name, uint16_t offset)
    {
        Symbol s;
        s.Kind = SymbolKind::Function;
        s.u.Function.Offset = offset;
        m_Symbols.emplace(name, s);
    }
    void AddEventField(const char* name, uint16_t offset)
    {
        Symbol s;
        s.Kind = SymbolKind::EventField;
        s.u.EventField.Offset = offset;
        m_Symbols.emplace(name, s);
    }
    
    template <typename TS>
    void AddLabel(TS&& name, uint16_t offset)
    {
        Symbol s;
        s.Kind = SymbolKind::Label;
        s.u.Label.Offset = offset;
        m_Symbols.emplace(std::forward<TS>(name), s);
    }
    
    template <typename TS>
    void AddType(TS&& name, TypeId tid)
    {
        Symbol s;
        s.Kind = SymbolKind::Type;
        s.u.Type.Tid = tid;
        m_Symbols.emplace(std::forward<TS>(name), s);
    }
    
    TypeId LookupType(const std::string& v) const
    {
        auto& sym = Lookup(v);
        if(sym.Kind != SymbolKind::Type)
            throw std::runtime_error("incorrect symbol type");
        
        return sym.u.Type.Tid;
    }

    const Symbol& Lookup(const std::string& v) const
    {
        auto it = m_Symbols.find(v);
        if(it == m_Symbols.end())
        {
            if(m_Parent)
            {
                return m_Parent->Lookup(v);
            }
            else
            {
                throw std::runtime_error("invalid symbol");
            }
        }
        
        return it->second;
    }
    
private:
    const SymbolTable* m_Parent = nullptr;
    std::unordered_map<std::string, Symbol> m_Symbols;
};

