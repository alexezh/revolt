// Copyright (c) 2013-2014 Alexandre Grigorovitch (alexezh@gmail.com).
//
// Permission is hereby granted, free of charge, to any person obtaining a
// copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit
// persons to whom the Software is furnished to do so, subject to the
// following conditions:
//
// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
// OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN
// NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
// USE OR OTHER DEALINGS IN THE SOFTWARE.
#pragma once

#include <regex>
#include "symboltable.h"

class DataAddressParser
{
public:
    DataAddressParser(const SymbolTable& st)
        : m_Symbols(st)
        , m_Bp("^bp\\+(\\d+)")
        , m_BpTyped("^bp\\+(\\d+):([A-Za-z_][A-Za-z_0-9]*)")
        , m_RaTyped("^ra:([A-Za-z_][A-Za-z_0-9]*)")
        , m_IConst("^(\\d+)")
        , m_IConstTyped("^(\\d+):([A-Za-z_][A-Za-z_0-9]*)")
        , m_FConst("^(\\d+).(\\d*)")
        , m_FConstTyped("^(\\d+).(\\d*):([A-Za-z_][A-Za-z_0-9]*)")
        , m_BoolTyped("^((true)|(false)):([A-Za-z_][A-Za-z_0-9]*)")
        , m_Global("([A-Za-z_][A-Za-z_0-9]*)")
        , m_GlobalTyped("([A-Za-z_][A-Za-z_0-9]*):([A-Za-z_][A-Za-z_0-9]*)")
    {
    }
    
    // address formats
    //  bp+N
    //  ra
    //  Name
    //  N
    std::pair<DataAddress, Value> Parse(const std::string& param)
    {
        std::smatch ma;
        
        if(std::regex_match(param, ma, m_Bp))
        {
            return std::make_pair(MakeDataAddress(DataAddressType::StackBase, std::stoi(ma[1])), MakeValue());
        }
        else if(std::strcmp(param.c_str(), "ra") == 0)
        {
            return std::make_pair(MakeDataAddress(DataAddressType::Accumulator, 0), MakeValue());
        }
        else if(param == "true")
        {
            return std::make_pair(MakeDataAddress(DataAddressType::Constant, 0), MakeValue(true));
        }
        else if(param == "false")
        {
            return std::make_pair(MakeDataAddress(DataAddressType::Constant, 0), MakeValue(false));
        }
        else if(std::regex_match(param, ma, m_Global))
        {
            auto symbol = m_Symbols.Lookup(ma[1]);
            if(symbol.Kind == SymbolKind::GlobalVar)
            {
                return std::make_pair(MakeDataAddress(DataAddressType::Global, symbol.u.GlobalVar.Offset), MakeValue());
            }
            else
            {
                throw std::runtime_error("invalid symbol used as address");
            }
        }
        else if(std::regex_match(param, ma, m_IConst))
        {
            return std::make_pair(MakeDataAddress(DataAddressType::Constant, 0), MakeValue(std::stoi(ma[1])));
        }
        else if(std::regex_match(param, ma, m_FConst))
        {
            return std::make_pair(MakeDataAddress(DataAddressType::Constant, 0), MakeValue(std::stod(ma[1])));
        }
        else
        {
            throw std::runtime_error("incorrect data address format");
        }
    }
    
    // address formats
    //  bp+N
    //  ra
    //  Name
    //  N
    std::tuple<DataAddress, Value, TypeId> ParseTyped(const std::string& param)
    {
        std::smatch ma;
        
        if(std::regex_match(param, ma, m_BpTyped))
        {
            return std::make_tuple(MakeDataAddress(DataAddressType::StackBase, std::stoi(ma[1])), MakeValue(), FromDataType(StringToType(ma[2])));
        }
        else if(std::regex_match(param, ma, m_RaTyped))
        {
            return std::make_tuple(MakeDataAddress(DataAddressType::Accumulator, 0), MakeValue(), FromDataType(StringToType(ma[1])));
        }
        else if(std::regex_match(param, ma, m_BoolTyped))
        {
            if(ma[1] == "true")
            {
                return std::make_tuple(MakeDataAddress(DataAddressType::Constant, 0), MakeValue(true), FromDataType(DataType::Boolean));
            }
            else
            {
                return std::make_tuple(MakeDataAddress(DataAddressType::Constant, 0), MakeValue(false), FromDataType(DataType::Boolean));
            }
        }
        else if(std::regex_match(param, ma, m_GlobalTyped))
        {
            auto symbol = m_Symbols.Lookup(ma[1]);
            if(symbol.Kind != SymbolKind::GlobalVar)
                throw std::runtime_error("invalid symbol used as address");

            auto tid = m_Symbols.LookupType(ma[2]);
            return std::make_tuple(MakeDataAddress(DataAddressType::Global, symbol.u.GlobalVar.Offset), MakeValue(), tid);
        }
        else if(std::regex_match(param, ma, m_IConstTyped))
        {
            return std::make_tuple(MakeDataAddress(DataAddressType::Constant, 0), MakeValue(std::stoi(ma[1])), FromDataType(DataType::Int32));
        }
        else if(std::regex_match(param, ma, m_FConstTyped))
        {
            return std::make_tuple(MakeDataAddress(DataAddressType::Constant, 0), MakeValue(std::stod(ma[1])), FromDataType(DataType::Double));
        }
        else
        {
            throw std::runtime_error("incorrect data address format");
        }
    }

    // TODO: lookup in symbol table
    static DataType StringToType(const std::string& s)
    {
        if(s == "bool")
        {
            return DataType::Boolean;
        }
        else if(s == "int32")
        {
            return DataType::Int32;
        }
        else if(s == "int64")
        {
            return DataType::Int64;
        }
        else if(s == "double")
        {
            return DataType::Double;
        }
        else if(s == "void")
        {
            return DataType::Void;
        }
        else
        {
            throw std::runtime_error("unsupported type");
        }
    }
    
private:
    const SymbolTable& m_Symbols;
    std::regex m_Bp;
    std::regex m_BpTyped;
    std::regex m_RaTyped;
    std::regex m_Global;
    std::regex m_GlobalTyped;
    std::regex m_IConst;
    std::regex m_IConstTyped;
    std::regex m_FConst;
    std::regex m_FConstTyped;
    std::regex m_BoolTyped;
};
