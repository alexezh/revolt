// Copyright (c) 2013-2014 Alexandre Grigorovitch (alexezh@gmail.com).
//
// Permission is hereby granted, free of charge, to any person obtaining a
// copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit
// persons to whom the Software is furnished to do so, subject to the
// following conditions:
//
// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
// OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN
// NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
// USE OR OTHER DEALINGS IN THE SOFTWARE.
#include <iostream>
#include <string>
#include <atomic>
#include <fstream>
#include <chrono>

#include "asm.h"
#include "runner.h"
#include "keystore.h"

#include "testdef.h"
#include "testrunner.h"

typedef std::chrono::high_resolution_clock my_clock;

class StopWatch
{
public:
    void Start()
    {
        m_Start = my_clock::now();
    }

    void Stop()
    {
        m_Stop = my_clock::now();
    }
    
    void Report(const char* prefix)
    {
        std::chrono::microseconds scripttime =
            std::chrono::duration_cast<std::chrono::microseconds>(m_Stop - m_Start);

        std::cout << prefix << " " << scripttime.count() << "\n";
    }
    
private:
    my_clock::time_point m_Start;
    my_clock::time_point m_Stop;
};

std::vector<char> LoadFile(const std::string& szName)
{
    std::vector<char> data;
    std::ifstream file;
    file.exceptions(std::ifstream::badbit
                      | std::ifstream::failbit
                      | std::ifstream::eofbit);
	file.open(szName, std::ios_base::binary | std::ios_base::in);
    file.seekg(0, std::ios::end);
    std::streampos length(file.tellg());
    if (length)
    {
        file.seekg(0, std::ios::beg);
        data.resize(static_cast<std::size_t>(length));
        file.read(&data.front(), static_cast<std::size_t>(length));
    }
    
    return std::move(data);
}

struct Event1
{
    int32_t f1;
    int32_t f2;
};


void ProcessClick1(Runtime& rt, KeyStore* pStore1)
{
	pStore1->AddKey(rt, MakeValue(1), MakeValue(2));
}

void ProcessClick2(Runtime& rt, KeyStore* pStore1, KeyStore* pStore2)
{
	Value v = pStore1->RemoveKey(rt, MakeValue(1));
    auto tuple = rt.GetObject<Tuple>(v.u.Handle);
	if(!tuple->Get(rt, 0).u.Bool)
    {
        rt.ReleaseObject(v.u.Handle);
		return;
    }

	pStore2->AddKey(rt, MakeValue(3), MakeValue(1));
    rt.ReleaseObject(v.u.Handle);
}

void ProcessClick3(Runtime& rt, KeyStore* pStore2, std::function<void()>& func)
{
	Value v = pStore2->RemoveKey(rt, MakeValue(1));
    auto tuple = rt.GetObject<Tuple>(v.u.Handle);
	if(!tuple->Get(rt, 0).u.Bool)
    {
        rt.ReleaseObject(v.u.Handle);
		return;
    }
    
	func();
    rt.ReleaseObject(v.u.Handle);
}

void TestPerfJoin(Runner& rt, Program& prg)
{
    int cCalls = 0;

    rt.SetSendHandler([&cCalls](const std::vector<TypedValue>& params)
    {
        assert(params[2].v.u.Int32 == 1);
        cCalls++;
#if 0
        std::cout << "Number " << params.size() << "\n";
        for(auto& p : params)
        {
            std::cout << "Type " << static_cast<int>(p.t) << " ";
            switch (p.t)
            {
                case DataType::Boolean: std::cout << "Val " << p.v.u.Bool; break;
                case DataType::Int32: std::cout << "Val " << p.v.u.Int32; break;
                case DataType::Int64: std::cout << "Val " << p.v.u.Int64; break;
                default:
                    std::cout << "Unknown value type";
            }
            std::cout << "\n";
        }
#endif
    });
    
    Event1 e1 = { 0, 1 };
    OpOffset offset2 = prg.Bindings().GetEventEntry("2");
    OpOffset offset3 = prg.Bindings().GetEventEntry("3");
    OpOffset offset4 = prg.Bindings().GetEventEntry("4");
    StopWatch sw;
    
    sw.Start();
    for(int i = 0; i < 1000000; i++)
    {
        rt.Run(offset2 /*"2"*/, reinterpret_cast<unsigned char*>(&e1), sizeof(e1));
        rt.Run(offset3 /*"3"*/, reinterpret_cast<unsigned char*>(&e1), sizeof(e1));
        rt.Run(offset4 /*"4"*/, reinterpret_cast<unsigned char*>(&e1), sizeof(e1));
    }
    sw.Stop();

    TestAssert::IsTrue(cCalls == 1000000);

    sw.Report("Script");
    
    // run native code
    auto offsetStore1 = prg.GlobalVars().FindOffset("PendingButtonClicks");
    auto offsetStore2 = prg.GlobalVars().FindOffset("PendingAdapterClicks");
    
    auto store1 = rt.GetGlobalValue(offsetStore1);
    auto store2 = rt.GetGlobalValue(offsetStore2);
    auto objStore1 = rt.Runtime().GetObject<KeyStore>(store1.u.Handle);
    auto objStore2 = rt.Runtime().GetObject<KeyStore>(store2.u.Handle);


    sw.Start();
    cCalls = 0;
    std::function<void()> send = [&cCalls]() { cCalls++; };
    for(int i = 0; i < 1000000; i++)
    {
        ProcessClick1(rt.Runtime(), objStore1);
        ProcessClick2(rt.Runtime(), objStore1, objStore2);
        ProcessClick3(rt.Runtime(), objStore2, send);
    }
    sw.Stop();
    sw.Report("Native");

    TestAssert::IsTrue(cCalls == 1000000);

#if 0
    
    start = my_clock::now();
    std::atomic<int> i;
    for(i = 0; i < 1000000; i++)
        Foo(i);
    
    end = my_clock::now();
    
    std::chrono::microseconds cpptime =
        std::chrono::duration_cast<std::chrono::microseconds>(end - start);

    // insert code here...
    std::cout << "Script " << scripttime.count() << " Cpp " << cpptime.count();
#endif
}

void TestPerfGroup(Runner& rt, Program& prg)
{
    int cCalls = 0;

    rt.SetSendHandler([&cCalls](const std::vector<TypedValue>& params)
    {
        TestAssert::IsTrue(params[0].v.u.Int32 == 90);
        cCalls++;
    });
    
    Event1 e1 = { 1, 10 };
    Event1 e2 = { 2, 30 };
    OpOffset offset5 = prg.Bindings().GetEventEntry("5");
    OpOffset offset6 = prg.Bindings().GetEventEntry("6");
    StopWatch sw;
    
    sw.Start();
    for(int i = 0; i < 1000000; i++)
    {
        rt.Run(offset5, reinterpret_cast<unsigned char*>(&e1), sizeof(e1));
        rt.Run(offset5, reinterpret_cast<unsigned char*>(&e2), sizeof(e2));
        rt.Run(offset5, reinterpret_cast<unsigned char*>(&e2), sizeof(e2));
        rt.Run(offset5, reinterpret_cast<unsigned char*>(&e2), sizeof(e2));
    }
    //rt.Run(offset6, reinterpret_cast<unsigned char*>(&e2), sizeof(e2));
    sw.Stop();
    sw.Report("Script");
}

void TestPerf(std::string dir)
{
    SymbolTable symbols;
    
    symbols.AddEventField("XamlAdapterClick!ButtonId", 0);
    symbols.AddEventField("XamlAdapterClick!Tcid", 0);
    symbols.AddEventField("OfficeSpaceAction!ParentTcid", 0);
    symbols.AddEventField("OfficeSpaceAction!Tcid", 0);
    symbols.AddEventField("XamlButtonClick!InputType", 0);
    symbols.AddEventField("XamlButtonClick!ButtonId", 0);
    symbols.AddEventField("GroupEvent!Field1", 0);
    symbols.AddEventField("GroupEvent!Field2", 4);
    
    std::vector<char> inputData = LoadFile(combine_path(dir, "perfkeystore.reva"));
    auto result = Asm::Load(symbols, inputData);

    Runner rt;
    rt.Init(std::get<1>(result), std::get<0>(result));
    
    TestPerfJoin(rt, std::get<1>(result));
    TestPerfGroup(rt, std::get<1>(result));
}

Value PendingButtonClicks; // input type
Value PendingAdapterClicks; // input type

// base class for different script tests
// loads script and initializes runner.
class ScriptTest : public Test
{
private:
	ScriptTest(const ScriptTest&);
	ScriptTest* operator=(const ScriptTest&);

public:
	ScriptTest()
	{
	}

	ScriptTest(ScriptTest&& test)
	{
		m_File = std::move(test.m_File);
		m_Prg = std::move(test.m_Prg);
		m_TS = std::move(test.m_TS);
		m_Rt = std::move(test.m_Rt);
		m_Symbols = std::move(test.m_Symbols);
	}

    ScriptTest(const std::string& file, std::string&& name)
        : Test(std::move(name))
        , m_File(file)
    {
    }

    void Setup() override
    {
        std::vector<char> inputData = LoadFile(m_File);
        auto result = Asm::Load(m_Symbols, inputData);
        m_Prg.swap(std::get<1>(result));
        m_TS.swap(std::get<0>(result));
        m_Rt.Init(m_Prg, m_TS);
    }

    std::string m_File;
    Program m_Prg;
    TypeStore m_TS;
    Runner m_Rt;
    SymbolTable m_Symbols;
};

// tests execute all functions exposed from script
// each function calls Send with 2 parameters; parameters should be equal for test to pass
class CompTest : public ScriptTest
{
public:
    CompTest(CompTest&& test)
        : ScriptTest(std::move(test))
    {
    }

    CompTest(const std::string& file, std::string&& name)
        : ScriptTest(file, std::move(name))
    {
    }
    void Run() override
    {
        m_Rt.SetSendHandler([](const std::vector<TypedValue>& params)
        {
            TestAssert::IsTrue(params.size() == 2);
            TestAssert::IsTrue(params[0].t == params[1].t);
            TestAssert::IsTrue(CompareValues(params[0].t, params[0].v, params[1].v));
        });
        
        for(auto& event : m_Prg.Bindings().Events())
        {
            Event1 e1;
            m_Rt.Run(event.second, reinterpret_cast<unsigned char*>(&e1), sizeof(e1));
            std::cout << event.first << "\n";
        }
    }
};

// tests execute all functions exposed from script
// each function calls Send twice. First with real parameters second with verifications values
// compare two sets
class Comp2Test : public ScriptTest
{
public:
    Comp2Test(const std::string& file, std::string&& name)
        : ScriptTest(file, std::move(name))
    {
    }

    Comp2Test(Comp2Test&& test)
        : ScriptTest(std::move(test))
    {
    }

    void Run() override
    {
        m_Rt.SetSendHandler([this](const std::vector<TypedValue>& params)
        {
            if(!m_GotReal)
            {
                m_RealValues.assign(params.begin(), params.end());
                m_GotReal = true;
            }
            else
            {
                m_TestValues.assign(params.begin(), params.end());
            }
        });
        
        for(auto& event : m_Prg.Bindings().Events())
        {
            Event1 e1;
            
            m_RealValues.resize(0);
            m_TestValues.resize(0);
            m_GotReal = false;
            
            m_Rt.Run(event.second, reinterpret_cast<unsigned char*>(&e1), sizeof(e1));
            
            TestAssert::IsTrue(m_RealValues.size() > 0 && m_RealValues.size() == m_TestValues.size());
            for(size_t i = 0; i < m_RealValues.size(); i++)
            {
                TestAssert::IsTrue(CompareValues(m_RealValues[i].t, m_RealValues[i].v, m_TestValues[i].v));
            }

            std::cout << event.first << "\n";
        }
    }
    
    std::vector<TypedValue> m_RealValues;
    std::vector<TypedValue> m_TestValues;
    bool m_GotReal = false;
};

// Test fails if Send(false) called
// Passes otherwise
class IsTrueTest : public ScriptTest
{
public:
    IsTrueTest(IsTrueTest&& test)
        : ScriptTest(std::move(test))
    {
    }
    IsTrueTest(const std::string& file, std::string&& name)
        : ScriptTest(file, std::move(name))
    {
    }
    void Run() override
    {
        m_Rt.SetSendHandler([this](const std::vector<TypedValue>& params)
        {
            TestAssert::IsTrue(false);
        });
        
        for(auto& event : m_Prg.Bindings().Events())
        {
            Event1 e1;
            
            m_Rt.Run(event.second, reinterpret_cast<unsigned char*>(&e1), sizeof(e1));
            
            TestAssert::IsTrue(m_Rt.Runtime().AllocatedObjects() == 0);
            std::cout << event.first << "\n";
        }
    }
    
    std::vector<TypedValue> m_RealValues;
    std::vector<TypedValue> m_TestValues;
    bool m_GotReal = false;
};

// tests execute functions exposed from script
// for each function compares value provided by send with number of objects
// in white/black lists
class GcTest : public ScriptTest
{
public:
    GcTest(GcTest&& test)
        : ScriptTest(std::move(test))
    {
    }
    GcTest(const std::string& file, std::string&& name)
        : ScriptTest(file, std::move(name))
    {
    }
    void Run() override
    {
        m_Rt.SetSendHandler([this](const std::vector<TypedValue>& params)
        {
            m_Values.assign(params.begin(), params.end());
        });
        
        for(auto& event : m_Prg.Bindings().Events())
        {
            Event1 e1;
            
            m_Values.resize(0);
            auto initialCount = m_Rt.Runtime().AllocatedObjects();
            
            m_Rt.Run(event.second, reinterpret_cast<unsigned char*>(&e1), sizeof(e1));
            
            TestAssert::IsTrue(m_Values.size() == 1);
            TestAssert::IsTrue(m_Values[0].v.u.Int32 == m_Rt.Runtime().AllocatedObjects() - initialCount);

            std::cout << event.first << "\n";
        }
    }
    
    std::vector<TypedValue> m_Values;
};

void TestFunc(const std::string& dir)
{
    TestLibrary lib;
    
    lib.AddTest(ScriptTest(dir + "/testsend.reva", "sendTest"))
        .AddMethod<ScriptTest>("verifySend", [](ScriptTest& t)
        {
            Event1 e1;
            t.m_Rt.SetSendHandler([](const std::vector<TypedValue>& params)
            {
                TestAssert::IsTrue(params.size() == 1);
                TestAssert::IsTrue(params[0].t == FromDataType(DataType::Int32));
                TestAssert::IsTrue(params[0].v.u.Int32 == 1);
            });
            t.m_Rt.Run("sendIntConst", reinterpret_cast<unsigned char*>(&e1), sizeof(e1));
        }).AddMethod<ScriptTest>("verifySend2", [](ScriptTest& t)
        {
            Event1 e1;
            t.m_Rt.SetSendHandler([](const std::vector<TypedValue>& params)
            {
                TestAssert::IsTrue(params.size() == 2);
                TestAssert::IsTrue(params[0].t == FromDataType(DataType::Int32));
                TestAssert::IsTrue(params[0].v.u.Int32 == 1);
                TestAssert::IsTrue(params[1].t == FromDataType(DataType::Int32));
                TestAssert::IsTrue(params[1].v.u.Int32 == 2);
            });
            t.m_Rt.Run("send2IntConst", reinterpret_cast<unsigned char*>(&e1), sizeof(e1));
        })
        .AddTest(Comp2Test(dir + "/testtuple.reva", "testtuple"))
        .AddTest(ScriptTest(dir + "/testglobals.reva", "testglobals"))
        .AddMethod<ScriptTest>("setGlobal", [](ScriptTest& t)
        {
            Event1 e1;
            t.m_Rt.SetSendHandler([](const std::vector<TypedValue>& params)
            {
                TestAssert::IsTrue(params.size() == 3);
                TestAssert::IsTrue(params[0].t == FromDataType(DataType::Int32));
                TestAssert::IsTrue(params[0].v.u.Int32 == 1);
                // FIXME. type of global gets overwritten in compiler
                TestAssert::IsTrue(params[1].t == FromDataType(DataType::Int32));
                TestAssert::IsTrue(params[1].v.u.Int64 == 2);
                TestAssert::IsTrue(params[2].t == FromDataType(DataType::Double));
                TestAssert::IsTrue(params[2].v.u.Double == 3.0);
            });
            t.m_Rt.Run("setGlobal", reinterpret_cast<unsigned char*>(&e1), sizeof(e1));
            t.m_Rt.Run("getGlobal", reinterpret_cast<unsigned char*>(&e1), sizeof(e1));
        })
        .AddMethod<ScriptTest>("globalBetweenRuns", [](ScriptTest& t)
        {
            Event1 e1;
            t.m_Rt.SetSendHandler([](const std::vector<TypedValue>& params)
            {
                TestAssert::IsTrue(params.size() == 3);
                TestAssert::IsTrue(params[0].t == FromDataType(DataType::Int32));
                TestAssert::IsTrue(params[0].v.u.Int32 == 1);
                // FIXME. type of global gets overwritten in compiler
                TestAssert::IsTrue(params[1].t == FromDataType(DataType::Int32));
                TestAssert::IsTrue(params[1].v.u.Int64 == 2);
                TestAssert::IsTrue(params[2].t == FromDataType(DataType::Double));
                TestAssert::IsTrue(params[2].v.u.Double == 3.0);
            });
            t.m_Rt.Run("setGlobal", reinterpret_cast<unsigned char*>(&e1), sizeof(e1));
            t.m_Rt.Run("getGlobal", reinterpret_cast<unsigned char*>(&e1), sizeof(e1));
        })
        .AddMethod<ScriptTest>("changeGlobal", [](ScriptTest& t)
        {
            Event1 e1;
    
            // after first set; change the value of global
            t.m_Rt.SetSendHandler([](const std::vector<TypedValue>& params)
            {
            });
            t.m_Rt.Run("setGlobal", reinterpret_cast<unsigned char*>(&e1), sizeof(e1));

            t.m_Rt.SetSendHandler([](const std::vector<TypedValue>& params)
            {
                TestAssert::IsTrue(params.size() == 3);
                TestAssert::IsTrue(params[0].t == FromDataType(DataType::Int32));
                TestAssert::IsTrue(params[0].v.u.Int32 == 2);
            });
            
            auto offsetInt32 = t.m_Prg.GlobalVars().FindOffset("g_Int32");
            t.m_Rt.SetGlobalValue(offsetInt32, FromDataType(DataType::Int32), MakeValue(2));
            t.m_Rt.Run("getGlobal", reinterpret_cast<unsigned char*>(&e1), sizeof(e1));
        })
        .AddMethod<ScriptTest>("tupleGlobal", [](ScriptTest& t)
        {
            Event1 e1;
    
            auto sym = t.m_Symbols.Lookup("MyTuple");
            TestAssert::IsTrue(sym.Kind == SymbolKind::Type);

            // after first set; change the value of global
            t.m_Rt.SetSendHandler([&sym](const std::vector<TypedValue>& params)
            {
                TestAssert::IsTrue(params.size() == 1);
                TestAssert::IsTrue(params[0].t == sym.u.Type.Tid);
            });
            t.m_Rt.Run("setGlobalTuple", reinterpret_cast<unsigned char*>(&e1), sizeof(e1));

            t.m_Rt.SetSendHandler([&sym, &t](const std::vector<TypedValue>& params)
            {
                TestAssert::IsTrue(params.size() == 1);
                TestAssert::IsTrue(params[0].t == sym.u.Type.Tid);
                auto myTupleObj = t.m_Rt.Runtime().GetObject<Tuple>(params[0].v.u.Handle);
                TestAssert::IsTrue(myTupleObj->Get(t.m_Rt.Runtime(), 0).u.Int32 == 2);
            });
            
            // change tuple value
            auto offsetTuple = t.m_Prg.GlobalVars().FindOffset("g_Tuple");
            auto myTupleHandle = t.m_Rt.GetGlobalValue(offsetTuple);
            auto myTupleObj = t.m_Rt.Runtime().GetObject<Tuple>(myTupleHandle.u.Handle);
            myTupleObj->Set(t.m_Rt.Runtime(), 0, MakeValue(2));
            
            // t.m_Rt.SetGlobalValue(offsetTuple, FromDataType(DataType::Int32), MakeValue(2));
            t.m_Rt.Run("getGlobalTuple", reinterpret_cast<unsigned char*>(&e1), sizeof(e1));
        })
        .AddTest(CompTest(dir + "/testarithmetic.reva", "arithmetic"))
        .AddTest(CompTest(dir + "/testfunc.reva", "functions"))
        .AddTest(CompTest(dir + "/testflow.reva", "flow"))
        .AddTest(GcTest(dir + "/testgc.reva", "gcTest"))
        .AddTest(CompTest(dir + "/testkeystore.reva", "keystore"));
        //.AddTest(ScriptTest(dir + "/perfkeystore.reva", "perfKetStore"))
        //.AddMethod<ScriptTest>("keyStore", [](ScriptTest& t)
        //{
            // PerfKeyStore();
        //});

    RunTests(lib, "testglobals");
}

int main(int argc, const char * argv[])
{
    // load input file
    if(argc < 3)
    {
        std::cout << "revolt options dir\n";
        std::cout << "Dir:";
        std::cout << " Dir parameter specifies a directory for input files\n";
        std::cout << "Options:\n";
        std::cout << " -test1       perf test";
        std::cout << " -func        functionality test";
        exit(-1);
    }
    
    if(true || strcmp(argv[1], "-perf") == 0)
    {
        TestPerf(argv[2]);
    }
    else if(strcmp(argv[1], "-func") == 0)
    {
        TestFunc(argv[2]);
    }

    return 0;
}

