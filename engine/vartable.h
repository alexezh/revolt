// Copyright (c) 2013-2014 Alexandre Grigorovitch (alexezh@gmail.com).
//
// Permission is hereby granted, free of charge, to any person obtaining a
// copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit
// persons to whom the Software is furnished to do so, subject to the
// following conditions:
//
// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
// OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN
// NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
// USE OR OTHER DEALINGS IN THE SOFTWARE.
#pragma once

#include <vector>

// VarTable is a collection of variables identified by Id
class VarTable
{
public:
    typedef std::vector<std::pair<TypeId, std::string>>::const_iterator const_iterator;
    
    uint16_t Add(TypeId id, std::string&& name)
    {
        size_t idx = m_Vars.size();
        m_Vars.push_back(std::make_pair(std::move(id), std::move(name)));
        return static_cast<uint16_t>(idx);
    }
    
    TypeId Get(uint16_t offset) const
    {
        if(offset >= m_Vars.size())
            throw std::runtime_error("invalid type index");
        return m_Vars[offset].first;
    }

    // find offset for a type by name
    uint16_t FindOffset(const std::string& name) const
    {
        auto it = std::find_if(m_Vars.begin(), m_Vars.end(), [&name](const std::pair<TypeId, std::string>& e)
        {
            return e.second == name;
        });
        
        if(it == m_Vars.end())
            throw std::runtime_error("unknown name");
            
        return static_cast<uint16_t>(it - m_Vars.begin());
    }
    
    void swap(VarTable& v)
    {
        m_Vars.swap(v.m_Vars);
    }
    
    const_iterator begin() const
    {
        return m_Vars.begin();
    }

    const_iterator end() const
    {
        return m_Vars.end();
    }
    
private:
    std::vector<std::pair<TypeId, std::string>> m_Vars;
};


