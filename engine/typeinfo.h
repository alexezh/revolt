// Copyright (c) 2013-2014 Alexandre Grigorovitch (alexezh@gmail.com).
//
// Permission is hereby granted, free of charge, to any person obtaining a
// copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit
// persons to whom the Software is furnished to do so, subject to the
// following conditions:
//
// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
// OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN
// NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
// USE OR OTHER DEALINGS IN THE SOFTWARE.
#pragma once

#include <vector>
#include <memory>
#include <algorithm>
#include "op.h"

class Object;
class TypeInfo;
class TypeStore;
class Runtime;

// runtime type information
// objects are allocated by TypeStore so the constructor is protected
class TypeInfo
{
    friend class TypeStore;
public:
    typedef void (*ConstructHandler)(const TypeInfo&, Object*, size_t);
    typedef void (*DestructHandler)(Runtime&, Object*);
    typedef uint16_t (*GetSizeHandler)(const TypeInfo&);
    typedef void (*InitType)(TypeStore&, std::vector<const TypeInfo*>&);
   
private:
	TypeInfo(TypeInfo&);

protected:
    TypeInfo(DataType type)
        : m_Base(type)
        , m_Id(FromDataType(type))
    {
        m_Size = 0;
    }

    TypeInfo(TypeId id,
                DataType type,
                std::vector<const TypeInfo*>&& children,
                ConstructHandler construct,
                DestructHandler destruct,
                GetSizeHandler getsize)
        : m_Id(id)
        , m_Base(type)
        , m_Children(std::move(children))
        , m_Construct(construct)
        , m_Destruct(destruct)
    {
        m_Size = getsize(*this);
        for(auto& child : m_Children)
        {
            if(child->IsObject())
            {
                m_hasChildObjects = true;
                break;
            }
        }
    }

	void MoveFrom(TypeInfo&& info)
	{
		m_Base = info.m_Base;
		m_Id = info.m_Id;
		m_Children = std::move(info.m_Children);
		m_Size = info.m_Size;
		m_Construct = info.m_Construct;
		m_Destruct = info.m_Destruct;
		m_hasChildObjects = info.m_hasChildObjects;
	}

public:
	TypeInfo()
	{
	}

	TypeInfo(TypeInfo&& info)
	{
		MoveFrom(std::move(info));
	}

	TypeInfo& operator=(TypeInfo&& info)
	{
		MoveFrom(std::move(info));
		return *this;
	}

    FORCEINLINE DataType Base() const
    {
        return m_Base;
    }

    FORCEINLINE TypeId Id() const
    {
        return m_Id;
    }
    
    FORCEINLINE bool IsObject() const
    {
        return static_cast<uint8_t>(m_Base) >= static_cast<uint8_t>(DataType::Object);
    }
    
    // return size of type (0 for string)
    FORCEINLINE uint16_t Size() const
    {
        return m_Size;
    }
    
    FORCEINLINE const std::vector<const TypeInfo*>& Children() const
    {
        return m_Children;
    }

    FORCEINLINE const TypeInfo& GetChildAt(uint16_t idx) const
    {
        return *m_Children[idx];
    }

    FORCEINLINE uint16_t GetChildrenCount() const
    {
        return static_cast<uint16_t>(m_Children.size());
    }

    FORCEINLINE void Construct(const TypeInfo& ti, Object* pObj, size_t cbObj) const
    {
        if(m_Construct)
            m_Construct(ti, pObj, cbObj);
    }

    FORCEINLINE void Destruct(Runtime& rt, Object* pObj) const
    {
        if(m_Destruct)
            m_Destruct(rt, pObj);
    }
    
    FORCEINLINE bool HasChildObjects() const
    {
        return m_hasChildObjects;
    }
    
private:
    TypeInfo(const TypeInfo&);
    
    DataType m_Base;
    TypeId m_Id;
    std::vector<const TypeInfo*> m_Children;
    uint16_t m_Size;
    ConstructHandler m_Construct = nullptr;
    DestructHandler m_Destruct = nullptr;
    bool m_hasChildObjects = false;
};

// manages TypeId -> TypeInfo mapping for a program
class TypeStore
{
private:
	TypeStore(const TypeStore&);
	TypeStore& operator=(const TypeStore&);

public:
    TypeStore()
    {
        m_Types.push_back(std::unique_ptr<TypeInfo>(new TypeInfo(DataType::Undefined)));
        m_Types.push_back(std::unique_ptr<TypeInfo>(new TypeInfo(DataType::Boolean)));
        m_Types.push_back(std::unique_ptr<TypeInfo>(new TypeInfo(DataType::Int32)));
        m_Types.push_back(std::unique_ptr<TypeInfo>(new TypeInfo(DataType::Int64)));
        m_Types.push_back(std::unique_ptr<TypeInfo>(new TypeInfo(DataType::Double)));
        m_Types.push_back(std::unique_ptr<TypeInfo>(new TypeInfo(DataType::String)));
        
        assert(m_Types[(int)DataType::Undefined]->Base() == DataType::Undefined);
        assert(m_Types[(int)DataType::Boolean]->Base() == DataType::Boolean);
        assert(m_Types[(int)DataType::Int32]->Base() == DataType::Int32);
        assert(m_Types[(int)DataType::Int64]->Base() == DataType::Int64);
        assert(m_Types[(int)DataType::Double]->Base() == DataType::Double);
        assert(m_Types[(int)DataType::String]->Base() == DataType::String);
    }
    
    TypeStore(TypeStore&& store)
    {
        m_Types.swap(store.m_Types);
    }

    TypeStore& operator=(TypeStore&& store)
    {
        m_Types = std::move(store.m_Types);
        return *this;
    }
    
    void swap(TypeStore& store)
    {
        m_Types.swap(store.m_Types);
    }
    
    const TypeInfo& GetStockType(DataType dt)
    {
        if((int)dt > (int)DataType::String)
        {
            throw std::runtime_error("GetStockType only supported for primitive types");
        }
        
        return *(m_Types[(int)dt].get());
    }
    
    const TypeInfo& GetType(TypeId id) const
    {
        if(id >= m_Types.size())
            throw std::out_of_range("type index out of range");
        
        return *(m_Types[id].get());
    }

    // Lots of find operations would result in N^2
    // We might optimize at some point
    std::pair<bool, TypeId> FindType(DataType baseType,
                    const std::vector<TypeId>& types)
    {
        auto it = std::find_if(m_Types.begin(), m_Types.end(), [baseType, &types](const std::unique_ptr<TypeInfo>& ti) -> bool
        {
            if(baseType != ti->Base())
                return false;
            
            if(types.size() != ti->GetChildrenCount())
                return false;
            
            for(uint16_t i = 0; i < ti->GetChildrenCount(); i++)
            {
                if(types[i] != ti->GetChildAt(i).Id())
                    return false;
            }

            return true;
        });
        
        if(it == m_Types.end())
            return std::make_pair(false, FromDataType(DataType::Undefined));
        
        return std::make_pair(true, (*it)->Id());
    }
    
    TypeId CreateType(DataType baseType,
                    const std::vector<TypeId>& types,
                    TypeInfo::InitType init,
                    TypeInfo::ConstructHandler construct,
                    TypeInfo::DestructHandler destruct,
                    TypeInfo::GetSizeHandler getSize)
    {
        if(!(baseType == DataType::Tuple || baseType == DataType::KeyStore))
            throw std::runtime_error("invalid base type");
        
        std::vector<const TypeInfo*> typeObjs;
        typeObjs.reserve(types.size());
        
        for(auto& tid : types)
        {
            typeObjs.push_back(&GetType(tid));
        }
        
        if(init)
            init(*this, typeObjs);

        TypeId id = static_cast<TypeId>(m_Types.size());
        std::unique_ptr<TypeInfo> ti(new TypeInfo(id, baseType, std::move(typeObjs), construct, destruct, getSize));
        
        m_Types.push_back(std::move(ti));
        
        return id;
    }
    
private:
    std::vector<std::unique_ptr<TypeInfo>> m_Types;
};


