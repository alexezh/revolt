// Copyright (c) 2013-2014 Alexandre Grigorovitch (alexezh@gmail.com).
//
// Permission is hereby granted, free of charge, to any person obtaining a
// copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit
// persons to whom the Software is furnished to do so, subject to the
// following conditions:
//
// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
// OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN
// NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
// USE OR OTHER DEALINGS IN THE SOFTWARE.
#pragma once

#include <vector>
#include <unordered_map>
#include <tuple>
#include <algorithm>
#include "config.h"
#include "op.h"
#include "vartable.h"
#include "bindingtable.h"

class Program
{
public:
    Program()
    {
    }

    Program(Program&& prg)
    {
        swap(prg);
    }

    Program(std::vector<Op>&& ops, BindingTable&& bindings, VarTable&& globalVars)
        : m_Bindings(std::move(bindings))
        , m_GlobalVars(std::move(globalVars))
    {
        m_Ops = new Op[ops.size()];
        m_Size = ops.size();
        memcpy(m_Ops, &ops[0], ops.size() * sizeof(Op));
    }
    
    ~Program()
    {
        if(m_Ops)
            delete m_Ops;
    }

    void swap(Program& prg)
    {
        m_Bindings.swap(prg.m_Bindings);
        m_GlobalVars.swap(prg.m_GlobalVars);
        std::swap(m_Ops, prg.m_Ops);
        std::swap(m_Size, prg.m_Size);
    }
    
    Program& operator=(Program&& prg)
    {
        swap(prg);
        return *this;
    }
    
    const VarTable& GlobalVars() const { return m_GlobalVars; }
    const BindingTable& Bindings() const { return m_Bindings; }

	FORCEINLINE const Op& GetOp(size_t index) const
    {
        return m_Ops[index];
    }

    FORCEINLINE const Value& GetValue(size_t index) const
    {
        return m_Ops[index].u.value;
    }

private:
    VarTable m_GlobalVars;
    BindingTable m_Bindings;
    size_t m_Size = 0;
    Op* m_Ops = nullptr;
};

