// Copyright (c) 2013-2014 Alexandre Grigorovitch (alexezh@gmail.com).
//
// Permission is hereby granted, free of charge, to any person obtaining a
// copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit
// persons to whom the Software is furnished to do so, subject to the
// following conditions:
//
// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
// OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN
// NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
// USE OR OTHER DEALINGS IN THE SOFTWARE.
#pragma once

#include <vector>

// callstack records operation to return to
class CallStack
{
public:
    struct Entry
    {
        OpOffset HeadCode;
        size_t BaseData;
        size_t HeadData;
    };

    CallStack(size_t maxSize)
    {
        m_CallStack.resize(maxSize);
    }
    
    inline void Push(OpOffset offset, size_t baseData, size_t headData)
    {
        if(m_Head == m_CallStack.size())
            throw std::runtime_error("stack is too deep");
        
        m_CallStack[m_Head] = { offset, baseData, headData };
        m_Head++;
    }
    
    inline Entry Pop()
    {
        if(m_Head == 0)
            throw std::runtime_error("invalid pop");
        
        m_Head--;
        return m_CallStack[m_Head];
    }

private:
    size_t m_Head = 0;
    std::vector<Entry> m_CallStack;
};

