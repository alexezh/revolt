// Copyright (c) 2013-2014 Alexandre Grigorovitch (alexezh@gmail.com).
//
// Permission is hereby granted, free of charge, to any person obtaining a
// copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit
// persons to whom the Software is furnished to do so, subject to the
// following conditions:
//
// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
// OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN
// NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
// USE OR OTHER DEALINGS IN THE SOFTWARE.
#pragma once

#include <list>
#include <unordered_map>
#include "object.h"
#include "tuple.h"
#include "runtime.h"
#include "valueutils.h"

// cache for 8 values
class CacheBlock
{
public:
    CacheBlock(const TypeInfo& keyType, const TypeInfo& valueType)
        : m_KeyType(keyType)
        , m_ValueType(valueType)
    {
        // fill m_LRU with initial indexes
        for(int i = 0; i < 8; i++)
        {
            m_LRU |= (i | 0x8) << i*4;
        }
    }
    
    struct Entry
    {
        Entry()
        {
        }
        Value value;
        Value key;
        uint32_t hash;
    };
    
    void Add(Runtime& rt, Value key, Value data, uint32_t hash)
    {
        uint32_t dropIndex = m_LRU & 0xf;
        if((dropIndex & 0x8) == 0)
        {
            ClearEntry(rt, dropIndex);
        }
        else
        {
            dropIndex ^= 0x8;
        }
        
        assert(dropIndex < 8);
        Entry& e = m_Elems[dropIndex];
        e.key = key;
        e.value = data;
        e.hash = hash;
        
        m_LRU = (m_LRU >> 4) | (dropIndex << 28);
    }
    
    int Find(Value key, uint32_t hash)
    {
        uint32_t lru = m_LRU;
        for(int i = 7; i >= 0; i--)
        {
            uint32_t idx = (lru >> (i << 2)) & 0xf;
            if(idx & 0x8)
                continue;
            
            Entry& e = m_Elems[idx];
            if(e.hash == hash)
            {
                if(CompareValues(m_KeyType.Id(), e.key, key))
                {
                    return i;
                }
            }
        }
        
        return -1;
    }

    void Remove(Runtime& rt, int pos)
    {
        uint32_t idx = (m_LRU >> (pos << 2)) & 0xf;
        assert(idx < 8);
        ClearEntry(rt, idx);
        m_LRU |= 0x8 << (pos << 2);
    }

    Value GetAt(int pos)
    {
        uint32_t idx = (m_LRU >> (pos << 2)) & 0xf;
        assert(idx < 8);
        return m_Elems[idx].value;
    }

private:
    void ClearEntry(Runtime& rt, int idx)
    {
    }
    const TypeInfo& m_KeyType;
    const TypeInfo& m_ValueType;
    uint32_t m_LRU = 0; // 4 bits per entry. 0-7 is index. high bit indicates that value is empty
    Entry m_Elems[8];
};

// provides fast cache for 8 state key/values pairs
// when adding new value, oldest value is pushed out
class KeyStore : public Object
{
public:
    static const DataType Type = DataType::KeyStore;

    KeyStore(const TypeInfo& keyType, const TypeInfo& valueType, const TypeInfo& returnType)
        : m_KeyType(keyType)
        , m_ValueType(valueType)
        , m_ReturnType(returnType)
        , m_Cache(keyType, valueType)
    {
    }
    
    ~KeyStore()
    {
    }

    void AddKey(Runtime& rt, Value key, Value data)
    {
        m_Cache.Add(rt, key, data, HashValue(m_KeyType, key));
    }
    
    Value RemoveKey(Runtime& rt, Value key)
    {
        int idx = m_Cache.Find(key, HashValue(m_KeyType, key));
        if(idx == -1)
        {
            auto obj = rt.AllocObject<Tuple>(m_ReturnType);
            obj.second->Set(rt, 0, MakeValue(true));
            return MakeValue(obj.first);
        }

        auto obj = rt.AllocObject<Tuple>(m_ReturnType);
        obj.second->Set(rt, 1, m_Cache.GetAt(idx));

        m_Cache.Remove(rt, idx);

        return MakeValue(obj.first);
    }

    Value GetKey(Runtime& rt, Value key)
    {
        int idx = m_Cache.Find(key, HashValue(m_KeyType, key));
        if(idx == -1)
        {
            auto obj = rt.AllocObject<Tuple>(m_ReturnType);
            obj.second->Set(rt, 0, MakeValue(true));
            return MakeValue(obj.first);
        }

        auto obj = rt.AllocObject<Tuple>(m_ReturnType);
        obj.second->Set(rt, 1, m_Cache.GetAt(idx));

        return MakeValue(obj.first);
    }

    void Clear(Runtime& rt)
    {
/*
        bool releaseValue = m_pTi->GetChildAt(1).IsObject();
        for(auto& e : m_Map)
        {
            if(releaseValue)
            {
                // check that value is object
                assert(false);
                rt.ReleaseObject(e.second->value.u.Handle);
            }
            delete e.second;
        }
        m_Map.clear();
  */
    }
    
private:
    const TypeInfo& m_KeyType;
    const TypeInfo& m_ValueType;
    const TypeInfo& m_ReturnType;
    
    CacheBlock m_Cache;
};

void InitKeyStoreType(TypeStore& store, std::vector<const TypeInfo*>& fields)
{
    if(fields.size() != 2)
        throw std::runtime_error("incorrect number of types");
    
    std::vector<TypeId> nullFields = { FromDataType(DataType::Boolean), fields[1]->Id() };
    auto v = store.FindType(DataType::Tuple, nullFields);
    
    if(!v.first)
        throw std::runtime_error("type should be defined in module?!");
    
    auto& ti = store.GetType(v.second);
    fields.push_back(&ti);
}

void ConstructKeyStore(const TypeInfo& type, Object* pObj, size_t cbObj)
{
	if (cbObj != sizeof(KeyStore))
		throw std::runtime_error("invalid object size");

	if (!(type.Base() == DataType::KeyStore && type.Children().size() == 3))
		throw std::runtime_error("invalid type info");

	// call constructor
	new(pObj)KeyStore(*type.Children()[0], *type.Children()[1], *type.Children()[2]);
}

void DestructKeyStore(Runtime& rt, Object* p)
{
    KeyStore* pObj = reinterpret_cast<KeyStore*>(p);
    pObj->Clear(rt);
	pObj->~KeyStore();
}

uint16_t GetSizeKeyStore(const TypeInfo&)
{
    return static_cast<uint16_t>(sizeof(KeyStore));
}

