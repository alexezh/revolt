# Copyright (c) 2013-2014 Alexandre Grigorovitch (alexezh@gmail.com).

# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to permit
# persons to whom the Software is furnished to do so, subject to the
# following conditions:

# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN
# NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
# DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
# OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
# USE OR OTHER DEALINGS IN THE SOFTWARE.

import sys
import revlex
import revparse

from ply import *
from revast import *
from asmop import *
from symboltable import *


class AsmGenerator:
    def __init__(self,f : Function):
        self.function = f
        self.output = [] # of AsmOp
        self.varScopes = [] # array of variable scope. each scope is array of storage locations
        self.nextLabel = 1
        self.freeTemps = []
        self.usedTemps = []
        self.nextTemp = 0
        self.maxLabel = 0

    def allocVar(self, t:TypeId, temp=True, name=None) -> StorageLocation:
        if t == None:
            raise Exception('Need type to allocate storage')

        if len(self.freeTemps) == 0:
            loc = StorageLocation(LocationType.Stack, location=self.nextTemp);
            self.nextTemp += 1
        else:
            loc = self.freeTemps.pop()

        loc.type = t
        loc.temp = temp
        loc.varName = name

        self.add(AsmOp(AsmOpKind.AllocStorage, params=[loc]))
        self.usedTemps.append(loc)
        self.log('allocVar {0}'.format(loc.qualifiedLocation()))
        return loc

    def allocAcc(self, t:TypeId) -> StorageLocation:
        if t == None:
            raise Exception('Need type to allocate storage')

        loc = StorageLocation(LocationType.Accumulator)
        loc.type = t
        loc.temp = True
        self.add(AsmOp(AsmOpKind.AllocStorage, params=[loc]))
        return loc

    def freeTemp(self, loc:StorageLocation):
        if loc.temp:
            self.freeStorage(loc)

    def freeStorage(self, loc: StorageLocation):
        self.log('freeStorage {0} {1} {2}'.format(loc.varName, loc.qualifiedLocation(), loc.type))
        if loc.locType == LocationType.Stack:
            self.freeTemps.append(loc)

        if loc.locType == LocationType.Stack or loc.locType == LocationType.Accumulator:
            self.add(AsmOp(AsmOpKind.FreeStorage, params=[loc]))

            try:
                self.usedTemps.remove(loc)
            except ValueError:
                pass


    # free all local variables
    def freeAllLocals(self):
        self.generateFunctionExit()
        self.usedTemps = []

    def generateFunctionExit(self):
        self.log('Temps: {0}'.format(len(self.usedTemps)))
        for loc in self.usedTemps:
            if loc.type.isObject():
                self.add(AsmOp(AsmOpKind.FreeStorage, params=[loc]))

    def findLocalVar(self, name:str) -> StorageLocation:
        for loc in self.usedTemps:
            if hasattr(loc, 'varName') and loc.varName == name:
                return loc

        raise Exception('Symbol \'{0}\' not found'.format(name))


    def allocLabel(self) -> string:
        self.maxLabel += 1
        return self.function.name + '_' + str(self.maxLabel);

    def generate(self):
        self.varScopes.append([])

        # alloc parameters in order
        for p in self.function.params:
            self.log(str(p))
            rv = self.allocVar(p[0], temp=False, name=p[1])
            self.varScopes[len(self.varScopes)-1].append(rv)

        # process all expressions
        for e in self.function.body:
            self.processExp(e)

        appendReturn = False
        if len(self.function.body) > 0:
            lastExp = self.function.body[len(self.function.body)-1];
            if lastExp.kind != ExpressionKind.RETURN:
                appendReturn = True;
        else:
            appendReturn = True;

        if appendReturn:
            self.freeAllLocals()

            if not self.function.type.isVoid():
                raise Exception('Return missing')

            # just add return at the end
            self.output.append(AsmOp(AsmOpKind.Return))

        return self.output

    def add(self,op):
        self.log(str(op))
        self.output.append(op)

    def processCompound(self, ast:Expression):
        self.log('processCompound. children={0}'.format(len(ast.data)))
        for e in ast.data:
            self.processExp(e)

    def processBinaryExp(self, op:AsmOpKind, ast:Expression) -> StorageLocation:
        r1 = self.processExp(ast.data[0])
        r2 = self.processExp(ast.data[1])
        rr = self.allocVar(ast.getType())
        self.add(AsmOp(op, type=ast.getType(), params=[r1.qualifiedLocation(), r2.qualifiedLocation(), rr.qualifiedLocation()]))
        self.freeTemp(r1)
        self.freeTemp(r2)
        return rr

    def processEqualityExp(self, ast:Expression) -> StorageLocation:
        op = ast.data[1]
        r1 = self.processExp(ast.data[0])
        r2 = self.processExp(ast.data[2])
        rr = self.allocVar(MakeBoolType())
        opcode = None;
        if op == '==':
            opcode = 'eq'
        elif op == '<':
            opcode = 'lt'
        elif op == '<=':
            opcode = 'le'
        elif op == '>':
            opcode = 'gt'
        elif op == '>=':
            opcode = 'ge'
        elif op == '!=':
            opcode = 'ne'
        else:
            raise Exception('Unknown comparison operator', op)

        self.add(AsmOp(AsmOpKind.Cmp, type=ast.getType(), params=[opcode, r1, r2, rr]))
        self.freeTemp(r1)
        self.freeTemp(r2)
        return rr

    def processUnaryExp(self, ast:Expression) -> StorageLocation:
        r1 = self.processExp(ast.data[1])
        rr = self.allocVar(ast.getType());
        if isinstance(ast.data[0], str) and ast.data[0] == '!':
            self.add(AsmOp(AsmOpKind.Not, params=[r1, rr]))
        elif isinstance(ast.data[0], str) and ast.data[0] == '-':
            r1 = StorageLocation(LocationType.Constant, '0')
            r2 = self.processExp(ast.data[1])
            self.add(AsmOp(AsmOpKind.Sub, type=ast.getType(), params=[r1, r2, rr]))
            self.freeTemp(r1)
            self.freeTemp(r2)
        else:
            raise Exception('Unsupported unary expression {0}'.format(ast))
        return rr

    # functions returns result in 'ra' parameter
    def processCall(self, ast: Expression) -> StorageLocation:
        values = []
        for p in ast.data[1]:
            pt = p.getType()
            if pt.name == None:
                raise Exception('Type should have a name. {0}'.format(p))

            # process child op and associate type with it
            # in the future we might return type from op
            res = self.processExp(p)
            res.type = pt
            values.append(res)

        # check if we call tuple constructor, generate allocTuple instead of call
        if ast.callSymbol().kind == SymbolKind.Type:
            rr = self.allocVar(ast.getType())
            if ast.getType().category == TypeCategory.Tuple:
                self.add(AsmOp(AsmOpKind.AllocTuple, params=[rr, escapedTypeName(ast.name)] + values))
            elif ast.getType().category == TypeCategory.Container:
                self.add(AsmOp(AsmOpKind.AllocObject, params=[rr, escapedTypeName(ast.name)] + values))
            else:
                raise Exception('Unsupported type {0!s}'.format(ast.getType()))
            return rr
        else:
            if ast.callSymbol().kind == SymbolKind.NativeFunction:
                self.add(AsmOp(ast.callSymbol().asmOp, params=values))
            elif ast.callSymbol().kind == SymbolKind.Function:
                self.add(AsmOp(AsmOpKind.Call, params=[ast.name] + values))
            else:
                raise Exception('Syntax error. Cannot call {0} symbol'.format(ast.symbol.name))

            for v in values:
                self.freeTemp(v)

            return self.allocAcc(ast.getType())

    # a.b only used for events and replaced with readevent
    def processPeriod(self, ast: Expression) -> StorageLocation:
        r = self.allocVar(ast.getType())

        # TODO. cleanup type logic here
        # we should really get type from data[1] instead of stamping it on root object
        if ast.symbol.type.category == TypeCategory.Nullable:
            r1 = self.processExp(ast.data[0])
            idx = 0;

            if ast.data[1] == 'IsNull':
                idx = 0
            elif ast.data[1] == 'Value':
                idx = 1
            else:
                raise Exception('Invalid field for Nullable')

            self.add(AsmOp(AsmOpKind.GetTupleField, params=[r1, idx, r]))

        elif ast.symbol.kind == SymbolKind.Event:
            self.log('process event field. Type {0}'.format(ast.getType()))
            self.add(AsmOp(AsmOpKind.ReadEvent, type=ast.getType(), params=[ast.symbol.name + '!' + ast.data[1], r]))
        else:
            raise Exception('Symbol {0} not supported'.format(ast.symbol))

        return r

    def processIf(self, ast:Expression):
        res = self.processExp(ast.data[0])
        labelElse = self.allocLabel()
        labelEnd = self.allocLabel()
        self.add(AsmOp(AsmOpKind.Br, params=[res, labelElse]))
        self.processExp(ast.data[1])
        # if we have else, insert else block
        if ast.data[2] != None:
            self.add(AsmOp(AsmOpKind.Jump, params=[labelEnd]))
            self.add(AsmOp(AsmOpKind.Label, params=[labelElse + ':']))
            self.processExp(ast.data[2])
            self.add(AsmOp(AsmOpKind.Label, params=[labelEnd + ':']))
        else:
            self.add(AsmOp(AsmOpKind.Label, params=[labelElse + ':']))

        self.freeTemp(res)

    def processAssign(self, ast:Expression):
        left = ast.leftAsList()

        # that's a problem... we free value of variable even so it is not temp
        # we need var to be var and only free it when we out of scope
        res = self.processExp(ast.data[2])

        if ast.data[0].kind == ExpressionKind.LET:
            for v in left:
                rv = self.allocVar(v.getType(), temp=False, name=v.data)
                if not isinstance(v.data, str): 
                    raise Exception('Data must be of string type')
                self.log('add variable {0} location {1}'.format(rv.varName, rv.qualifiedLocation()))
                self.varScopes[len(self.varScopes)-1].append(rv)

        if len(left) > 1:
            self.log('processAssign. source={0} dest=tuple'.format(res))
            # read tuple into number of variables
            params = []
            params.append(res.qualifiedLocation())
            for v in left:
                params.append(self.processExp(v))
            self.add(AsmOp(AsmOpKind.GetTupleFields, params=params))
        else:
            self.log('processAssign. source={0} dest=var'.format(res))
            target = self.processExp(left[0])
            if target.location == LocationType.Constant:
                raise Exception('Target location must be variable')
            self.add(AsmOp(AsmOpKind.Store, type=ast.getType(), params=[target, res], init=ast.data[0].kind == ExpressionKind.LET))

        self.freeTemp(res)

    def processMakeTuple(self, ast:Expression):
        rr = self.allocVar(ast.getType())
        res = []

        for c in ast.childrenAsList():
            res.append(self.processExp(c))

        # result might be already tuple?
        self.log('results', len(res))

        # if we have more than one expression on the right, allocate tuple and store the result
        # possible optimization for future. we might expand tuple in the next operation so there might be no 
        # reason for storing it
        self.add(AsmOp(AsmOpKind.AllocTuple, params=[rr, ast.getType().name] + [r.qualifiedLocation() for r in res]))

        # free stack allocated by results of child ops
        for r in res:
            self.freeTemp(r)

        return rr

    def processReturn(self, ast:Expression):
        if ast.data == None:
            self.generateFunctionExit()
            self.add(AsmOp(AsmOpKind.Return))
        else:
            res = self.processExp(ast.data)
            # todo. protect res from releasing
            self.freeAllLocals()
            self.add(AsmOp(AsmOpKind.Return, params=[res]))
            self.freeTemp(res)

    # processes expression. returns address for result (register, constant, etc)
    def processExp(self, ast: Expression):
        # print('process {0}'.format(ast.kind))
        if not isinstance(ast, Expression):
            pdb.set_trace()
            raise Exception('Function body should only contain expressions', ast)

        if ast.kind == ExpressionKind.RETURN:
            self.processReturn(ast)
        elif ast.kind == ExpressionKind.SEQ:
            raise Exception('Internal error. .SEQ should not be used')
        elif ast.kind == ExpressionKind.ASSIGN:
            return self.processAssign(ast)
        elif ast.kind == ExpressionKind.MAKE_TUPLE:
            return self.processMakeTuple(ast)
        elif ast.kind == ExpressionKind.CONDITIONAL:
            raise Exception('Not implemented')
        elif ast.kind == ExpressionKind.IF:
            return self.processIf(ast)
        elif ast.kind == ExpressionKind.LOGICAL_OR:
            return self.processBinaryExp(AsmOpKind.Lor, ast)
        elif ast.kind == ExpressionKind.LOGICAL_AND:
            return self.processBinaryExp(AsmOpKind.Land, ast)
        elif ast.kind == ExpressionKind.INCLUSIVE_OR:
            return self.processBinaryExp('or', ast)
        elif ast.kind == ExpressionKind.EXCLUSIVE_OR:
            return self.processBinaryExp('xor', ast)
        elif ast.kind == ExpressionKind.AND:
            return self.processBinaryExp('and', ast)
        elif ast.kind == ExpressionKind.EQUALITY:
            return self.processEqualityExp(ast)
        elif ast.kind == ExpressionKind.IDENT:
            if ast.symbol.kind == SymbolKind.LocalVariable:
                return self.findLocalVar(ast.data)
            elif ast.symbol.kind == SymbolKind.GlobalVariable:
                return StorageLocation(LocationType.Global, location=ast.symbol.name)
            elif ast.symbol.kind == SymbolKind.ParamVariable:
                return self.findLocalVar(ast.data)
            else:
                raise Exception('Unsupported operation for {0}'.format(ast.symbol))
        elif ast.kind == ExpressionKind.CONSTANT:
            # just return constant; we do not have to load contants to registers
            return StorageLocation(LocationType.Constant, ast.data[0])
        elif ast.kind == ExpressionKind.PLUS:
            return self.processBinaryExp(AsmOpKind.Add, ast)
        elif ast.kind == ExpressionKind.MINUS:
            return self.processBinaryExp(AsmOpKind.Sub, ast)
        elif ast.kind == ExpressionKind.TIMES:
            return self.processBinaryExp(AsmOpKind.Mul, ast)
        elif ast.kind == ExpressionKind.DIVIDE:
            return self.processBinaryExp(AsmOpKind.Div, ast)
        # elif ast.kind == ExpressionKind.MOD:
        #     raise Exception('Not implemented')
        elif ast.kind == ExpressionKind.PLUSPLUS:
            self.add(AsmOp(AsmOpKind.Inc, type=ast.dataType, params=[ast.data[0], ast.data[1]]))
        elif ast.kind == ExpressionKind.MINUSMINUS:
            self.add(AsmOp(AsmOpKind.Dec, type=ast.dataType, params=[ast.data[0], ast.data[1]]))
        elif ast.kind == ExpressionKind.UNARY:
            return self.processUnaryExp(ast)
        elif ast.kind == ExpressionKind.POSTFIX_PLUSPLUS:
            self.add(AsmOp(AsmOpKind.Postpp, type=ast.dataType, params=[ast.data[0], ast.data[1]]))
        elif ast.kind == ExpressionKind.POSTFIX_MINUSMINUS:
            self.add(AsmOp(AsmOpKind.Postmm, type=ast.dataType, params=[ast.data[0], ast.data[1]]))
        # elif ast.kind == ExpressionKind.PERIOD:
        #     raise Exception('Not implemented')
        elif ast.kind == ExpressionKind.CALL:
            return self.processCall(ast)
        elif ast.kind == ExpressionKind.COMPOUND:
            return self.processCompound(ast)
        elif ast.kind == ExpressionKind.PERIOD:
            return self.processPeriod(ast)
        elif ast.kind == ExpressionKind.POSTFIX_BRACKET:
            if ast.data[0].getType().isTuple():
                r1 = self.processExp(ast.data[0])
                r2 = self.processExp(ast.data[1])
                rr = self.allocVar(ast.getType())
                self.add(AsmOp(AsmOpKind.GetTupleField, params=[r1, r2, rr]))
                return rr
            else:
                raise Exception('Not implemented')
        else:
            raise Exception('Not implemented. Kind = {0}'.format(ast.kind))

    def log(self, *args):
        print('AsmGenerator:', args)
