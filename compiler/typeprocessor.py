# Copyright (c) 2013-2014 Alexandre Grigorovitch (alexezh@gmail.com).

# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to permit
# persons to whom the Software is furnished to do so, subject to the
# following conditions:

# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN
# NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
# DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
# OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
# USE OR OTHER DEALINGS IN THE SOFTWARE.
import sys
import revlex
import revparse

from ply import *
from revast import *
from symboltable import *

class TypeProcessor:
    def computeFunctionTypes(self, func):
        for x in func.body:
            self.computeExpressionTypes(x, func.symbols)

    def computeExpressionTypes(self, ast, symbols):
        # print('process {0!s}'.format(ast))

        # if object is string, return
        if ast.data == None or isinstance(ast.data, str):
            return

        if isinstance(ast.data, AstNode):
            self.computeExpressionTypes(ast.data, symbols)

        if isinstance(ast.data, collections.Iterable):
            for c in ast.data:
                if isinstance(c, Expression):
                    self.computeExpressionTypes(c, symbols)

        self.log(ast.kind)

        if ast.kind == ExpressionKind.RETURN:
            ast.dataType = ast.data.getType()
        elif ast.kind == ExpressionKind.SEQ:
            raise Exception('Syntax error. Sequence can only be used for creating arrays or tuples')
        elif ast.kind == ExpressionKind.ASSIGN:
            self.computeAssign(ast, symbols)
        elif ast.kind == ExpressionKind.CONDITIONAL:
            ast.dataType = ast.data[2].getType()
        elif ast.kind == ExpressionKind.LOGICAL_OR:
            if not ast.data[1].getType().isBool():
                raise Exception('OR can be only used for boolean')
            ast.dataType = ast.data[1].getType()
        elif ast.kind == ExpressionKind.LOGICAL_AND:
            if not ast.data[1].getType().isBool():
                raise Exception('OR can be only used for boolean')
            ast.dataType = ast.data[1].getType()
        elif ast.kind == ExpressionKind.INCLUSIVE_OR:
            if not ast.data[1].getType().canUseLogicalOp():
                raise Exception('Logical operations cannot be used for {0}'.format(ast.data[1].getType()))
            ast.dataType = ast.data[1].getType()
        elif ast.kind == ExpressionKind.EXCLUSIVE_OR:
            if not ast.data[1].getType().canUseLogicalOp():
                raise Exception('Logical operations cannot be used for {0}'.format(ast.data[1].getType()))
            ast.dataType = ast.data[1].getType()
        elif ast.kind == ExpressionKind.AND:
            if not ast.data[1].getType().canUseLogicalOp():
                raise Exception('Logical operations cannot be used for {0}'.format(ast.data[1].getType()))
            ast.dataType = ast.data[1].getType()
        elif ast.kind == ExpressionKind.EQUALITY:
            ast.dataType = MakeBoolType()
        elif ast.kind == ExpressionKind.PRIMARY:
            print('what should be the type here?')
            pass
        elif ast.kind == ExpressionKind.CONSTANT:
            ast.dataType = ast.data[1]
        elif ast.kind == ExpressionKind.PLUS:
            self.validateArithmeticTypes(ast)
            ast.dataType = self.selectHigherPrecision(ast)
        elif ast.kind == ExpressionKind.MINUS:
            self.validateArithmeticTypes(ast)
            ast.dataType = self.selectHigherPrecision(ast)
        elif ast.kind == ExpressionKind.TIMES:
            self.validateArithmeticTypes(ast)
            ast.dataType = self.selectHigherPrecision(ast)
        elif ast.kind == ExpressionKind.DIVIDE:
            if not ast.data[1].getType().canUseArithmetic():
                raise Exception('Arithmetic cannot be used for {0}'.format(ast.data[1].getType()))
            ast.dataType = MakeDoubleType()
        elif ast.kind == ExpressionKind.MOD:
            raise Exception('Not implemented')
        elif ast.kind == ExpressionKind.PLUSPLUS:
            self.validateArithmeticTypes()
            ast.dataType = ast.data[1].getType()
        elif ast.kind == ExpressionKind.MINUSMINUS:
            self.validateArithmeticTypes()
            ast.dataType = ast.data[1].getType()
        elif ast.kind == ExpressionKind.UNARY:
            ast.dataType = ast.data[1].getType()
        elif ast.kind == ExpressionKind.POSTFIX_PLUSPLUS:
            self.validateArithmeticTypes()
            ast.dataType = ast.data[1].getType()
        elif ast.kind == ExpressionKind.POSTFIX_MINUSMINUS:
            self.validateArithmeticTypes(ast)
            ast.dataType = ast.data[1].getType()
        elif ast.kind == ExpressionKind.PERIOD:
            # For A.B, lookup A type, make sure that type is struct
            # and get type of B.  
            # do symbol lookup
            objType = ast.data[0].getType()
            if not objType.isStruct():
                raise Exception('Left side must be struct')

            fldType = objType.fields.get(ast.data[1])
            if fldType == None:
                raise Exception('Field {0} missing from type {1}'.format(ast.data[1], objType))
        elif ast.kind == ExpressionKind.MAKE_TUPLE:
            # make output of sequence a tuple
            ast.dataType = MakeTupleType(None, [])
            for c in ast.data:
                ast.dataType.fields.append(TupleField(c.getType(), None))
            symbols.addGlobalType(ast.dataType)
            self.log('Create tuple with {0} types'.format(len(ast.dataType.fields)))
        elif ast.kind == ExpressionKind.CALL:
            # function call
            if ast.getType() == None:
                raise Exception('Type for call must be set in BindCalls. Ast = {0!s}'.format(ast))

            # iterate through parameters
            for c in ast.data[1]:
                self.computeExpressionTypes(c, symbols)

        elif ast.kind == ExpressionKind.POSTFIX_BRACKET:
            if ast.data[0].getType().isTuple():
                # parameter should be constant
                if not ast.data[1].kind == ExpressionKind.CONSTANT:
                    raise Exception('Tuple index must be constant')

                # get tuple field by index and use this type 
                self.log('tuple fields', ast.data[0].getType().fields)
                ast.dataType = ast.data[0].getType().fields[int(ast.data[1].data[0])].type
            else:
                raise Exception('Not implemented')
        else:
            pass

        return ast.dataType
        # print('compute {0!s}'.format(self))

        # print('type {0} {1}'.format(ast.type, ast.getType()))

    # takes expression on the right and propagates the type to the left
    # if left already has type, validates it
    def computeAssign(self, ast:AssignExpression, symbols:SymbolTable):
        ast.dataType = ast.data[2].getType()
        ast.setLeftType(ast.dataType)

    def selectHigherPrecision(self, ast):
        if ast.data[0].getType().isDouble() or ast.data[1].getType().isDouble():
            return MakeDoubleType();
        if ast.data[0].getType().isInt64() or ast.data[1].getType().isInt64():
            return MakeInt64Type();
        if ast.data[0].getType().isInt32() or ast.data[1].getType().isInt32():
            return MakeInt32Type();
        raise Exception('Unknown type')

    def validateArithmeticTypes(self, ast):
        if not ast.data[0].getType().canUseArithmetic():
            raise Exception('Arithmetic cannot be used for {0}'.format(ast.data[0].getType()))

        if not ast.data[1].getType().canUseArithmetic():
            raise Exception('Arithmetic cannot be used for {0}'.format(ast.data[1].getType()))

    def log(self, *args):
        print('TypeProcessor:', args)
