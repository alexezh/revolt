# Copyright (c) 2013-2014 Alexandre Grigorovitch (alexezh@gmail.com).

# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to permit
# persons to whom the Software is furnished to do so, subject to the
# following conditions:

# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN
# NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
# DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
# OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
# USE OR OTHER DEALINGS IN THE SOFTWARE.

import sys

from asmop import *
from revast import Function
from symboltable import *

class AsmWriter:
	def __init__(self, f:Function):
		self.function = f
		self.output = []

	def processOp(self, op:AsmOp):
		ps = []

		if op.op == AsmOpKind.Label:
			return op.params[0]

		for p in op.params:
			if isinstance(p, StorageLocation):
				if op.isExternal():
					ps.append(':'.join([p.qualifiedLocation(), p.type.name]))
				else:
					ps.append(p.qualifiedLocation())
			elif isinstance(p, str):
				ps.append(p)
			elif isinstance(p, int):
				ps.append(str(p))
			else:
				raise Exception('Unsupported type {0}'.format(p))

		if op.op == AsmOpKind.AllocStorage:
			return None
		elif op.op == AsmOpKind.FreeStorage:
			if len(op.params) != 1 or not isinstance(op.params[0], StorageLocation):
				raise Exception('operation should have single parameter of StorageLocation type')

			if not op.params[0].type.isObject():
				return None

			return '{0} {1}'.format('releaseObject', ','.join(ps))
		elif op.op == AsmOpKind.Store:
			if op.type.isObject():
				if op.init:
					opCode = 'initObject'
				else:
					opCode = 'storeObject'
			else:
				opCode = 'store'
			
			return '{0} {1}'.format(opCode, ','.join(ps))
		else:
			if op.isTyped():
				return '{0} {1} {2}'.format(op.asString(), op.type.name, ','.join(ps))
			else:
				return '{0} {1}'.format(op.asString(), ','.join(ps))

	def write(self) -> str:
		for op in self.function.asm:
			r = self.processOp(op)
			if r != None:
				self.output.append(r)

		return self.output


