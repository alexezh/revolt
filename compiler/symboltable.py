# Copyright (c) 2013-2014 Alexandre Grigorovitch (alexezh@gmail.com).

# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to permit
# persons to whom the Software is furnished to do so, subject to the
# following conditions:

# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN
# NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
# DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
# OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
# USE OR OTHER DEALINGS IN THE SOFTWARE.
import sys
from enum import Enum
from revast import *
from asmop import AsmOpKind
import pdb
from collections import OrderedDict

escapeNameTable = str.maketrans(',', '_')

def escapedTypeName(name) -> str:
    return name.translate(escapeNameTable)

class SymbolKind(Enum):
    Function = 1
    NativeFunction = 2
    LocalVariable = 3
    ParamVariable = 4
    GlobalVariable = 5
    Event = 6
    Type = 7

class Symbol:
    def __init__(self, kind : SymbolKind, name : str, t : TypeId = None):
        self.kind = kind
        self.name = name
        self.type = t

class NativeFunctionSymbol(Symbol):
    def __init__(self,name : str,asmOp : AsmOpKind,t : TypeId = None):
        super().__init__(SymbolKind.NativeFunction, name, t);
        self.asmOp = asmOp

class FunctionSymbol(Symbol):
    def __init__(self,name,t):
        super().__init__(SymbolKind.Function, name, t);

class LocalVarSymbol(Symbol):
    def __init__(self,name,t):
        super().__init__(SymbolKind.LocalVariable, name, t);

class ParamVarSymbol(Symbol):
    def __init__(self,name,t):
        super().__init__(SymbolKind.ParamVariable, name, t);

class GlobalVarSymbol(Symbol):
    def __init__(self,name:str,t : TypeId):
        super().__init__(SymbolKind.GlobalVariable, name, t);

class EventSymbol(Symbol):
    def __init__(self,name:str,t:TypeId):
        if not isinstance(t, EventTypeId):
            raise Exception('EventSymbol requires EventTypeId parameter')

        super().__init__(SymbolKind.Event, name, t);
        self.fields = {}
        for f in t.fields:
            self.fields[f[1]] = f

class TypeSymbol(Symbol):
    def __init__(self,name:str,cat:TypeCategory,structFields=[]):
        super().__init__(SymbolKind.Type, name, t);
        self.structFields = structFields

class SymbolTable:
    def __init__(self, parent = None):
        self.data = OrderedDict()
        self.parent = parent
        if parent == None:
            self.nextTupleId = 1

    def items(self):
        return self.data.items()

    # add new symbol for a type or returns existing type
    def addGlobalType(self, t:TypeId) -> Symbol:
        if self.parent != None:
            return self.parent.addGlobalType(t)
        else:
            if t.category != TypeCategory.Tuple:
                raise Exception('A type should be tuple')
            t.name = self._createTupleName()
            s = TypeSymbol(t.name, t)
            self.add(s)
            return s

    def add(self, sym:Symbol):
        if sym.name == None:
            raise Exception('A type should have a name')
        self.log('add', sym.name, sym.type)
        self.data[sym.name] = sym

    def findType(self, name:str) -> Symbol:
        pass

    def find(self, name:str) -> Symbol:
        if name == 'keystore<int32,int32>':
            pdb.set_trace()
            
        v = self.data.get(name)
        if v != None:
            return v
        if self.parent != None:
            return self.parent.find(name)
        return None

    def _createTupleName(self) -> int:
        name = '__tuple' + str(self.nextTupleId)
        self.nextTupleId += 1
        return name

    def log(self, *args):
        print('SymbolTable:', args)
