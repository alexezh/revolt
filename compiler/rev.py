# Copyright (c) 2013-2014 Alexandre Grigorovitch (alexezh@gmail.com).

# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to permit
# persons to whom the Software is furnished to do so, subject to the
# following conditions:

# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN
# NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
# DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
# OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
# USE OR OTHER DEALINGS IN THE SOFTWARE.
import sys
import revlex
import revparse
import json
import pdb

from ply import *
from revast import *
from asmop import AsmOpKind
from asmgenerator import *
from asmwriter import *
from typeprocessor import *
from symboltable import *

def CheckNode(node, pred, res):
    if pred(node):
        res.append(node)
        return False
    return True

def SelectNodes(ast, pred):
    res = []
    ast.visit(lambda node : CheckNode(node, pred, res))
    return res

def PrintNode(node):
    print(node)
    return True

def BuildGlobalTypes(ast:Module, symbols:SymbolTable):
    assert isinstance(ast, Module)
    print("BuildGlobalSymbols")
    symbols.add(TypeSymbol('bool', MakeBoolType()))
    symbols.add(TypeSymbol('int32', MakeInt32Type()))
    symbols.add(TypeSymbol('int64', MakeInt64Type()))
    symbols.add(TypeSymbol('Nullable<int32>', MakeNullableType(MakeInt32Type(), MakeBoolType())))
    symbols.add(TypeSymbol('keystore', TypeId(TypeCategory.Container, 'keystore', [MakeAny('TKey'), MakeAny('TValue')])))
    # symbols.add(TypeSymbol('keystore<int32,int32>', TypeId(TypeCategory.Container, 'keystore', [MakeInt32Type(), MakeInt32Type()])))

    for e in ast.statements:
        if isinstance(e, EventNode):
            symbols.add(EventSymbol(e.name, e.type))
        elif isinstance(e, TupleNode):
            symbols.add(TypeSymbol(e.name, e.type))
        else:
            pass

def BuildGlobalSymbols(ast:Module, symbols:SymbolTable):
    assert isinstance(ast, Module)
    print("BuildGlobalSymbols")
    symbols.add(NativeFunctionSymbol('Send', AsmOpKind.Send, MakeVoidType()))
    symbols.add(NativeFunctionSymbol('CreateKeyStore', AsmOpKind.AddKey, MakeVoidType()))
    symbols.add(NativeFunctionSymbol('AddKey', AsmOpKind.AddKey, MakeVoidType()))
    symbols.add(NativeFunctionSymbol('RemoveKey', AsmOpKind.RemoveKey, MakeNullableType(MakeInt32Type(), MakeBoolType())))
    symbols.add(NativeFunctionSymbol('GetKey', AsmOpKind.GetKey, MakeNullableType(MakeInt32Type(), MakeBoolType())))

    for e in ast.statements:
        if isinstance(e, Function):
            if e.name != None:
                symbols.add(FunctionSymbol(e.name, e.type))
        elif isinstance(e, EventNode):
            pass
        elif isinstance(e, StructNode):
            symbols.add(TypeSymbol(e.name, e.fields))
        elif isinstance(e, Binding):
            pass
        elif isinstance(e, GlobalVar):
            symbols.add(GlobalVarSymbol(e.name, None))
        else:
            raise Exception('Unknown node type {0}'.format(e))

    # go through symbols and 

def BuildGlobalInitFunction(ast:Module, symbols:SymbolTable):
    globals = SelectNodes(ast, lambda node : isinstance(node, GlobalVar))
    initFuncBody = []
    for g in globals:
        initFuncBody.append(g.expr)

    initFunc = Function('__initGlobals',MakeVoidType(),[],initFuncBody)
    ast.functions.append(initFunc)
    ast.bindings.append(Binding('__initGlobals', [ BindingParameter('symbol', '__initGlobals'), BindingParameter('handler', initFunc)]))

def CacheBindings(ast:Module):
    ast.bindings = SelectNodes(ast, lambda node : (isinstance(node, Binding)))

def CacheFunctions(ast : Module):
    ast.functions = SelectNodes(ast, lambda node : isinstance(node, Function))
    for i,f in enumerate(ast.functions):
        if f.name == None:
            f.name = '_Func'+str(i)

# resolve idents; handle variable scope 
# todo. symbols are local inside scope
# todo. remove this code. variables are defined as they are initialized
def BindScopeNames(ast : Expression, symbols : SymbolTable):
    if ast.kind == ExpressionKind.LET:
        for ident in ast.data:
            if not ident.kind == ExpressionKind.IDENT:
                raise Exception('Left side should be a variable')
            symbols.add(LocalVarSymbol(ident.name, None))
    elif ast.kind == ExpressionKind.IDENT:
        sym = symbols.find(ast.name)
        if sym == None:
            raise Exception('Symbol {0} is not found'.format(ast.data))
        print('found symbol {0}'.format(ast.name))
        ast.symbol = sym
    else:
        # if object is string, return
        # otherwise iterate through ast.data
        if ast.hasChildren():
            for c in ast.childrenAsList():
                if not (c == None or isinstance(c, str)):
                    BindScopeNames(c, symbols)

#    print('process {0!s}'.format(ast))

def BindNames(ast:Module, symbols:SymbolTable):
    print('BindVars')

    for f in ast.functions:
        print('BindNames for {0}. params={1}'.format(f.name, len(f.params)))
        f.symbols = SymbolTable(symbols)
        # add parameters to symbols
        for p in f.params:
            f.symbols.add(ParamVarSymbol(p[1], p[0]))

        for e in f.body:
            BindScopeNames(e, f.symbols)

# compute types for everything
def ComputeTypes(ast:Module, symbols:SymbolTable):
    print('ComputeTypes: functions={0}'.format(len(ast.functions)))
    tp = TypeProcessor()

    for f in ast.functions:
        print('Compute ', f.name)
        tp.computeFunctionTypes(f)


def GenerateAssembler(ast):
    print('GenerateAssembler')

    for f in ast.functions:
        g = AsmGenerator(f)
        f.asm = g.generate()

def WriteAssembler(ast):
    print('WriteAssembler')
    for f in ast.functions:
        g = AsmWriter(f)
        f.asmFinal = g.write()

def PrintAsm(ast : AstNode):
    for f in ast.functions:
        print(':{0}'.format(f.name))
        print('\n'.join(f.asm))

def ProcessAst(ast : AstNode):
    # process ast tree in multiple steps
    print('\n\nStart processing {0}....'.format(inputFile))
    CacheFunctions(ast)
    CacheBindings(ast)
    BuildGlobalTypes(ast, symbols)
    BuildGlobalSymbols(ast, symbols)
    BuildGlobalInitFunction(ast, symbols)
    BindNames(ast, symbols)
    ComputeTypes(ast, symbols)
    GenerateAssembler(ast)
    WriteAssembler(ast)
    print('\nStats: functions {0}'.format(len(ast.functions)))

def OutputAsm(ast : AstNode, symbols : SymbolTable, fp):
    # pdb.set_trace()

    output = [
        {'bindings' : [
            b.toJson() for b in ast.bindings
        ]},
        {'types' : [
            { 'name' : escapedTypeName(s[1].name), 'type' : s[1].type.toJson() } for key,s in enumerate(symbols.items()) if isinstance(s[1], TypeSymbol) and not s[1].type.isPrimitive()
        ]},
        {'globals' : [
            { 'name' : s[1].name, 'type' : s[1].type.toJson() } for key,s in enumerate(symbols.items()) if isinstance(s[1], GlobalVarSymbol)
        ]},
        {'functions' : [
            { 'name' : f.name, 'code' : '\n'.join(f.asmFinal) } for f in ast.functions
        ]}
    ]
    json.dump(output, fp, indent=2, sort_keys=True)

if len(sys.argv) <= 1:
    print("usage : pgen.py [-nocode] inputfile outputfile")
    raise SystemExit

idx = 1;
if sys.argv[1] == '-nocode':
    pparse.emit_code = 0
    idx = 2

inputFile = sys.argv[idx]
outputFile = sys.argv[idx+1]

ast = yacc.parse(open(inputFile).read())

# ast.visit(PrintNode)
symbols = SymbolTable()

breakOnError = True

if breakOnError: 
    ProcessAst(ast)
else:
    try:
       ProcessAst(ast)
    except Exception as e:
       print('Compile filed\nError: {0}'.format(e))
       sys.exit(255)

print('Write output to {0}'.format(outputFile))
with open(outputFile, 'w') as fp:
    OutputAsm(ast, symbols, fp)
