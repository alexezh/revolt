# Copyright (c) 2013-2014 Alexandre Grigorovitch (alexezh@gmail.com).

# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to permit
# persons to whom the Software is furnished to do so, subject to the
# following conditions:

# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN
# NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
# DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
# OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
# USE OR OTHER DEALINGS IN THE SOFTWARE.
import sys
import re
import revlex
from collections import namedtuple
from revast import *
import ply.yacc as yacc

# Get the token map
tokens = revlex.tokens

#
# define module as list of global;function or binding
#
def p_module_1(t):
    'module : module_specification'
    t[0] = Module(t[1], t.lineno(1))

def p_module_specification_1(t):
    'module_specification : module_statement'
    t[0] = [t[1]]

def p_module_statement_list_2(t):
    'module_specification : module_specification module_statement'
    t[0] = t[1] + [t[2]]

def p_module_statement(t):
    '''
    module_statement : item_static
                      | item_fn
                      | binding_declaration
                      | item_struct
    '''
    t[0] = t[1]

#
# global variables definition
#       type name;
#       [attributes] type name;
def p_item_static_1(t):
    'item_static : STATIC ID assignment_operator assignment_expression SEMI'
    assignExpr = AssignExpression((t[2], t[3], t[4]))
    t[0] = GlobalVar(t[2], assignExpr)

# functions
def p_item_fn_1(t):
    'item_fn : FN ident_or_generic fn_declaration SEMI'
    t[0] = Function(t[2], t[3], t[4], None)

def p_item_function_2(t):
    'item_fn : FN ident_or_generic fn_declaration code_block'
    t[0] = Function(t[2], t[3], t[4], t[5])

def p_fn_declaration_1(t):
    'fn_declaration : LPAREN parameter_declaration_list_opt RPAREN fn_ret_ty_opt'
    t[0] = t[2]

def p_fn_ret_ty_opt_1(t):
    'fn_ret_ty_opt : empty'
    t[0] = None

def p_fn_ret_ty_opt_2(t):
    'fn_ret_ty_opt : ARROW ty'
    t[0] = t[2]

# attributes
def p_attribute_declaration_1(t):
    'attribute_declaration : LBRACKET attribute_list RBRACKET'
    t[0] = t[2]

# p_attribute_list is a map of attributes
def p_attribute_list_1(t):
    'attribute_list : attribute'
    id, val = t[1]
    t[0] = {}
    t[0][id] = val

def p_attribute_list_2(t):
    'attribute_list : attribute COMMA attribute_list'
    id, val = t[1]
    t[3][id] = val

def p_attribute_1(t):
    'attribute : ID'
    t[0] = (t[1], None)

def p_attribute_2(t):
    'attribute : ID EQUALS ICONST'
    t[0] = (t[1], t[3])

# binding
def p_binding_declaration_1(t):
    'binding_declaration : ID COLON binding_parameters'
    t[0] = Binding(t[1], t[3])

def p_binding_parameters_1(t):
    'binding_parameters : binding_parameter'
    t[0] = [t[1]]

def p_binding_parameters_2(t):
    'binding_parameters : binding_parameter COMMA binding_parameters'
    t[0] = [t[1]] + t[3]

def p_binding_parameter_1(t):
    'binding_parameter : ID EQUALS ICONST'
    t[0] = BindingParameter(t[1], t[3])

def p_binding_parameter_2(t):
    'binding_parameter : ID EQUALS SCONST'
    match = re.search('\"(.*)\"', t[3])
    t[0] = BindingParameter(t[1], match.group(1))

# handler=() { something }
def p_binding_parameter_3(t):
    'binding_parameter : ID EQUALS fn_declaration code_block'
    t[0] = BindingParameter(t[1], Function(None, MakeVoidType(), t[3], t[4]))

# struct
def p_item_struct_1(t):
    'item_struct : STRUCT ID LBRACE struct_field_list RBRACE'
    t[0] = StructNode(t[2], t[4])

def p_item_struct_2(t):
    'item_struct : STRUCT ID LPAREN ty_list RPAREN'
    t[0] = StructNode(t[2], t[4])

def p_struct_field_list_1(t):
    'struct_field_list : struct_field'
    t[0] = [t[1]]

def p_struct_field_list_2(t):
    'struct_field_list : struct_field struct_field_list'
    t[0] = [t[1]] + t[2]

def p_struct_field_1(t):
    'struct_field : ID COLON ty SEMI'
    t[0] = (t[1], t[2])

# parameter list
def p_parameter_declaration_list_opt_1(t):
    'parameter_declaration_list_opt : empty'
    t[0] = []

def p_parameter_declaration_list_opt_2(t):
    'parameter_declaration_list_opt : parameter_declaration_list'
    t[0] = t[1]

def p_parameter_declaration_list_1(t):
    'parameter_declaration_list : parameter_declaration'
    t[0] = [t[1]]

# parameter_declaration
def p_parameter_declaration_list_2(t):
    'parameter_declaration_list : parameter_declaration COMMA parameter_declaration_list'
    t[0] = [t[1]] + t[3] 

def p_parameter_declaration_1(t):
    'parameter_declaration : ID COLON ty'
    t[0] = (t[2], t[1])

# function_body
def p_code_block_1(t):
    'code_block : LBRACE statement_list RBRACE'
    t[0] = t[2]

# statement_list
def p_statement_list_1(t):
    'statement_list : statement'
    t[0] = [t[1]]

def p_statement_list_2(t):
    'statement_list : statement statement_list'
    t[0] = [t[1]] + t[2]

# statement
def p_statement(t):
    '''statement : expression_statement
                | compound_statement
                | iteration_statement
                | selection_statement
                | return_statement
    '''
    t[0] = t[1]

# expression-statement:
def p_expression_statement(t):
    'expression_statement : expression_opt SEMI'
    if not isinstance(t[1], AstNode):
        raise Exception('Must be node')
    t[1].lineno = t.lexer.lineno
    t[0] = t[1]

# compound-statement:
def p_compound_statement_1(t):
    'compound_statement : LBRACE statement_list RBRACE'
    t[0] = Expression(ExpressionKind.COMPOUND, t[2])

def p_compound_statement_2(t):
    'compound_statement : LBRACE RBRACE'
    t[0] = Expression(ExpressionKind.COMPOUND, [])

# selection_statement
def p_selection_statement_1(t):
    'selection_statement : IF LPAREN expression RPAREN statement'
    t[0] = Expression(ExpressionKind.IF, (t[3], t[5], None))

def p_selection_statement_2(t):
    'selection_statement : IF LPAREN expression RPAREN statement ELSE statement '
    t[0] = Expression(ExpressionKind.IF, (t[3], t[5], t[7]))

# iteration_statement:
def p_iteration_statement_1(t):
    'iteration_statement : WHILE LPAREN expression RPAREN statement'
    t[0] = Expression(ExpressionKind.WHILE, (t[3], t[5]))

def p_iteration_statement_2(t):
    'iteration_statement : FOR LPAREN expression_opt SEMI expression_opt SEMI expression_opt RPAREN statement '
    t[0] = Expression(ExpressionKind.FOR, (t[3], t[5], t[7], t[9]))

def p_iteration_statement_3(t):
    'iteration_statement : DO statement WHILE LPAREN expression RPAREN SEMI'
    t[0] = Expression(ExpressionKind.DO, (t[2], t[5]))

# return statement
def p_return_statement_1(t):
    'return_statement : RETURN expression_opt SEMI'
    t[0] = Expression(ExpressionKind.RETURN, (t[2]))

# expression_opt
def p_expression_opt_1(t):
    'expression_opt : empty'
    t[0] = None

def p_expression_opt_2(t):
    'expression_opt : expression'
    t[0] = t[1]

# expression:
def p_expression_1(t):
    'expression : assignment_expression'
    t[0] = t[1]

def p_expression_2(t):
    'expression : expression COMMA assignment_expression'
    if t[1].kind == ExpressionKind.SEQ:
        t[1].data.append(t[3])
        t[0] = t[1]
    else:
        t[0] = Expression(ExpressionKind.SEQ, [t[1], t[3]], lineno=t.lexer.lineno)

# assigment_expression:
def p_assignment_expression_1(t):
    'assignment_expression : conditional_expression'
    t[0] = t[1]

def p_assignment_expression_2(t):
    'assignment_expression : unary_expression assignment_operator assignment_expression'
    t[0] = AssignExpression((t[1], t[2], t[3]))

def p_assignment_expression_3(t):
    'assignment_expression : LET ident assignment_operator assignment_expression'
    t[0] = AssignExpression((Expression(ExpressionKind.LET, [t[2]]), t[3], t[4]))

def p_assignment_expression_4(t):
    'assignment_expression : LET LPAREN ident_list RPAREN assignment_operator assignment_expression'
    t[0] = AssignExpression((Expression(ExpressionKind.LET, t[2], lineno=t.lexer.lineno), t[3], t[4]), lineno=t.lexer.lineno)

# assignment_operator:
def p_assignment_operator(t):
    '''
    assignment_operator : EQUALS
                        '''
    t[0] = t[1]

# conditional-expression
def p_conditional_expression_1(t):
    'conditional_expression : logical_or_expression'
    t[0] = t[1]

def p_conditional_expression_2(t):
    'conditional_expression : logical_or_expression CONDOP expression COLON conditional_expression '
    t[0] = Expression(ExpressionKind.CONDITIONAL, (t[1], t[3], t[5]))

# logical-or-expression

def p_logical_or_expression_1(t):
    'logical_or_expression : logical_and_expression'
    t[0] = t[1]

def p_logical_or_expression_2(t):
    'logical_or_expression : logical_or_expression LOR logical_and_expression'
    t[0] = Expression(ExpressionKind.LOGICAL_OR, (t[1], t[3]))

# logical-and-expression

def p_logical_and_expression_1(t):
    'logical_and_expression : inclusive_or_expression'
    t[0] = t[1]

def p_logical_and_expression_2(t):
    'logical_and_expression : logical_and_expression LAND inclusive_or_expression'
    t[0] = Expression(ExpressionKind.AND, (t[1], t[3]))

# inclusive-or-expression:

def p_inclusive_or_expression_1(t):
    'inclusive_or_expression : exclusive_or_expression'
    t[0] = t[1]

def p_inclusive_or_expression_2(t):
    'inclusive_or_expression : inclusive_or_expression OR exclusive_or_expression'
    t[0] = Expression(ExpressionKind.INCLUSIVE_OR, (t[1], t[3]))

# exclusive-or-expression:

def p_exclusive_or_expression_1(t):
    'exclusive_or_expression :  and_expression'
    t[0] = t[1]

def p_exclusive_or_expression_2(t):
    'exclusive_or_expression :  exclusive_or_expression XOR and_expression'
    t[0] = Expression(ExpressionKind.EXCLUSIVE_OR, (t[1], t[3]))

# AND-expression

def p_and_expression_1(t):
    'and_expression : equality_expression'
    t[0] = t[1]

def p_and_expression_2(t):
    'and_expression : and_expression AND equality_expression'
    t[0] = Expression(ExpressionKind.AND, (t[1], t[3]))

# equality-expression:
def p_equality_expression_1(t):
    'equality_expression : relational_expression'
    t[0] = t[1]

def p_equality_expression_2(t):
    'equality_expression : equality_expression EQ relational_expression'
    t[0] = Expression(ExpressionKind.EQUALITY, (t[1], t[2], t[3]))

def p_equality_expression_3(t):
    'equality_expression : equality_expression NE relational_expression'
    t[0] = Expression(ExpressionKind.EQUALITY, (t[1], t[2], t[3]))

# relational-expression:
def p_relational_expression_1(t):
    'relational_expression : shift_expression'
    t[0] = t[1]

def p_relational_expression_2(t):
    'relational_expression : relational_expression LT shift_expression'
    t[0] = Expression(ExpressionKind.EQUALITY, (t[1], t[2], t[3]))

def p_relational_expression_3(t):
    'relational_expression : relational_expression GT shift_expression'
    t[0] = Expression(ExpressionKind.EQUALITY, (t[1], t[2], t[3]))

def p_relational_expression_4(t):
    'relational_expression : relational_expression LE shift_expression'
    t[0] = Expression(ExpressionKind.EQUALITY, (t[1], t[2], t[3]))

def p_relational_expression_5(t):
    'relational_expression : relational_expression GE shift_expression'
    t[0] = Expression(ExpressionKind.EQUALITY, (t[1], t[2], t[3]))


# shift-expression
# we do not support shift, so it is just additive
def p_shift_expression_1(t):
    'shift_expression : additive_expression'
    t[0] = t[1]

# def p_shift_expression_2(t):
#    'shift_expression : shift_expression LSHIFT additive_expression'
#    pass

#def p_shift_expression_3(t):
#    'shift_expression : shift_expression RSHIFT additive_expression'
#    pass

# additive-expression

def p_additive_expression_1(t):
    'additive_expression : multiplicative_expression'
    t[0] = t[1]

def p_additive_expression_2(t):
    'additive_expression : additive_expression PLUS multiplicative_expression'
    t[0] = Expression(ExpressionKind.PLUS, (t[1], t[3]))

def p_additive_expression_3(t):
    'additive_expression : additive_expression MINUS multiplicative_expression'
    t[0] = Expression(ExpressionKind.MINUS, (t[1], t[3]))

# multiplicative-expression

def p_multiplicative_expression_1(t):
    'multiplicative_expression : cast_expression'
    t[0] = t[1]

def p_multiplicative_expression_2(t):
    'multiplicative_expression : multiplicative_expression TIMES cast_expression'
    t[0] = Expression(ExpressionKind.TIMES, (t[1], t[3]))

def p_multiplicative_expression_3(t):
    'multiplicative_expression : multiplicative_expression DIVIDE cast_expression'
    t[0] = Expression(ExpressionKind.DIVIDE, (t[1], t[3]))

def p_multiplicative_expression_4(t):
    'multiplicative_expression : multiplicative_expression MOD cast_expression'
    t[0] = Expression(ExpressionKind.MOD, (t[1], t[3]))


# cast-expression:

def p_cast_expression_1(t):
    'cast_expression : unary_expression'
    t[0] = t[1]

# def p_cast_expression_2(t):
#    'cast_expression : LPAREN type_name RPAREN cast_expression'
#    pass

# unary-expression:
def p_unary_expression_1(t):
    'unary_expression : postfix_expression'
    t[0] = t[1]

def p_unary_expression_2(t):
    'unary_expression : PLUSPLUS unary_expression'
    t[0] = Expression(ExpressionKind.PLUSPLUS, (t[1]))

def p_unary_expression_3(t):
    'unary_expression : MINUSMINUS unary_expression'
    t[0] = Expression(ExpressionKind.MINUSMINUS, (t[1]))

def p_unary_expression_4(t):
    'unary_expression : unary_operator cast_expression'
    t[0] = Expression(ExpressionKind.UNARY, (t[1], t[2]))
    
#unary-operator
def p_unary_operator(t):
    '''unary_operator : TIMES
                    | PLUS 
                    | MINUS
                    | NOT
                    | LNOT '''
    t[0] = t[1]


# postfix-expression:
def p_postfix_expression_1(t):
    'postfix_expression : primary_expression'
    t[0] = t[1]

def p_postfix_expression_2(t):
    'postfix_expression : postfix_expression LBRACKET expression RBRACKET'
    t[0] = Expression(ExpressionKind.POSTFIX_BRACKET, (t[1], t[3]))

def p_postfix_expression_3(t):
    'postfix_expression : postfix_expression LPAREN argument_expression_list RPAREN'
    t[0] = CallExpression((t[1], t[3]))

def p_postfix_expression_4(t):
    'postfix_expression : postfix_expression LPAREN RPAREN'
    t[0] = CallExpression((t[1], []))

def p_postfix_expression_5(t):
    'postfix_expression : postfix_expression PERIOD ID'
    t[0] = Expression(ExpressionKind.PERIOD, (t[1], t[3]))

def p_postfix_expression_7(t):
    'postfix_expression : postfix_expression PLUSPLUS'
    t[0] = Expression(ExpressionKind.POSTFIX_PLUSPLUS, (t[1]))

def p_postfix_expression_8(t):
    'postfix_expression : postfix_expression MINUSMINUS'
    t[0] = Expression(ExpressionKind.POSTFIX_MINUSMINUS, (t[1]))

# primary-expression:
def p_primary_expression_1(t):
    '''primary_expression :  ident_or_generic
    '''
    t[0] = t[1]

def p_primary_expression_2(t):
    'primary_expression : constant'
    t[0] = t[1]

# sequence with parentesis is tuple otherwise it is just parentesis
# TODO: fix case of tuple with single parameter and comma
def p_primary_expression_3(t):
    'primary_expression : LPAREN expression RPAREN'
    if t[2].kind == ExpressionKind.SEQ:
        if not isinstance(t[2].data, list):
            raise Exception('Internal error: sequence should always contain list')
        t[0] = Expression(ExpressionKind.MAKE_TUPLE, t[2].data, lineno=t.lexer.lineno)
    else:
        t[0] = t[2]

# constant:
def p_constant_1(t): 
    'constant : ICONST'
    t[0] = Expression(ExpressionKind.CONSTANT, (t[1], MakeInt32Type()), lineno=t.lexer.lineno)

def p_constant_2(t): 
    'constant : FCONST'
    t[0] = Expression(ExpressionKind.CONSTANT, (t[1], MakeDoubleType()), lineno=t.lexer.lineno)

def p_constant_3(t): 
    'constant : SCONST'
    t[0] = Expression(ExpressionKind.CONSTANT, (t[1], MakeStringType()), lineno=t.lexer.lineno)

def p_constant_4(t): 
    '''constant : TRUE 
        | FALSE
    '''
    t[0] = Expression(ExpressionKind.CONSTANT, (t[1], MakeBoolType()), lineno=t.lexer.lineno)

# primary_expression_list
def p_primary_expression_list_1(t):
    'primary_expression_list :  primary_expression'
    t[0] = [t[1]]

def p_primary_expression_list_2(t):
    'primary_expression_list :  primary_expression COMMA primary_expression_list'
    t[0] = [t[1]] + t[3]

# argument-expression-list:
def p_argument_expression_list_1(t):
    'argument_expression_list :  assignment_expression'
    t[0] = [t[1]]

def p_argument_expression_list_2(t):
    'argument_expression_list :  assignment_expression COMMA argument_expression_list'
    t[0] = [t[1]] + t[3]

# just ID
def p_ident(t):
    'ident : ID'
    t[0] = Ident(t[1],[],lineno=t.lexer.lineno)

# ident_or_generic
def p_ident_or_generic_1(t):
    'ident_or_generic : ID'
    t[0] = Ident(t[1],[],lineno=t.lexer.lineno)

def p_ident_or_generic_2(t):
    'ident_or_generic : ID LT ty_list GT'
    t[0] = Ident(t[1],t[3],lineno=t.lexer.lineno)

# ident_list
def p_ident_list_1(t):
    'ident_list : ident'
    t[0] = [t[1]]

def p_ident_list_2(t):
    'ident_list : ident COMMA ident_list'
    t[0] = [t[1]] + t[3]

# ty_list
def p_ty_list_1(t):
    'ty_list : ty'
    t[0] = [t[1]]

def p_ty_list_2(t):
    'ty_list : ty COMMA ty_list'
    t[0] = [t[1]] + t[3]

# type-specifier
def p_ty_1(t):
    'ty : LPAREN ty_list RPAREN'
    t[0] = TypeId(TypeCategory.Tuple, t[2], None)

def p_ty_2(t):
    'ty : ID'
    t[0] = TypeId(TypeCategory.Primitive, t[1], None)

def p_empty(t):
    'empty : '
    pass

def p_error(t):
    raise Exception('Syntax error. {0}'.format(t))


import profile
# Build the grammar

yacc.yacc(method='LALR')

#profile.run("yacc.yacc(method='LALR')")




