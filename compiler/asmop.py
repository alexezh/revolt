# generates assembler 
# Copyright (c) 2013-2014 Alexandre Grigorovitch (alexezh@gmail.com).

# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to permit
# persons to whom the Software is furnished to do so, subject to the
# following conditions:

# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN
# NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
# DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
# OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
# USE OR OTHER DEALINGS IN THE SOFTWARE.

import sys
import copy
from enum import Enum
from revast import TypeId

class LocationType(Enum):
    Constant = 1
    Temp = 2
    Stack = 3
    Global = 4
    Heap = 5
    Accumulator = 6

class StorageLocation:
    def __init__(self,locType:LocationType,location:str = None,type:TypeId = None):
        self.locType = locType
        self.location = location
        self.type = type
        self.temp = False
        self.varName = None

    def qualifiedLocation(self) -> str:
        if self.locType == LocationType.Stack or self.locType == LocationType.Temp:
            return 'bp+{0!s}'.format(self.location)
        elif self.locType == LocationType.Global or self.locType == LocationType.Heap or self.locType == LocationType.Constant:
            return self.location
        elif self.locType == LocationType.Accumulator:
            return "ra"
        else:
            raise Exception('Unknown location')

    def __str__(self):
        return 'type:{0} loc:{1}'.format(self.type, self.location)

class AutoNumber(Enum):
    def __new__(cls):
        value = len(cls.__members__) + 1
        obj = object.__new__(cls)
        obj._value_ = value
        return obj

class AsmOpKind(AutoNumber):
    Label = ()
    Stop = ()
    Jump = ()
    FunctionProlog = ()
    Call = ()
    Return = ()
    AllocTuple = ()
    AllocObject = ()
    GetTupleField = ()
    GetTupleFields = ()
    Store = ()
    StoreObject = ()
    Br = ()
    Cmp = ()
    Not = ()
    Add = ()
    Sub = ()
    Mul = ()
    Div = ()
    Inc = ()
    Dec = ()
    Lor = ()
    Land = ()
    ReadEvent = ()
    Send = ()
    AddKey = ()
    RemoveKey = ()
    GetKey = ()
    # internal
    AllocStorage = ()
    FreeStorage = ()

def CombineType(op:str, type:TypeId):
    if type.isInt32():
        return op + 'Int32'
    elif type.isInt64():
        return op + 'Int64'
    elif type.isDouble():
        return op + 'Double'
    else:
        raise Exception('Unsupported type')

AsmOpAttr = {
    AsmOpKind.Label:(None, False),
    AsmOpKind.Stop:('stop', False),
    AsmOpKind.Jump:('jump', False),
    AsmOpKind.FunctionProlog:('prolog', False),
    AsmOpKind.Call:('call', False),
    AsmOpKind.Return:('return', False),
    AsmOpKind.AllocTuple:('allocTuple', False),
    AsmOpKind.AllocObject:('allocObject', False),
    AsmOpKind.GetTupleField:('getTupleField', False),
    AsmOpKind.GetTupleFields:('getTupleFields', False),
    AsmOpKind.Store:('store', False),
    AsmOpKind.StoreObject:('storeObject', False),
    AsmOpKind.Br:('br', False),
    AsmOpKind.Cmp:('cmp', True),
    AsmOpKind.Not:('not', False),
    AsmOpKind.Add:('add', False, CombineType),
    AsmOpKind.Sub:('sub', False, CombineType),
    AsmOpKind.Mul:('mul', False, CombineType),
    AsmOpKind.Div:('div', False, CombineType),
    AsmOpKind.Inc:('inc', False, CombineType),
    AsmOpKind.Dec:('dec', False, CombineType),
    AsmOpKind.Lor:('lor', False),
    AsmOpKind.Land:('land', False),
    AsmOpKind.ReadEvent:('readEvent', False, CombineType),
    AsmOpKind.Send:('send', False),
    AsmOpKind.AddKey:('addKey', False),
    AsmOpKind.RemoveKey:('removeKey', False),
    AsmOpKind.GetKey:('getKey', False),
}

class AsmOp:
    def __init__(self, op:AsmOpKind, type:AsmOpKind=None, params:[]=[],init:bool = False):
        self.op = op;
        self.type = type
        self.params = [copy.copy(p) for p in params]
        self.init = init

    def isTyped(self) -> bool:
        s = AsmOpAttr.get(self.op)
        if s == None:
            raise Exception('Unsupported op {0}'.format(self.op))

        return s[1]

    def isExternal(self):
        if self.op == AsmOpKind.Send:
            return True
        else:
            return False

    def asString(self) -> str:
        if self.op == AsmOpKind.Label:
            raise Exception('Internal error')

        s = AsmOpAttr.get(self.op)
        if s == None:
            raise Exception('Unsupported op {0}'.format(self.op))

        if len(s) == 3:
            return s[2](s[0], self.type)
        else:
            return s[0]

    def __str__(self):
        return ' '.join([str(self.op), str(self.type), str(self.params)])
