# Copyright (c) 2013-2014 Alexandre Grigorovitch (alexezh@gmail.com).

# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to permit
# persons to whom the Software is furnished to do so, subject to the
# following conditions:

# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN
# NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
# DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
# OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
# USE OR OTHER DEALINGS IN THE SOFTWARE.
import sys
import collections
import string
import types
from collections import namedtuple
import pdb
from enum import Enum

def logParse(*args):
    print('Parser:', args)

StructField = namedtuple('name', 'type')

class AstNode:
    def __init__(self,lineno=0):
        self.lineno = lineno

    # deep traversal of tree
    # stops if visitor returns false
    def visit(self, visitor):
        if visitor(self) == False:
            return False

        self.visitCore(visitor)
        self.lineno = 0

    def visitCore(self, visitor):
        pass

    def visitObject(self, obj, visitor):
        if obj == None:
            return

        # if object is string, return
        if isinstance(obj, str):
            return

        if isinstance(obj, AstNode):
            return obj.visit(visitor)

        if isinstance(obj, collections.Iterable):
            for c in obj:
                self.visitObject(c, visitor)
        else:                  
            print("Should be AstNode", str(obj))
            raise SystemExit

class Module(AstNode):
    def __init__(self, statements, lineno):
        super().__init__()
        self.statements = statements
        self.functions = []
        self.lineno = lineno

    def visitCore(self, visitor):
        self.visitObject(self.statements, visitor)

class Function(AstNode):
    pass

class Binding(AstNode):
    def __init__(self,id,params):
        self.id = id
        self.params = params

    def visitCore(self, visitor):
        self.visitObject(self.params, visitor)

    def getSymbol(self):
        return next(x for x in self.params if x.name == 'etw' or x.name == 'symbol')

    def getHandler(self) -> Function:
        return next(x.value for x in self.params if x.name == 'handler')

    def toJson(self):
        sym = self.getSymbol()
        return { 'symbol' : sym.value, 'label' : self.getHandler().name }

    def __str__(self):
        return 'Binding ' + self.id

class BindingParameter(AstNode):
    def __init__(self,name,value):
        self.name = name
        self.value = value
        print('Binding {0}'.format(value))

    def visitCore(self, visitor):
        self.visitObject(self.value, visitor)

    def __str__(self):
        return 'BindingParam ' + self.name

class Function(AstNode):
    def __init__(self,name,t,params,body):
        self.name = name
        self.type = t
        self.params = params
        self.body = body
        self.vars = []
        self.varsLookup = {}
        self.symbol = None
        self.symbols = None
        # offset to the first temp variable
        self.tempOffset = 0

    def visitCore(self, visitor):
        self.visitObject(self.params, visitor)
        self.visitObject(self.body, visitor)

    def __str__(self):
        return 'Function {0} params {1!s}'.format(self.name, len(self.params))

class GlobalVar(AstNode):
    def __init__(self,name,expr):
        self.type = None
        self.name = name
        self.expr = expr
        self.symbol = None

    def visitCore(self, visitor):
        self.visitObject(self.expr, visitor)

class TypeId:
    pass

class EventNode(AstNode):
    def __init__(self,name:str,fields:[(TypeId, str)]):
        self.name = name
        self.type = MakeEventType(fields)
        self.symbol = None

class StructNode(AstNode):
    def __init__(self,name:str,fields:[StructField]):
        self.name = name
        # just store fields as is
        # we will bind them later once we have access to symbols
        self.fields = fields
        self.symbol = None

class ExpressionKind(Enum):
    IF = 1
    WHILE = 2
    FOR = 3
    DO = 4
    RETURN = 5
    SEQ = 6
    ASSIGN = 7
    CONDITIONAL = 8
    LOGICAL_OR = 9
    LOGICAL_AND = 10
    INCLUSIVE_OR = 11
    EXCLUSIVE_OR = 12
    AND = 13
    EQUALITY = 14
    PRIMARY = 15
    CONSTANT = 16
    PLUS = 17
    MINUS = 18
    TIMES = 19
    DIVIDE = 20
    MOD = 21
    PLUSPLUS = 22
    MINUSMINUS = 23
    UNARY = 24
    POSTFIX_PLUSPLUS = 25
    POSTFIX_MINUSMINUS = 26
    PERIOD = 27
    CALL = 28
    POSTFIX_BRACKET = 29
    COMPOUND = 30
    LET = 31   # declare variable
    MAKE_TUPLE = 32
    TYPEID = 33
    IDENT = 34

class Expression(AstNode):
    def __init__(self,t,data,lineno=0):
        super().__init__(lineno)
        self.kind = t
        self.data = data
        self.dataType = None

    def visitCore(self, visitor):
        self.visitObject(self.data, visitor)

    def getType(self):
        return self.dataType

    def hasChildren(self):
        if self.data == None or isinstance(self.data, str):
            return False
        else:
            return True

    def childrenAsList(self):
        if isinstance(self.data, list):
            return self.data
        elif isinstance(self.data, tuple):
            return self.data
        else:
            return [self.data]

    # debug print object
    def dtList(self, lst, depth:int):
        print('children {0}: '.format(len(self.data)))
        for c in lst:
            if isinstance(c, list):
                self.dtList(c, depth)
            else:
                c.dt(depth)

    def dt(self, depth:int = 1):
        if depth < 0:
            return
        print('kind: {0!s} type: {1!s}'.format(self.kind, self.getType()))
        if self.data:
            if isinstance(self.data, Expression):
                if depth > 0:
                    print('child: ')
                    self.data.dt(depth-1)
            elif isinstance(self.data, list) or isinstance(self.data, tuple):
                if depth > 0:
                    self.dtList(self.data, depth-1)
            elif isinstance(self.data, str):
                print('data: {0}'.format(self.data))
            else:
                print('unknown data type {0}'.format(self.data))

    def log(self, *args):
        print('Expression:', args)

    def __str__(self):
        s = 'kind:{0!s} type:{1!s}'.format(self.kind, self.dataType)
        if isinstance(self.data, str):
            s = s + ' data:' + self.data
        return s

class CallExpression(Expression):
    def __init__(self,data,lineno=0):
        super().__init__(ExpressionKind.CALL, data,lineno)

        if not (self.data[0].kind == ExpressionKind.IDENT):
            raise Exception('Incorrent syntax. Must be a function name')

    def childrenAsList(self):
        return [self.data[0]] + self.data[1]

    def getType(self):
        return self.data[0].getType()

    def callSymbol(self):
        return self.data[0].getSymbol();

class AssignExpression(Expression):
    def __init__(self,data,lineno=0):
        super().__init__(ExpressionKind.ASSIGN, data,lineno)

    def rightAsList(self):
        if isinstance(self.data[2], list):
            return self.data[2]
        else:
            return [self.data[2]]

    # true if left side of assign expression is a list
    # either direct or var with the list
    def isLeftList(self):
        if self.data[0].kind == ExpressionKind.LET:
            return isinstance(self.data[0].data, list) and len(self.data[0].data) > 1
        else:
            return isinstance(self.data[0], list) and len(self.data[0]) > 1
    
    def leftAsList(self):
        if self.data[0].kind == ExpressionKind.LET:
            node = self.data[0].data
        else:
            node = self.data[0]

        if isinstance(node, list):
            return node
        else:
            return [node]

    # set type to left (either var or)
    def setLeftType(self, t:TypeId):
        if t.category == TypeCategory.Tuple:
            if self.isLeftList():
                self.setExpandedTuple(t.fields)
                return
            else:
                self.setSingleType(t)
        else:
            self.setSingleType(t)

    # returns true if node is a variable list
    def setSingleType(self, t:TypeId):
        if self.data[0].kind == ExpressionKind.LET:
            node = self.data[0].data[0]
        else:
            node = self.data[0]
        
        node.symbol.type = t
        self.log('setSingleType {0} = {1}'.format(node.symbol.name, str(t)))

    # sets tuple parts
    def setExpandedTuple(self, types:{}):
        self.log('setExpandedTuple {0}'.format(types))

        if self.data[0].kind == ExpressionKind.LET:
            nodes = self.data[0].data
        else:
            nodes = self.data

        for idx, n in enumerate(nodes):
            if types[idx].type == None:
                raise Exception('Internal error. Type should not be None')

            n.symbol.type = types[idx].type
            self.log('set type for {0} = {1}'.format(n.symbol.name, types[idx].type))


# primary expression is ident or constant
# class PrimaryExpression(Expression):

class Ident(Expression):
    def __init__(self,name,generic=None,lineno=0):
        super().__init__(ExpressionKind.IDENT, None, lineno)
        self.name = name
        self.generic = generic
        self.symbol = None

    def getType(self):
        return self.symbol.type

    def callSymbol(self):
        return self.symbol;

class TypeCategory(Enum):
    Primitive = 0
    Nullable = 1
    Tuple = 2
    Struct = 3
    Container = 4
    Event = 5
    Any = 6 # template parameter
    User = 7 # any user defined symbol

def MakeAny(name:str):
    return TypeId(TypeCategory.Any, name, None)

def MakeBoolType():
    return TypeId(TypeCategory.Primitive, 'bool', None)

def MakeInt32Type():
    return TypeId(TypeCategory.Primitive, 'int32', None)

def MakeInt64Type():
    return TypeId(TypeCategory.Primitive, 'int64', None)

def MakeDoubleType():
    return TypeId(TypeCategory.Primitive, 'double', None)

def MakeVoidType():
    return TypeId(TypeCategory.Primitive, 'void', None)

def MakeStringType():
    return TypeId(TypeCategory.Primitive, 'string', None)

def MakeTupleType(name:str, fields:[TypeId]):
    return TupleTypeId(name, fields)

def MakeEventType(fields:[TypeId]):
    return EventTypeId(fields)

def MakeNullableType(value:TypeId, bt:TypeId):
    return NullableTypeId(value,bt)

class TypeId(Expression):
    def __init__(self,category:TypeCategory,name:string,tid:[TypeId],lineno=0):
        super().__init__(ExpressionKind.TYPEID, None, lineno)

        self.category = category
        self.name = name
        self.templateid = tid

    def visitCore(self, visitor):
        if self.templateid != None:
            self.visitObject(self.templateid, visitor)

    def isPrimitive(self):
        return self.category == TypeCategory.Primitive

    def getDefault(self):
        if self.isPrimitive():
            if self.name == 'bool':
                return False
            elif self.name == 'int32':
                return 0
            elif self.name == 'int64':
                return 0
            elif self.name == 'double':
                return 0.0
            else:
                raise Exception('Not supported type {0!s}', self)
        else:
            raise Exception('Not implemented')

    def isVoid(self):
        return self.isPrimitive() and self.name == 'void'

    def isBool(self):
        return self.isPrimitive() and self.name == 'bool'

    def isInt32(self):
        return self.isPrimitive() and self.name == 'int32'

    def isInt64(self):
        return self.isPrimitive() and self.name == 'int64'

    def isDouble(self):
        return self.isPrimitive() and self.name == 'double'

    def isObject(self) -> bool:
        return self.category == TypeCategory.Nullable or self.category == TypeCategory.Tuple or self.category == TypeCategory.Container

    def isTuple(self) -> bool:
        return self.category == TypeCategory.Tuple

    def isStruct(self) -> bool:
        return self.category == TypeCategory.Struct

    def isKeyStore(self) -> bool:
        return self.category == TypeCategory.Container and self.name == 'keystore'

    def canUseLogicalOp(self) -> bool:
        return self.isBool() or self.isInt32() or self.isInt64()

    def canUseArithmetic(self) -> bool:
        return self.isInt32() or self.isInt64() or self.isDouble()

    def toJson(self):
        if self.isPrimitive():
            return self.name
        elif self.category == TypeCategory.Container:
            return { 'base' : self.name, 'fields' : [ t.name for t in self.templateid ] }
        elif self.category == TypeCategory.Tuple:
            return { 'base' : 'tuple', 'fields' : [ t.type.name for t in self.fields ] }
        elif self.category == TypeCategory.Struct:
            return { 'base' : 'struct', 'fields' : [ t.type.name for t in self.fields ] }
        else:
            raise Exception('Cannot serialize type {0} to json'.format(self))

    def __str__(self):
        if self.isPrimitive() or self.category == TypeCategory.User or self.category == TypeCategory.Nullable:
            return self.name
        elif self.category == TypeCategory.Container:
            return '{0}<{1}>'.format(self.name, ','.join([ str(t) for t in self.templateid ]))
        elif self.category == TypeCategory.Tuple:
            return '({0})'.format(','.join([ str(t.type) for t in self.fields]))
        else:
            raise Exception('Cannot serialize type {0}'.format(self))

# tuple support
class TupleTypeId(TypeId):
    def __init__(self,name:str,fields:{}):
        super().__init__(TypeCategory.Tuple, name, None)
        self.fields = fields

class StructTypeId(TypeId):
    def __init__(self,name:str,fields:{}):
        super().__init__(TypeCategory.Struct, name, None)
        self.fields = fields

# event support
class EventTypeId(TypeId):
    def __init__(self,fields:[(TypeId, str)]):
        super().__init__(TypeCategory.Event, None, None)
        self.fields = fields

class NullableTypeId(TypeId):
    def __init__(self,value:TypeId,bt:TypeId):
        super().__init__(TypeCategory.Nullable, ''.join(['Nullable<', value.name, '>']), [bt, value])
        self.value = value

    def toJson(self):
        return { 'base' : 'nullable', 'fields' : [ t.toJson() for t in self.templateid ] }
