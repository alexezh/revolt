﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace oc
{
    public class ArrayHelper
    {
        public static bool Equals<T>(T[] v1, T[] v2)
        {
            if (v1.Length != v2.Length)
                return false;

            // we can use Zip/All but this would be lots of allocs
            for (int i = 0; i < v1.Length; i++)
            {
                if (!v1[i].Equals(v2[i]))
                    return false;
            }

            return true;
        }

        public static int Find<T>(T v, T[] arr) where T : TypeDefinition
        {
            for (int i = 0; i < arr.Length; i++)
            {
                if (v == arr[i])
                    return i;
            }

            throw new InvalidOperationException("cannot find parameter");
        }
    }

    // templates should be handled at instantiation
    // until then we keep tree unresolved since we cannot really tell the types
    // or if types are appropriate given parameters
    public class TemplateParameters
    {
 
    }

    public abstract class TypeDefinition
    {
        public abstract string Name
        {
            get;
        }
        public abstract string Signature
        {
            get;
        }
        public abstract bool IsObject { get; }

        // for template type, contains a list of template parameters
        protected TypeDefinition[] _TemplateParams;

        /// <summary>
        /// true if type is generic
        /// </summary>
        public virtual bool IsTemplate { get { return false; } }

        /// <summary>
        /// either JObject or string
        /// </summary>
        /// <returns></returns>
        public abstract JToken AsJson();

        public static bool Match(TypeDefinition t1, TypeDefinition t2)
        {
            if (t1.GetType().Equals(t2.GetType()))
            {
                return t1.Equals(t2);
            }
            else if (t1 is GenericType)
            {
                return ((GenericType)t1).Match(t2);
            }
            else if (t2 is GenericType)
            {
                return ((GenericType)t2).Match(t1);
            }
            else
            {
                return false;
            }
        }

        public override string ToString()
        {
            return Signature;
        }

        public bool MatchTemplateArgs(TypeDefinition[] templateArgs)
        {
            TypeDefinition[] result;
            return GenericTypeHelper.ResolveTypes(_TemplateParams, templateArgs, out result);
        }

        public virtual TypeDefinition InstantiateTemplate(TypeDefinition[] templateArgs)
        {
            throw new InvalidOperationException("Not supported for base type");
        }
    }


    /// <summary>
    /// list of type TValue
    /// </summary>
    abstract class ListTypeDefinition : TypeDefinition
    { 
    }

    /// <summary>
    /// map of type TKey, TValue
    /// </summary>
    abstract class MapTypeDefinition : TypeDefinition
    { 
    }


    public class PrimitiveTypeDefinition : TypeDefinition
    {
        public enum Kind
        {
            Void,
            Boolean,
            Int32,
            Int64,
            Double,
            String,
        }

        public PrimitiveTypeDefinition(Kind kind)
        {
            _Kind = kind;
        }

        public override int GetHashCode()
        {
            return _Kind.GetHashCode();
        }

        public override bool Equals(System.Object obj)
        {
            PrimitiveTypeDefinition p = obj as PrimitiveTypeDefinition;
            if ((object)p == null)
            {
                return false;
            }

            return Equals((PrimitiveTypeDefinition)obj);
        }

        public bool Equals(PrimitiveTypeDefinition p)
        {
            return _Kind == p._Kind;
        }

        public override string Name
        {
            get {
                switch (_Kind)
                {
                    case Kind.Void: return "void";
                    case Kind.Boolean: return "bool";
                    case Kind.Int32: return "int32";
                    case Kind.Int64: return "int64";
                    case Kind.Double: return "double";
                    case Kind.String: return "string";
                    default:
                        throw new InvalidOperationException("invalid parameter");
                }
            }
        }

        public override string Signature
        {
            get
            {
                return Name;
            }
        }

        public override bool IsObject { get { return _Kind == Kind.String; } }

        public override JToken AsJson()
        {
            return new JValue(Name);
        }

        Kind _Kind;
    }
}
