﻿using Antlr4.Runtime.Tree;
using System;
using System.Collections.Generic;

namespace oc
{
    /// <summary>
    /// scans top level symbols and caches them
    /// create scope object for each scope
    /// </summary>
    class DefineSymbolsPhase : oxideBaseVisitor<object>
    {
        ParseTreeDataStore _Data;
        NameScopeMap _Scopes;
        NameScope _CurrentScope;
        private CompilerCache _Cache;

        public DefineSymbolsPhase(NameScopeMap scopes, ParseTreeDataStore data, CompilerCache cache)
        {
            _Scopes = scopes;
            _CurrentScope = _Scopes.Root;
            _Data = data;
            _Cache = cache;
        }

        private void PushScope(IParseTree tree)
        {
            NameScope scope = new NameScope(_CurrentScope);
            _Scopes.Put(tree, scope);
            _CurrentScope = scope;
        }
        private void PopScope()
        {
            _CurrentScope = _CurrentScope.Parent;
        }

        public override object VisitModule(oxideParser.ModuleContext context)
        {
            PushScope(context);
            base.VisitModule(context);
            PopScope();
            return null;
        }

        public override object VisitItem_import(oxideParser.Item_importContext context)
        {
            _CurrentScope.Add(_Scopes.Imports[context.ID().GetText()].Public);
            return base.VisitItem_import(context);
        }

        public override object VisitItem_struct(oxideParser.Item_structContext context)
        {
            var sym = new Symbol(NameScope.FromItemContext(context.ident_or_generic()), Symbol.Kind.Type, false);
            _Data.Get(context).Symbol = sym;

            HandleGenericDeclarationList(context.ident_or_generic());
            
            _CurrentScope.Add(sym);
            PushScope(context);
            HandleGenericDeclarationList(context.ident_or_generic());
            base.VisitItem_struct(context);
            PopScope();
            return null;
        }

        public override object VisitItem_binding(oxideParser.Item_bindingContext context)
        {
            _CurrentScope.Add(new FunctionSymbol(NameScope.FromItemContext(context), false));
            PushScope(context);

            base.VisitItem_binding(context);
            PopScope();
            return null;
        }

        public override object VisitBinding_handler(oxideParser.Binding_handlerContext context)
        {
            var funcName = NameScope.UniqueFuncName();
            var sym = new FunctionSymbol(funcName, false);
            _CurrentScope.Add(sym);
            _Data.Get(context).Symbol = sym;

            PushScope(context);
            HandleParameterDeclarationList(context.fn_declaration().parameter_declaration_list());
            base.VisitBinding_handler(context);
            PopScope();
            return null;
        }

        public override object VisitItem_fn_declare(oxideParser.Item_fn_declareContext context)
        {
            var sym = new FunctionSymbol(NameScope.FromContext(context.ident_or_generic().ident()), context.PUB() != null);
            _CurrentScope.Add(sym);
            _Data.Get(context).Symbol = sym;

            PushScope(context);
            HandleGenericDeclarationList(context.ident_or_generic());
            base.VisitItem_fn_declare(context);
            PopScope();
            return null;
        }

        public override object VisitItem_fn_implement(oxideParser.Item_fn_implementContext context)
        {
            var sym = new FunctionSymbol(NameScope.FromContext(context.ident_or_generic().ident()), context.PUB() != null);
            _CurrentScope.Add(sym);
            _Data.Get(context).Symbol = sym;

            PushScope(context);

            HandleGenericDeclarationList(context.ident_or_generic());
            HandleParameterDeclarationList(context.fn_declaration().parameter_declaration_list());

            base.VisitItem_fn_implement(context);
            PopScope();
            return null;
        }

        public override object VisitLet_statement(oxideParser.Let_statementContext context)
        {
            // TODO: let in global context should create global symbol
            _CurrentScope.Add(new VariableSymbol(NameScope.FromContext(context), VariableSymbol.VariableKind.Local, false));
            return base.VisitLet_statement(context);
        }

        private void HandleGenericDeclarationList(oxideParser.Ident_or_genericContext context)
        {
            // add generic parameters to scope
            // at this point we do not know if parameters are actually generic or specific types
            // will figure out things later
            if (context.ty_list() != null)
            {
                foreach (var tyParam in context.ty_list().ty())
                {
                    var sym = new Symbol(NameScope.FromContext(tyParam), Symbol.Kind.Type, false);
                    _CurrentScope.Add(sym);
                    _Data.Get(tyParam).Symbol = sym;
                }
            }

            if (context.ty_variadic() != null)
            {
                var sym = new Symbol(context.ty_variadic().ID().GetText(), Symbol.Kind.Type, false);
                _CurrentScope.Add(sym);
                _Data.Get(context.ty_variadic()).Symbol = sym;
            }
        }

        void HandleParameterDeclarationList(oxideParser.Parameter_declaration_listContext p)
        {
            // get parameter names
            if (p == null)
                return;

            for(int i = 0;; i++)
            {
                var pp = p.parameter_declaration(i);
                if (pp == null)
                    break;

                _CurrentScope.Add(new VariableSymbol(NameScope.FromContext(pp), VariableSymbol.VariableKind.Param, false));
            }
        }
    }
}