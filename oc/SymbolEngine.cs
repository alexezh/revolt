﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace oc
{
    /// <summary>
    /// Provides query functionality over name scope tree
    /// Query method can instantiate template types if needed
    /// </summary>
    public class SymbolEngine
    {
        private NameScope _Scope;


        public SymbolEngine(NameScope scope)
        {
            _Scope = scope;
        }

        public IEnumerable<Symbol> Query(string name)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Symbol> Query(string name, Symbol.Kind kind)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Symbol> Query(string name, TypeDefinition type)
        {
            throw new NotImplementedException();
        }
    }

}
