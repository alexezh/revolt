﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace oc
{
    public class FunctionTypeDefinition : TypeDefinition
    {
        static private GenericType[] _EmptySet = new GenericType[0];

        private FunctionTypeDefinition(string name, TypeDefinition result, TypeDefinition[] args, TypeDefinition[] templateParams)
        {
            _Name = name;
            _Result = result;
            _Args = args;
            _GenericParams = genericParams;
        }

        static public FunctionTypeDefinition Create(string name, TypeDefinition result, TypeDefinition[] args, TypeDefinition[] genericArgs)
        {
            return new FunctionTypeDefinition(name, result, args, genericArgs == null ? _EmptySet : genericArgs);
        }
        static public FunctionTypeDefinition CreateGeneric(string name, string result, string signature)
        {
            Dictionary<string, GenericType> types = new Dictionary<string, GenericType>();
            types.Add(result, new GenericType(result, false));
            string[] args = signature.Split(';');
            foreach (var arg in args)
            {
                if (types.ContainsKey(arg))
                    continue;

                types.Add(arg, new GenericType(arg));
            }

            return new FunctionTypeDefinition(name, types[result], args.Select(x => types[x]).ToArray(), types.Values.ToArray());
        }

        public FunctionTypeDefinition(string name)
        {
            _Name = name;
        }


        public override string Name
        {
            get
            {
                return (_Name != null) ? _Name : Signature;
            }
        }
        public override string Signature
        {
            get
            {
                return string.Format("({1})=>{0}", _Result.Name, string.Join(",", _Args.Select(x => x.Name)));
            }
        }

        public override bool IsObject { get { return true; } }

        public override JToken AsJson()
        {
            
            throw new NotImplementedException();
        }

        public bool IsGeneric
        {
            get
            {
                return _GenericParams.Any(x => x is GenericType);
            }
        }

        public override bool Equals(System.Object obj)
        {
            TypeDefinition p = obj as TypeDefinition;
            if ((object)p == null)
            {
                return false;
            }

            return Equals((FunctionTypeDefinition)obj);
        }

        public bool Equals(FunctionTypeDefinition p)
        {
            if (!base.Equals(p))
                return false;

            if (!ArrayHelper.Equals(_Args, p._Args))
                return false;

            if (!_Result.Equals(p._Result))
                return false;

            return true;
        }

        /// <summary>
        /// checks if generic function matches concrete function
        /// </summary>
        /// <param name="func"></param>
        /// <returns></returns>
        public bool Match(FunctionTypeDefinition func)
        {
            /*
                        if (func.Args.Length != Args.Length)
                            return false;

                        Dictionary<string, TypeSymbol> dict = new Dictionary<string, TypeSymbol>();
                        for (int i = 0; i < _Args.Length; i++)
                        {
                            TypeSymbol ts;
                            if (!dict.TryGetValue(_Args[i], out ts))
                            {
                                dict.Add(_Args[i], func.Args[i]);
                            }
                            else
                            {
                                if (!ts.Equals(func.Args[i]))
                                {
                                    return false;
                                }
                            }
                        }
            */
            return false;
        }

        /// <summary>
        /// Creates new function definition for generic function by replacing first N generic params
        /// </summary>
        /// <param name="types"></param>
        /// <returns></returns>
        public FunctionTypeDefinition InstantiateExplicit(TypeDefinition[] genericParams)
        {
            if (genericParams.Length != _GenericParams.Length)
            {
                throw new InvalidOperationException("incorrect number of generic parameters");
            }

            TypeDefinition[] newArgs = new TypeDefinition[_Args.Length];

            for (int i = 0; i < _Args.Length; i++)
            {
                if (_Args[i] is GenericType)
                {
                    int genericIdx = ArrayHelper.Find(_Args[i], _GenericParams);
                    newArgs[i] = genericParams[i];
                }
                else
                {
                    newArgs[i] = _Args[i];
                }
            }

            TypeDefinition newResult = _Result;
            if (newResult is GenericType)
            {
                int genericIdx = ArrayHelper.Find(_Result, _GenericParams);
                newResult = genericParams[genericIdx];
            }

            FunctionTypeDefinition newFunc = new FunctionTypeDefinition(Name, newResult, newArgs, genericParams);

            return newFunc;
        }

        /// <summary>
        /// type definition can either be final type or another generic
        /// in latter case we want to keep the result function as generic
        /// </summary>
        /// <param name="args"></param>
        /// <returns></returns>
        public FunctionTypeDefinition InstantiateImplicit(TypeDefinition[] realArgs)
        {
            TypeDefinition[] resolvedArgs;
            if (!GenericTypeHelper.ResolveTypes(_Args, realArgs, out resolvedArgs))
                throw new InvalidOperationException("Internal error. Match should be called first");

            TypeDefinition[] newArgs = new TypeDefinition[_Args.Length];
            TypeDefinition[] genericSubst = new TypeDefinition[_GenericParams.Length];

            for (int i = 0; i < _Args.Length; i++)
            {
                if (_Args[i] is GenericType)
                {
                    int genericIdx = ArrayHelper.Find(_Args[i], _GenericParams);
                    if (genericSubst[genericIdx] != null)
                    {
                        // already substituted argument, check if it matches
                        if (!TypeDefinition.Match(genericSubst[genericIdx], resolvedArgs[i]))
                            throw new InvalidOperationException("Incorrect parameter");
                    }
                    else
                    {
                        genericSubst[genericIdx] = resolvedArgs[i];
                    }
                }
                else
                {
                    if (!TypeDefinition.Match(_Args[i], resolvedArgs[i]))
                        throw new InvalidOperationException("argument does not match");
                }

                newArgs[i] = resolvedArgs[i];
            }

            TypeDefinition newResult = _Result;
            if (_Result is GenericType)
            {
                int genericIdx = ArrayHelper.Find(_Result, _GenericParams);
                if (genericSubst[genericIdx] == null)
                    throw new InvalidOperationException("What???");

                newResult = genericSubst[genericIdx];
            }

            FunctionTypeDefinition newFunc = new FunctionTypeDefinition(Name, newResult, newArgs, genericSubst);

            return newFunc;
        }

        public TypeDefinition Result
        {
            get { return _Result; }
        }

        private string _Name;
        private TypeDefinition _Result;
        private TypeDefinition[] _Args;

        private TypeDefinition _Parent;

        public bool Intrinsic { get; set; }
    }
}
