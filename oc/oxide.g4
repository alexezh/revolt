grammar oxide;

@parser::namespace { OxideCompiler }
@lexer::namespace { OxideCompiler }

module : module_item* EOF;

module_item
		: item_import
		| item_static
		| item_fn
		| item_struct
		| item_binding
		;


item_import : IMPORT ID SEMI;
item_static : STATIC ID assignment_operator assignment_expression SEMI;
item_fn 
		: PUB FN ident_or_generic fn_declaration SEMI # item_fn_declare
		| PUB FN ident_or_generic fn_declaration code_block # item_fn_implement
		;

fn_declaration : LPAREN parameter_declaration_list? RPAREN fn_return?;
fn_return : (ARROW ty);

//attribute_declaration : LBRACKET attribute_list RBRACKET;
//attribute_list : attribute (COMMA attribute)*;
//attribute 
//		: ID
//		| ID EQUALS ICONST;

// binding
item_binding : 'input' COLON binding_parameters;
binding_parameters : binding_parameter (COMMA binding_parameter)*;
binding_parameter 
		: 'etw' EQUALS constant # etw
		| 'handler' EQUALS binding_handler # handler
		;

binding_handler : fn_declaration code_block;

// struct
item_struct 
		: 'struct' ident_or_generic LBRACE struct_field_list RBRACE
		| 'struct' ident_or_generic LPAREN ty_list RPAREN
		;

struct_field_list : (struct_field)+;
struct_field : ID COLON ty SEMI;
parameter_declaration_list : parameter_declaration (COMMA parameter_declaration)*;
parameter_declaration : ID COLON ty;
code_block : LBRACE statement_list? RBRACE;

// statements
statement_list : (statement)+;
statement
		: expression_statement
        | compound_statement
        | iteration_statement
        | selection_statement
        | return_statement
		| let_statement;

expression_statement : expression? SEMI;
compound_statement : LBRACE statement_list? RBRACE;
selection_statement 
		: IF LPAREN expression RPAREN branch_true=statement (ELSE branch_false=statement)?;

iteration_statement 
		: WHILE LPAREN expression RPAREN statement
		| FOR LPAREN expression? SEMI expression? SEMI expression? RPAREN statement
		| DO statement WHILE LPAREN expression RPAREN SEMI;

return_statement : RETURN expression? SEMI;

expression 
		: assignment_expression
		| expression COMMA assignment_expression;

assignment_expression
		: conditional_expression
		| unary_expression assignment_operator assignment_expression;

let_statement 
		: LET ident assignment_operator assignment_expression
		| LET LPAREN ident_list RPAREN assignment_operator assignment_expression;

assignment_operator : EQUALS;
conditional_expression 
		: logical_or_expression
		| logical_or_expression CONDOP expression COLON conditional_expression;

logical_or_expression
		: logical_and_expression
		| logical_or_expression LOR logical_and_expression;

logical_and_expression
		: inclusive_or_expression
		| logical_and_expression LAND inclusive_or_expression;

inclusive_or_expression 
		: exclusive_or_expression
		| inclusive_or_expression OR exclusive_or_expression;

exclusive_or_expression
		: and_expression
		| exclusive_or_expression XOR and_expression;

and_expression
		: equality_expression
		| and_expression AND equality_expression;

equality_expression
		: relational_expression
		| equality_expression EQ relational_expression
		| equality_expression NE relational_expression;

relational_expression
		: shift_expression
		| relational_expression (LT|GT|LE|GE) shift_expression;


// we do not support shift, so it is just additive
shift_expression : additive_expression;

// def p_shift_expression_2(t):
//    'shift_expression : shift_expression LSHIFT additive_expression'
//    pass

//def p_shift_expression_3(t):
//    'shift_expression : shift_expression RSHIFT additive_expression'
//    pass

additive_expression 
		: multiplicative_expression
		| additive_expression op=(PLUS|MINUS) multiplicative_expression;

multiplicative_expression
		: cast_expression
		| multiplicative_expression op=(TIMES|DIVIDE|MOD) cast_expression;

cast_expression : unary_expression;

// def p_cast_expression_2(t):
//    'cast_expression : LPAREN type_name RPAREN cast_expression'
//    pass

unary_expression
		: postfix_expression
		| op=(PLUSPLUS|MINUSMINUS) unary_expression
		| unary_operator cast_expression;
    
unary_operator 
		: op=(TIMES|PLUS|MINUS|NOT|LNOT);

postfix_expression
		: primary_expression # postfix_expression_primary
		| postfix_expression LBRACKET expression RBRACKET # postfix_expression_bracket
		| postfix_expression LPAREN expression_list? RPAREN # postfix_expression_paren
		| postfix_expression PERIOD ID # postfix_expression_period
		| postfix_expression (PLUSPLUS|MINUSMINUS) # postfix_expression_arithmetic
		; 

primary_expression
		: ident_or_generic # primary_expression_ident
		| constant # primary_expression_constant
		| LPAREN expression RPAREN # primary_expression_paren
		| LPAREN expression COMMA RPAREN # primary_expression_tuple_single
		| LPAREN expression (COMMA expression)+ RPAREN # primary_expression_tuple
		;

constant 
		: IntegerConstant
		| FloatingConstant
		| CharacterConstant
		| StringConstant
		| 'true'
		| 'false'
		;

primary_expression_list 
		: primary_expression (COMMA primary_expression);

expression_list
		: expression (COMMA expression);

// just ID
ident : ID;
ident_or_generic 
	: ident (LT ty_list GT)?
	| ident (LT (ty_list COMMA)? (ty_variadic)? GT)?
	;

// ident_or_generic used when using generics
// generic definition used when defining generic and can contain variadic
// should be ident or generic declaration
// generic_declaration : ident (LT (ty_list COMMA)? (ty_variadic)? GT)?;
ident_list : ident (COMMA ident)*;
ty_list 
		: ty (COMMA ty)*;

ty_list_variadic 
		: (ty_list COMMA)? ID VARIADIC;

ty 
		: LPAREN ty_list RPAREN # ty_tuple
		| ID LT ty_list GT # ty_generic
		| ID LT ty_list_variadic GT # ty_genericVariadic
		| ID # ty_id
		;

ty_or_variadic
		: ty # Simple
		| ty_variadic # Variadic
		;

ty_variadic	: ID VARIADIC;

/*
 * Lexer Rules
 */

PLUS	: '+';
MINUS	: '-';
TIMES	: '*';
DIVIDE	: '/';
MOD		: '%';
OR		: '|';
AND		: '&';
NOT		: '~';
XOR		: '^';
//#t_LSHIFT           = r'<<'
//#t_RSHIFT           = r'>>'
LOR		: '||';
LAND	: '&&';
LNOT	: '!';
LT		: '<';
GT		: '>';
LE      : '<=';
GE      : '>=';
EQ      : '==';
NE		: '!=';

EQUALS	: '=';

PLUSPLUS	: '++';
MINUSMINUS	: '--';
ARROW       : '->';
VARIADIC	: '...';

// Delimeters
LPAREN		: '(';
RPAREN      : ')';
LBRACKET    : '[';
RBRACKET    : ']';
LBRACE      : '{';
RBRACE      : '}';
COMMA       : ',';
PERIOD      : '.';
SEMI        : ';';
COLON       : ':';

// ?
CONDOP		: '?';


//ICONST
//	: '-'? ('0'..'9')+
//	;

//FCONST
//	: '-'? ('0'..'9')+'.'('0'..'9')*
//	;

//SCONST
//	: '"'CCharSequence'"';


IF		: 'if';
ELSE	: 'else';
DO		: 'do';
WHILE	: 'while';
FOR		: 'for';
RETURN	: 'return';
LET		: 'let';
FN		: 'fn';
EVENT	: 'event';
PUB		: 'pub';
STRUCT	: 'struct';
IMPORT	: 'import';
//fragment
//STATIC	: 'static';
//STRUCT	: 'struct';

ID
    :   IdentifierNondigit
        (   IdentifierNondigit
        |   Digit
        )*
    ;

fragment
IdentifierNondigit
    :   Nondigit
    |   UniversalCharacterName
    //|   // other implementation-defined characters...
    ;

fragment
Nondigit
    :   [a-zA-Z_]
    ;

fragment
Digit
    :   [0-9]
    ;

fragment
UniversalCharacterName
    :   '\\u' HexQuad
    |   '\\U' HexQuad HexQuad
    ;

fragment
HexQuad
    :   HexadecimalDigit HexadecimalDigit HexadecimalDigit HexadecimalDigit
    ;

/*
Constant
    :   IntegerConstant
    |   FloatingConstant
    //|   EnumerationConstant
    |   CharacterConstant
	|   StringConstant
    ;
*/

//fragment
IntegerConstant
    :   DecimalConstant IntegerSuffix?
    |   OctalConstant IntegerSuffix?
    |   HexadecimalConstant IntegerSuffix?
    ;

fragment
DecimalConstant
    :   NonzeroDigit Digit*
    ;

fragment
OctalConstant
    :   '0' OctalDigit*
    ;

fragment
HexadecimalConstant
    :   HexadecimalPrefix HexadecimalDigit+
    ;

fragment
HexadecimalPrefix
    :   '0' [xX]
    ;

fragment
NonzeroDigit
    :   [1-9]
    ;

fragment
OctalDigit
    :   [0-7]
    ;

fragment
HexadecimalDigit
    :   [0-9a-fA-F]
    ;

fragment
IntegerSuffix
    :   UnsignedSuffix LongSuffix?
    |   UnsignedSuffix LongLongSuffix
    |   LongSuffix UnsignedSuffix?
    |   LongLongSuffix UnsignedSuffix?
    ;

fragment
UnsignedSuffix
    :   [uU]
    ;

fragment
LongSuffix
    :   [lL]
    ;

fragment
LongLongSuffix
    :   'll' | 'LL'
    ;

// fragment
FloatingConstant
    :   DecimalFloatingConstant
    |   HexadecimalFloatingConstant
    ;

fragment
DecimalFloatingConstant
    :   FractionalConstant ExponentPart? FloatingSuffix?
    |   DigitSequence ExponentPart FloatingSuffix?
    ;

fragment
HexadecimalFloatingConstant
    :   HexadecimalPrefix HexadecimalFractionalConstant BinaryExponentPart FloatingSuffix?
    |   HexadecimalPrefix HexadecimalDigitSequence BinaryExponentPart FloatingSuffix?
    ;

fragment
FractionalConstant
    :   DigitSequence? '.' DigitSequence
    |   DigitSequence '.'
    ;

fragment
ExponentPart
    :   'e' Sign? DigitSequence
    |   'E' Sign? DigitSequence
    ;

fragment
Sign
    :   '+' | '-'
    ;

fragment
DigitSequence
    :   Digit+
    ;

fragment
HexadecimalFractionalConstant
    :   HexadecimalDigitSequence? '.' HexadecimalDigitSequence
    |   HexadecimalDigitSequence '.'
    ;

fragment
BinaryExponentPart
    :   'p' Sign? DigitSequence
    |   'P' Sign? DigitSequence
    ;

fragment
HexadecimalDigitSequence
    :   HexadecimalDigit+
    ;

fragment
FloatingSuffix
    :   'f' | 'l' | 'F' | 'L'
    ;

// fragment
StringConstant
	:   '"' CCharSequence '"'
	;

// fragment
CharacterConstant
    :   '\'' CCharSequence '\''
    |   'L\'' CCharSequence '\''
    |   'u\'' CCharSequence '\''
    |   'U\'' CCharSequence '\''
    ;

fragment
CCharSequence
    :   CChar+
    ;

fragment
CChar
    :   ~['\\\r\n]
    |   EscapeSequence
    ;

fragment
EscapeSequence
    :   SimpleEscapeSequence
    |   OctalEscapeSequence
    |   HexadecimalEscapeSequence
    |   UniversalCharacterName
    ;

fragment
SimpleEscapeSequence
    :   '\\' ['"?abfnrtv\\]
    ;

fragment
OctalEscapeSequence
    :   '\\' OctalDigit
    |   '\\' OctalDigit OctalDigit
    |   '\\' OctalDigit OctalDigit OctalDigit
    ;

fragment
HexadecimalEscapeSequence
    :   '\\x' HexadecimalDigit+
    ;

StringLiteral
    :   EncodingPrefix? '"' SCharSequence? '"'
    ;

fragment
EncodingPrefix
    :   'u8'
    |   'u'
    |   'U'
    |   'L'
    ;

fragment
SCharSequence
    :   SChar+
    ;

fragment
SChar
    :   ~["\\\r\n]
    |   EscapeSequence
    ;

//ID
//	: [_a..zA..Z][_a..zA..Z0..9]*
//	;

COMMENT
	: '/*' .*? '*/' -> channel(HIDDEN)
	;

LINE_COMMENT
	: '//' ~('\n'|'\r')* '\r'? '\n' -> channel(HIDDEN)
	;

WS
	: [ \n\r\t]+ -> skip
	;
