﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace oc
{
    public enum LocationType
    {
        Constant = 1,
        Temp = 2,
        Stack = 3,
        Global = 4,
        Heap = 5,
        Accumulator = 6,
    }

    public class StorageLocation
    {
        public LocationType LocType;
        public string Value;
        public TypeDefinition TypeDef;
        public bool Temp;
        public string VarName;

        public StorageLocation(LocationType type, string value = null)
        {
            LocType = type;
            Value = value;
        }
        public StorageLocation(LocationType type, TypeDefinition typeDef, string value = null)
        {
            LocType = type;
            TypeDef = typeDef;
            Value = value;
        }

        public string QualifiedLocation
        {
            get
            {
                if (LocType == LocationType.Stack || LocType == LocationType.Temp)
                {
                    return string.Format("bp+{0}", Value);
                }
                else if (LocType == LocationType.Global || LocType == LocationType.Heap || LocType == LocationType.Constant)
                {
                    return Value;
                }
                else if (LocType == LocationType.Accumulator)
                {
                    return "ra";
                }
                else
                {
                    throw new InvalidOperationException("Unknown location");
                }
            }
        }

        public override string ToString()
        {
            return QualifiedLocation;
        }
    }

    public enum AsmOpKind
    {
        Label,
        Function,
        Stop,
        Jump,
        Call,
        Return,
        AllocTuple,
        AllocObject,
        GetTupleField,
        GetTupleFields,
        Store,
        StoreObject,
        Br,
        Cmp,
        Not,
        Add,
        Sub,
        Mul,
        Div,
        Inc,
        Dec,
        Lor,
        Land,
        UnaryMinus,
        ReadEvent,
        Send,
        AddKey,
        RemoveKey,
        GetKey,

        // internal
        AllocStorage,
        FreeStorage,
    }


    delegate string AsmOpTextHandler(AsmOp op, TypeDefinition typeDef);

    class AsmOpAttr
    {
        public AsmOpAttr(AsmOpKind kind, string text, bool Typed, bool combineType = false)
        {
            Kind = kind;
            Text = text;
            CombineType = combineType;
        }
        public AsmOpKind Kind;
        public string Text;
        public bool Typed;
        public bool CombineType;

        string CombineTypeWorker(AsmOp op, TypeDefinition typeDef)
        {
            /*
            if type.isInt32():
                return op + 'Int32'
            elif type.isInt64():
                return op + 'Int64'
            elif type.isDouble():
                return op + 'Double'
            else:
                raise Exception('Unsupported type')
   
             */
            return "";
        }

        static AsmOpAttr[] Attrs = new AsmOpAttr[28] { 
        new AsmOpAttr(AsmOpKind.Label, null, false),
        new AsmOpAttr(AsmOpKind.Stop, "stop", false),
        new AsmOpAttr(AsmOpKind.Jump, "jump", false),
        new AsmOpAttr(AsmOpKind.Function, "function", false),
        new AsmOpAttr(AsmOpKind.Call, "call", false),
        new AsmOpAttr(AsmOpKind.Return, "return", false),
        new AsmOpAttr(AsmOpKind.AllocTuple, "allocTuple", false),
        new AsmOpAttr(AsmOpKind.AllocObject, "allocObject", false),
        new AsmOpAttr(AsmOpKind.GetTupleField, "getTupleField", false),
        new AsmOpAttr(AsmOpKind.GetTupleFields, "getTupleFields", false),
        new AsmOpAttr(AsmOpKind.Store, "store", false),
        new AsmOpAttr(AsmOpKind.StoreObject, "storeObject", false),
        new AsmOpAttr(AsmOpKind.Br, "br", false),
        new AsmOpAttr(AsmOpKind.Cmp, "cmp", true),
        new AsmOpAttr(AsmOpKind.Not, "not", false),
        new AsmOpAttr(AsmOpKind.Add, "add", false, true),
        new AsmOpAttr(AsmOpKind.Sub, "sub", false, true),
        new AsmOpAttr(AsmOpKind.Mul, "mul", false, true),
        new AsmOpAttr(AsmOpKind.Div, "div", false, true),
        new AsmOpAttr(AsmOpKind.Inc, "inc", false, true),
        new AsmOpAttr(AsmOpKind.Dec, "dec", false, true),
        new AsmOpAttr(AsmOpKind.Lor, "lor", false),
        new AsmOpAttr(AsmOpKind.Land, "land", false),
        new AsmOpAttr(AsmOpKind.ReadEvent, "readEvent", false, true),
        new AsmOpAttr(AsmOpKind.Send, "send", false),
        new AsmOpAttr(AsmOpKind.AddKey, "addKey", false),
        new AsmOpAttr(AsmOpKind.RemoveKey, "removeKey", false),
        new AsmOpAttr(AsmOpKind.GetKey, "getKey", false),
    };

        public string AsString()
        {
            return Text;
        }

        internal static AsmOpAttr Get(AsmOpKind Kind)
        {
            return null;
        }
    }

    public class AsmOp
    {
        public AsmOpKind Kind;
        TypeDefinition TypeDef;
        object[] Args;

        static public AsmOp Make(AsmOpKind kind, object obj1)
        {
            return new AsmOp(kind, null, new object[1] { obj1 });
        }
        static public AsmOp Make(AsmOpKind kind, object obj1, object obj2)
        {
            return new AsmOp(kind, null, new object[2] { obj1, obj2 });
        }
        static public AsmOp Make(AsmOpKind kind, object obj1, object obj2, object obj3)
        {
            return new AsmOp(kind, null, new object[3] { obj1, obj2, obj3 });
        }
        static public AsmOp MakeTyped(AsmOpKind kind, TypeDefinition typeDef, object obj1)
        {
            return new AsmOp(kind, typeDef, new object[1] { obj1 });
        }
        static public AsmOp MakeTyped(AsmOpKind kind, TypeDefinition typeDef, object obj1, object obj2)
        {
            return new AsmOp(kind, typeDef, new object[2] { obj1, obj2 });
        }

        static public AsmOp MakeStoreOp(TypeDefinition typeDef, StorageLocation target, StorageLocation src, bool init)
        {
            return new AsmOp(AsmOpKind.Store, typeDef, new object[3] { target, src, init });
        }
        public static AsmOp MakeCall(Symbol sym, StorageLocation[] args)
        {
            return new AsmOp(AsmOpKind.Call, null, new object[2] { sym, args });
        }
        internal static AsmOp MakeLabel(string p)
        {
            return new AsmOp(AsmOpKind.Label, null, new object[1] { p });
        }
        internal static AsmOp MakeFunction(string p)
        {
            return new AsmOp(AsmOpKind.Function, null, new object[1] { p });
        }
        internal static AsmOp MakeReturn(StorageLocation storageLocation = null)
        {
            return new AsmOp(AsmOpKind.Return, null, (storageLocation != null) ? new object[1] { storageLocation } : new object[0]);
        }

        public AsmOp(AsmOpKind kind, TypeDefinition typeDef, object[] args)
        {
            Kind = kind;
            Args = args;
            TypeDef = typeDef;
        }

        public bool IsTyped
        {
            get
            {
                return AsmOpAttr.Get(Kind).Typed;
            }
        }

        public bool IsExternal
        {
            get
            {
                return Kind == AsmOpKind.Send;
            }
        }

        public bool IsInternal
        {
            get
            {
                return Kind == AsmOpKind.AllocStorage || Kind == AsmOpKind.FreeStorage || Kind == AsmOpKind.Function;
            }
        }

        public override string ToString()
        {
            if(Args != null)
            {
                return string.Format("{0} {1}", Kind.ToString(), 
                        string.Join(",", Args.Select(x => x is Array ? string.Join(",", x as object[]) : x.ToString()))); 
            }
            else
            {
                return string.Format("{0}", Kind.ToString()); 
            }
        }

        public string AsString()
        {
            if (Kind == AsmOpKind.Label)
                throw new InvalidOperationException("Internal error");

            return AsmOpAttr.Get(Kind).AsString();
        }
    }

    public class AsmVarAllocator
    {
        // free should be sorted so we reuse same addresses more often
        List<StorageLocation> _Free = new List<StorageLocation>();
        List<StorageLocation> _Used = new List<StorageLocation>();
        int _NextTemp = 0;

        public StorageLocation AllocVar(ScriptImplementation scipt, TypeDefinition typeDef, bool temp = true, string name = null)
        {
            StorageLocation loc;

            if (_Free.Count == 0)
            {
                loc = new StorageLocation(LocationType.Stack, _NextTemp.ToString());
                _NextTemp++;
            }
            else
            {
                loc = _Free[0];
                _Free.RemoveAt(0);
            }

            loc.TypeDef = typeDef;
            loc.Temp = temp;
            loc.VarName = name;

            scipt.Add(AsmOp.Make(AsmOpKind.AllocStorage, loc));
            _Used.Add(loc);
            return loc;
        }

        public StorageLocation AllocAcc(ScriptImplementation module, TypeDefinition typeDef)
        {
            StorageLocation loc = new StorageLocation(LocationType.Accumulator);
            loc.TypeDef = typeDef;
            loc.Temp = true;
            module.Add(AsmOp.Make(AsmOpKind.AllocStorage, loc));
            return loc;
        }

        public StorageLocation FindVar(string name)
        {
            return _Used.Where(x => x.VarName == name).First();
        }

        public void FreeTemp(ScriptImplementation module, StorageLocation loc)
        {
            if (loc.Temp)
            {
                FreeStorage(module, loc);
            }
        }
        public void FreeStorage(ScriptImplementation module, StorageLocation loc)
        {
            if (loc.LocType == LocationType.Stack)
            {
                _Free.Add(loc);
            }

            if (loc.LocType == LocationType.Stack || loc.LocType == LocationType.Accumulator)
            {
                module.Add(AsmOp.Make(AsmOpKind.FreeStorage, loc));
            }

            _Used.Remove(loc);
        }

        /// <summary>
        /// free all local variables
        /// </summary>
        /// <param name="module"></param>
        public void FreeAllLocals(ScriptImplementation module)
        {
            foreach(var temp in _Used)
            {
                if(temp.TypeDef.IsObject)
                {
                    if (temp.LocType == LocationType.Stack || temp.LocType == LocationType.Accumulator)
                    {
                        module.Add(AsmOp.Make(AsmOpKind.FreeStorage, temp));
                    }
                }

                if (temp.LocType == LocationType.Stack)
                {
                    _Free.Add(temp);
                }
            }
            // self.generateFunctionExit()
            _Used.Clear();
        }
    }
}
