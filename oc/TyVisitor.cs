﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace oc
{
    abstract class TyVisitor
    {
        protected ParseTreeDataStore _Data;
        protected NameScope _CurrentScope;

        protected TyVisitor(NameScope currentScope, ParseTreeDataStore data)
        {
            _CurrentScope = currentScope;
            _Data = data;
        }

        abstract public ResolveTypeResult VisitTy_id(oxideParser.Ty_idContext context);
        abstract public ResolveTypeResult VisitTy_generic(oxideParser.Ty_genericContext context);
        abstract public ResolveTypeResult VisitTy_variadic(oxideParser.Ty_variadicContext context);
    }

    class TyVisitor_GenericParam : TyVisitor
    {
        public override ResolveTypeResult VisitTy_id(oxideParser.Ty_idContext context)
        {
            var sym = _Data.Get(context).Symbol;

            // first check if is coming from parent scope, use it instead
            var parentTypes = _CurrentScope.Parent.Find(sym.Name, x => x.SymbolKind == Symbol.Kind.Type);
            var parentType = parentTypes.FirstOrDefault();
            if (parentType != null)
            {
                sym.TypeDef = parentType.TypeDef;
            }
            else
            {
                // type is generic without restrictions
                sym.TypeDef = new GenericType(context.GetText());
            }

            _Data.Get(context).Symbol = sym;
            return new ResolveTypeResult(sym);
        }

        public override ResolveTypeResult VisitTy_generic(oxideParser.Ty_genericContext context)
        {
            var sym = _Data.Get(context).Symbol;
            sym.TypeDef = new GenericType(context.ID().GetText(), true);
            return new ResolveTypeResult(sym);
        }

        public override ResolveTypeResult VisitTy_variadic(oxideParser.Ty_variadicContext context)
        {
            var sym = _Data.Get(context).Symbol;
            sym.TypeDef = new GenericType(context.ID().GetText(), true);
            return new ResolveTypeResult(sym);
        }

        public TyVisitor_GenericParam(NameScope currentScope, ParseTreeDataStore data)
            : base(currentScope, data)
        {
        }
    }

    class TyVisitor_FnBody : TyVisitor
    {
        public override ResolveTypeResult VisitTy_id(oxideParser.Ty_idContext context)
        {
            var sym = _CurrentScope.FindFirst(context.GetText(), Symbol.Kind.Type);
            _Data.Get(context as oxideParser.TyContext).Symbol = sym;
            return new ResolveTypeResult(sym);
        }

        public override ResolveTypeResult VisitTy_generic(oxideParser.Ty_genericContext context)
        {
            // GenericTypeHelper.ResolveTypes()
            // need to match with substitution
            var sym = _CurrentScope.FindFirst(NameScope.FromContext(context), Symbol.Kind.Type);
            throw new NotImplementedException();
        }

        public override ResolveTypeResult VisitTy_variadic(oxideParser.Ty_variadicContext context)
        {
            throw new NotImplementedException("What");
        }

        public TyVisitor_FnBody(NameScope currentScope, ParseTreeDataStore data)
            : base(currentScope, data)
        {
        }
    }

}
