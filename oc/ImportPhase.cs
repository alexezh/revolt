﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace oc
{
    /// <summary>
    /// extract all imports
    /// </summary>
    class ImportPhase : oxideBaseListener
    {
        public List<string> Imports = new List<string>();

        public override void EnterItem_import(oxideParser.Item_importContext context)
        {
            Imports.Add(context.ID().GetText());
            base.EnterItem_import(context);
        }
    }
}
