﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace oc
{
    /// <summary>
    /// struct type
    /// </summary>
    public class StructTypeDefinition : TypeDefinition
    {
        static private TypeDefinition[] _EmptySet = new GenericType[0];
        public StructTypeDefinition(string name, Field[] field, TypeDefinition[] templateParams)
        {
            _Name = name;
            _Fields = field;
            _TemplateParams = (templateParams != null) ? templateParams : _EmptySet;
        }

        public override string Name
        {
            get
            {
                return _Name;
            }
        }
        public override string Signature
        {
            get
            {
                return string.Format("({0})", string.Join(",", _Fields.Select(x => string.Format("{0}:{1}", x.Name, x.Type.Name))));
            }
        }

        public override bool IsObject { get { return true; } }

        public override bool Equals(System.Object obj)
        {
            StructTypeDefinition p = obj as StructTypeDefinition;
            if ((object)p == null)
            {
                return false;
            }

            return Equals((StructTypeDefinition)obj);
        }

        public override JToken AsJson()
        {
            return new JObject(
                    new JProperty("name", Name),
                    new JProperty("type", "struct"),
                    new JProperty("fields", new JArray(_Fields.Select(x => new JObject(new JProperty("name", x.Name), new JProperty("type", x.Type.Name))).ToArray())));
        }

        public bool Equals(StructTypeDefinition p)
        {
            return ArrayHelper.Equals(_Fields, p._Fields);
        }

        public struct Field
        {
            public bool Equals(Field p)
            {
                return Name.Equals(p.Name) && Type.Equals(p.Type);
            }

            public string Name;
            public TypeDefinition Type;

            public Field(string name, TypeDefinition type)
            {
                Name = name;
                Type = type;
            }
        }

        // this is not sufficient to handle T : U[E] type specification
        public TypeDefinition InstantiateTemplate(TypeDefinition[] templateArgs)
        {
            if (templateArgs.Length != _TemplateParams.Length)
            {
                throw new InvalidOperationException("incorrect number of template parameters");
            }

            Field[] newFields = new Field[_Fields.Length];
            TypeDefinition[] newParams = new TypeDefinition[_TemplateParams.Length];

            for (int i = 0; i < _Fields.Length; i++ )
            {
                newFields[i].Name = _Fields[i].Name;
                if (_Fields[i].Type is GenericType)
                {
                    int paramIdx = ArrayHelper.Find(_Fields[i].Type, _TemplateParams);
                    newFields[i].Type = _TemplateParams[paramIdx];
                }
                else
                {
                    newFields[i].Type = _Fields[i].Type;
                }
            }

            StructTypeDefinition newType = new StructTypeDefinition(Name, newFields, newParams);

            return newType;
        }

        string _Name;
        Field[] _Fields;

        StructTypeDefinition _Parent;
    }
}
