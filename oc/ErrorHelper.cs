﻿using Antlr4.Runtime.Tree;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace oc
{
    class ErrorHelper
    {
        public static Exception InvalidToken(IParseTree tree)
        {
            return new InvalidOperationException("Invalid token");
        }
        public static Exception NotImplemented(IParseTree tree)
        {
            return new NotImplementedException();
        }

        internal static Exception InvalidExpression(IParseTree tree, string message)
        {
            return new InvalidOperationException(message);
        }

        internal static Exception VariableNotInitialized(IParseTree tree)
        {
            return new InvalidOperationException("Variable not initialized");
        }
    }
}
