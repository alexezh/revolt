﻿using Antlr4.Runtime;
using Antlr4.Runtime.Tree;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using CommandLine;
using CommandLine.Text;

namespace oc
{
    //ImportPhase
    //DefinePhase
    //ResolvePhase
    //TypeProcessPhase
    //CodeGenPhase

    namespace Ast
    {
        class OModule
        { 
        }
        class ONode
        { 
        }
        class OExpression
        { 
        }
        class OMethod
        { 
        }
        class OParameter
        { 
        }
        class OType
        { 
        }
    }

    class Program
    {
        static CompilerCache _Cache = new CompilerCache();

        static SourceFile CompileFile(string fileName, int depth = 0)
        {
            StreamReader inputStream = new StreamReader(fileName);
            AntlrInputStream input = new AntlrInputStream(inputStream.ReadToEnd());
            oxideLexer lexer = new oxideLexer(input);
            CommonTokenStream tokens = new CommonTokenStream(lexer);
            oxideParser parser = new oxideParser(tokens);

            parser.RemoveErrorListeners();
            parser.AddErrorListener(new VerboseListener());

            IParseTree tree = parser.module();

            NameScopeMap scopes = new NameScopeMap();
            scopes.Root = new NameScope(null);
            BuiltInTypes.Populate(scopes.Root);

            ParseTreeWalker walker = new ParseTreeWalker();

            ImportPhase importPhase = new ImportPhase();
            walker.Walk(importPhase, tree);

            // for each import, get exports (shallow)
            foreach(var imp in importPhase.Imports)
            {
                var importFile = _Cache.TryGet(imp);
                if (importFile == null)
                {
                    importFile = CompileFile(imp + ".oxide", depth+1);
                }

                scopes.Imports.Add(imp, importFile);
                // scopes.Root.Add(importFile.Public);
            }

            ParseTreeDataStore store = new ParseTreeDataStore();

            DefineSymbolsPhase defineSymbols = new DefineSymbolsPhase(scopes, store, _Cache);
            defineSymbols.Visit(tree);

            // ResolveSymbolsPhase resolveSymbols = new ResolveSymbolsPhase(scopes, store);
            //resolveSymbols.Visit(tree);

            ResolveTypePhase resolveTypes = new ResolveTypePhase(scopes, store);
            resolveTypes.Visit(tree);

            if (depth == 0)
            {
                AsmGenPhase asmPhase = new AsmGenPhase(scopes, store);
                asmPhase.Visit(tree);
            }

            // as last step, export fully resolved definition of public functions
            PublicPhase publicPhase = new PublicPhase(scopes);
            publicPhase.Process();

            SourceFile sourceFile = new SourceFile() { Public = publicPhase.Exports.Values, Impl = (depth == 0) ? AsmFileWriter.Write(scopes).ToString() : null };
            _Cache.Add(fileName, sourceFile);

            return sourceFile;
        }

        static void Main(string[] args)
        {
            var options = new CommandLineOptions();
            if (!CommandLine.Parser.Default.ParseArguments(args, options))
            {
                Console.WriteLine(options.GetUsage());
                Environment.Exit(-1);
            }

            foreach (var fileName in options.Items)
            {
                var srcFile = CompileFile(args[0]);

                Console.Write(srcFile.Impl);
                // srcFile.Print();
            }
        }
    }
}
