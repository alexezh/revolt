﻿using Antlr4.Runtime.Tree;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace oc
{
    public class ModuleSymbolTable
    {
        public void AddImport(ModuleSymbolTable sym)
        { 
        }

        private List<ModuleSymbolTable> Imports = new List<ModuleSymbolTable>();
    }

    public class NameScope
    {
        private static int _NextId = 1;
        private static List<Symbol> _EmptyBucket = new List<Symbol>();

        public static string UniqueFuncName()
        {
            string s = "__Func" + _NextId;
            _NextId++;
            return s;
        }

        public NameScope(NameScope parent)
        {
            _Parent = parent;
        }

        public Symbol FindFirst(string s, Symbol.Kind kind)
        {
            return Find(s).Where(x => x.SymbolKind == kind).First();
        }

        public delegate bool FindHandler(Symbol kind);

        public Symbol FindFirst(string s, FindHandler handler)
        {
            return Find(s, handler).First();
        }

        public IEnumerable<Symbol> Find(string s, FindHandler handler)
        {
            return Find(s).Where(x => handler(x));
        }

        public IEnumerable<Symbol> Find(string s)
        {
            List<Symbol> bucket = null;
            if (!_Symbols.TryGetValue(s, out bucket))
            {
                bucket = _EmptyBucket;
            }

            if (_Parent != null)
                return bucket.Concat(_Parent.Find(s));

            return bucket;
        }

        public Symbol FindFunction(string s, TypeDefinition[] args)
        {
            var funcSym = Find(s).Where(x =>
            {
                if (!x.IsFunction)
                    return false;

                return x.AsFunction.TypeDef.MatchArgs(args);
            }).First();

            var func = funcSym.AsFunction.TypeDef;

            // if we get generic function, create an instance with specific types
            // in this scope. We might have types specific to scope; we will combine them later
            if (func.IsGeneric)
            {
                funcSym = Add(new FunctionSymbol(funcSym.Name, false, func.InstantiateImplicit(args)));
            }

            return funcSym;
        }

        /// <summary>
        /// find or create (for generic) type
        /// </summary>
        /// <param name="p"></param>
        /// <returns></returns>
        public Symbol FindType(string s, TypeDefinition[] templateArgs)
        {
            var typeSym = Find(s).Where(x =>
            {
                if (!x.IsType)
                    return false;

                return x.TypeDef.MatchTemplateArgs(templateArgs);
            }).First();

            var type = typeSym.TypeDef;

            if (type.IsTemplate)
            {
                typeSym = Add(new Symbol(typeSym.Name, false, type.InstantiateTemplate(args)));
            }

            return typeSym;
        }

        public void Add(IEnumerable<Symbol> symbols)
        {
            foreach (var sym in symbols)
            {
                List<Symbol> bucket;
                if (!_Symbols.TryGetValue(sym.Name, out bucket))
                {
                    _Symbols.Add(sym.Name, bucket = new List<Symbol>());
                }

                bucket.Add(sym);
            }
        }

        public T Add<T>(T sym) where T : Symbol
        {
            List<Symbol> bucket;
            if (!_Symbols.TryGetValue(sym.Name, out bucket))
            {
                _Symbols.Add(sym.Name, bucket = new List<Symbol>());
            }

            bucket.Add(sym);

            return sym;
        }

        public IEnumerable<Symbol> All
        {
            get
            {
                IEnumerable<Symbol> symbols = new List<Symbol>();
                foreach (var bucket in _Symbols.Values)
                {
                    symbols = symbols.Concat(bucket);
                }
                return symbols;
            }
        }

        public static string FromItemContext(oxideParser.Item_bindingContext context)
        {
            // we do not have name so we use etw instead
            foreach (var p in context.binding_parameters().binding_parameter())
            {
                if (p is oxideParser.EtwContext)
                {
                    return ((oxideParser.EtwContext)p).constant().GetText();
                }
            }

            throw new InvalidOperationException("must have etw name");
        }

        public static string FromItemContext(oxideParser.Ident_or_genericContext context)
        {
            return context.ident().ID().GetText();
        }

        internal static string FromContext(oxideParser.Ident_or_genericContext context)
        {
            throw new NotImplementedException();
        }

        internal static string FromContext(oxideParser.Let_statementContext context)
        {
            return context.ident().ID().GetText();
        }

        public static string FromContext(oxideParser.Parameter_declarationContext pp)
        {
            return pp.ID().GetText();
        }

        public static string FromContext(oxideParser.TyContext ty)
        {
            if (ty is oxideParser.Ty_tupleContext)
            {
                return "(" + string.Join(",", ((oxideParser.Ty_tupleContext)ty).ty_list().ty().Select(x => x.GetText())) + ")";
            }
            else if (ty is oxideParser.Ty_genericContext)
            {
                var tyGen = (oxideParser.Ty_genericContext)ty;
                return tyGen.ID().GetText() + "<" + string.Join(",", tyGen.ty_list().ty().Select(x => x.GetText())) + ">";
            }
            else if (ty is oxideParser.Ty_idContext)
            {
                return ((oxideParser.Ty_idContext)ty).ID().GetText();
            }
            else
            {
                throw new InvalidOperationException("Unknown token type");
            }
        }


        internal static string FromContext(oxideParser.IdentContext context)
        {
            return context.GetText();
        }

        public NameScope Parent { get { return _Parent; } }

        private Dictionary<string, List<Symbol>> _Symbols = new Dictionary<string, List<Symbol>>();
        private NameScope _Parent;

        internal static string FromContext(ITerminalNode terminalNode)
        {
            throw new NotImplementedException();
        }
    }

    public class NameScopeMap : ParseTreeProperty<NameScope>
    {
        public NameScope Root;
        public Dictionary<string, SourceFile> Imports = new Dictionary<string,SourceFile>();

        public IEnumerable<NameScope> Values
        {
            get
            {
                return annotations.Values;
            }
        }

        public IEnumerable<Symbol> All
        {
            get
            {
                IEnumerable<Symbol> all = new List<Symbol>();

                foreach (var scope in annotations.Values)
                {
                    all = all.Concat(scope.All);
                }

                return all;
            }
        }
    }
}
