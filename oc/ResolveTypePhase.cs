﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using Antlr4.Runtime.Tree;

namespace oc
{
    public class ResolveTypeResult
    {
        public enum Kind
        {
            Void,
            TypeDef,
            Symbol,
            SymbolName,
            Tuple,
        }

        public ResolveTypeResult() { _Kind = Kind.Void; }
        public ResolveTypeResult(TypeDefinition def) { _Data = def; _Kind = Kind.TypeDef; }
        public ResolveTypeResult(TypeDefinition[] defs) { _Data = defs; _Kind = Kind.Tuple; }
        public ResolveTypeResult(Symbol sym) { _Data = sym; _Kind = Kind.Symbol; }
        public ResolveTypeResult(string sym) { _Data = sym; _Kind = Kind.SymbolName; }

        public Kind ValueKind { get { return _Kind; }}
        public TypeDefinition TypeDef { get { return (TypeDefinition)_Data; } }
        public Symbol Symbol { get { return (Symbol)_Data; } }
        public string SymbolName { get { return (string)_Data; } }

        Kind _Kind;
        object _Data;

        internal static ResolveTypeResult Void()
        {
            return new ResolveTypeResult();
        }
    }

    /// <summary>
     /// evaluates expression types
     /// infer types for variables and functions
     /// </summary>
    class ResolveTypePhase : PhaseVisitor<ResolveTypeResult>
    {
        TyVisitor _TyVisitor;

        public ResolveTypePhase(NameScopeMap scopes, ParseTreeDataStore data)
            : base(scopes, data)
        {
        }

        private ResolveTypeResult VisitItem_fn(IParseTree tree, oxideParser.Ident_or_genericContext genericContext, oxideParser.Fn_declarationContext fnContext, oxideParser.Code_blockContext codeBlockContext)
        {
            IEnumerable<TypeDefinition> genericParams = new TypeDefinition[0];

            if (genericContext != null)
            {
                // we already added template symbols to scope
                // now visit them and create type definitions
                _TyVisitor = new TyVisitor_GenericParam(_CurrentScope, _Data);

                if (genericContext.ty_list() != null)
                {
                    Visit(genericContext.ty_list());

                    genericParams = genericContext.ty_list().ty().Select(x => _Data.Get(x).Symbol.TypeDef);
                }

                if (genericContext.ty_variadic() != null)
                {
                    Visit(genericContext.ty_variadic());

                    var variadic = new TypeDefinition[1] { _Data.Get(genericContext.ty_variadic()).Symbol.TypeDef };
                    genericParams = genericParams.Concat(variadic);
                }
            }

            // now go through parameters and return values
            PushScope(tree);
            _TyVisitor = new TyVisitor_FnBody(_CurrentScope, _Data);
            Visit(fnContext);
            if (codeBlockContext != null)
                Visit(codeBlockContext);
            PopScope();

            // get actual parameter types
            TypeDefinition retType = null;
            if(fnContext.fn_return() != null)
            {
                retType = _Data.Get(fnContext.fn_return()).Symbol.TypeDef; 
            }
            else
            {
                retType = BuiltInTypes.Void.TypeDef;
            }

            // by this time we went through all child items and filled all types (stored in data)
            // so now we just have to get all fields and create Function type
            var funcSym = _Data.Get(tree).Symbol;

            var args = fnContext.parameter_declaration_list().parameter_declaration().Select(x => _Data.Get(x.ty()).Symbol.TypeDef);
            funcSym.TypeDef = FunctionTypeDefinition.Create(null, retType, args.ToArray(), genericParams.ToArray());

            return null;
        }

        public override ResolveTypeResult VisitBinding_handler(oxideParser.Binding_handlerContext context)
        {
            return VisitItem_fn(context, null, context.fn_declaration(), context.code_block());
        }

        public override ResolveTypeResult VisitItem_fn_implement(oxideParser.Item_fn_implementContext context)
        {
            return VisitItem_fn(context, context.ident_or_generic(), context.fn_declaration(), context.code_block());
        }

        /// <summary>
        /// called after Fn scope is defined to
        /// </summary>
        /// <param name="generic_declarationContext"></param>
        public override ResolveTypeResult VisitItem_fn_declare(oxideParser.Item_fn_declareContext context)
        {
            return VisitItem_fn(context, context.ident_or_generic(), context.fn_declaration(), null);
        }

        public override ResolveTypeResult VisitItem_struct(oxideParser.Item_structContext context)
        {
            var sym = _Data.Get(context).Symbol;
            if (sym == null)
                throw new InvalidOperationException("Type should be set");

            _TyVisitor = new TyVisitor_FnBody(_CurrentScope, _Data);
            if (context.struct_field_list() != null)
            {
                var fields = new List<StructTypeDefinition.Field>();
                foreach (var field in context.struct_field_list().struct_field())
                {
                    var resType = Visit(field.ty());
                    fields.Add(new StructTypeDefinition.Field(field.ID().GetText(), resType.Symbol.TypeDef));
                }

                sym.TypeDef = new StructTypeDefinition(sym.Name, fields.ToArray(), null);
            }
            else if (context.ty_list() != null)
            {
                throw new InvalidOperationException("Unknown grammar");
            }
            else
            {
                throw new InvalidOperationException("Unknown grammar");
            }
            _TyVisitor = null;

            return new ResolveTypeResult(sym);
        }

        public override ResolveTypeResult VisitStruct_field(oxideParser.Struct_fieldContext context)
        {
            var sym = _Data.Get(context.ty()).Symbol;
            if (sym == null)
                throw new InvalidOperationException("Type should be set");

            _TyVisitor = new TyVisitor_FnBody(_CurrentScope, _Data);
            base.VisitStruct_field(context);
            _TyVisitor = null;

            // symbol should be already resolved. Just set it on ID

            _Data.Get(context.ID()).Symbol = _Data.Get(context.ty()).Symbol;

            return new ResolveTypeResult();
        }

        public override ResolveTypeResult VisitLet_statement(oxideParser.Let_statementContext context)
        {
            if (context.ident() != null)
            {
                var src = Visit(context.assignment_expression());
                var sym = _CurrentScope.FindFirst(NameScope.FromContext(context.ident()), Symbol.Kind.Var);
                if (sym.TypeDef != null)
                {
                    throw ErrorHelper.InvalidExpression(context, "let used twide");
                }
                sym.TypeDef = src.TypeDef;
                _Data.Get(context).Symbol = sym;
                return ResolveTypeResult.Void();
            }
            else
            {
                // tuple code
                throw ErrorHelper.NotImplemented(context);
            }
        }

        public override ResolveTypeResult VisitAssignment_expression(oxideParser.Assignment_expressionContext context)
        {
            if (context.assignment_expression() != null)
            {
                var dest = Visit(context.unary_expression());
                var src = Visit(context.assignment_expression());

                var funcSym = _CurrentScope.FindFunction("__assign", new TypeDefinition[] { dest.TypeDef, src.TypeDef });

                if (dest.ValueKind == ResolveTypeResult.Kind.Symbol)
                {
                    _Data.Get(context).Symbol = dest.Symbol;
                }
                else if(dest.ValueKind == ResolveTypeResult.Kind.SymbolName)
                {
                    throw new InvalidOperationException("Internal error. Symbol should be resolved be inner code");
                }

                return new ResolveTypeResult(funcSym.AsFunction.TypeDef.Result);
            }
            return base.VisitAssignment_expression(context);
        }

        public override ResolveTypeResult VisitConditional_expression(oxideParser.Conditional_expressionContext context)
        {
            // context.
            return base.VisitConditional_expression(context);
        }

        public override ResolveTypeResult VisitLogical_or_expression(oxideParser.Logical_or_expressionContext context)
        {
            return base.VisitLogical_or_expression(context);
        }

        public override ResolveTypeResult VisitLogical_and_expression(oxideParser.Logical_and_expressionContext context)
        {
            return base.VisitLogical_and_expression(context);
        }

        public override ResolveTypeResult VisitInclusive_or_expression(oxideParser.Inclusive_or_expressionContext context)
        {
            return base.VisitInclusive_or_expression(context);
        }

        public override ResolveTypeResult VisitAdditive_expression(oxideParser.Additive_expressionContext context)
        {
            if (context.additive_expression() != null)
            {
                var o1 = Visit(context.additive_expression());
                var o2 = Visit(context.multiplicative_expression());

                // now lookup plus operator of specific type
                string op;
                switch (context.op.Type)
                {
                    case oxideLexer.PLUS:
                        op = "__add";
                        break;
                    case oxideLexer.MINUS:
                        op = "__sub";
                        break;
                    default:
                        throw ErrorHelper.InvalidToken(context);
                }
                var funcDef = _CurrentScope.FindFunction(op, new TypeDefinition[] { o1.TypeDef, o2.TypeDef });
                _Data.Get(context).Symbol = funcDef;
                return new ResolveTypeResult(funcDef.AsFunction.TypeDef.Result);
            }
            return base.VisitAdditive_expression(context);
        }

        public override ResolveTypeResult VisitMultiplicative_expression(oxideParser.Multiplicative_expressionContext context)
        {
            if (context.multiplicative_expression() != null)
            {
                var o1 = Visit(context.multiplicative_expression());
                var o2 = Visit(context.cast_expression());

                // now lookup plus operator of specific type
                string op;
                switch (context.op.Type)
                {
                    case oxideLexer.TIMES:
                        op = "__mul";
                        break;
                    case oxideLexer.DIVIDE:
                        op = "__div";
                        break;
                    case oxideLexer.MOD:
                        op = "__mod";
                        break;
                    default:
                        throw ErrorHelper.InvalidToken(context);
                }

                var funcDef = _CurrentScope.FindFunction(op, new TypeDefinition[] { o1.TypeDef, o2.TypeDef });
                _Data.Get(context).Symbol = funcDef;
                return new ResolveTypeResult(funcDef.AsFunction.TypeDef.Result);
            }

            return base.VisitMultiplicative_expression(context);
        }

        public override ResolveTypeResult VisitUnary_expression(oxideParser.Unary_expressionContext context)
        {
            if (context.unary_expression() != null)
            {
                var o1 = Visit(context.unary_expression());

                // now lookup plus operator of specific type
                string op;
                switch (context.op.Type)
                {
                    case oxideLexer.MINUSMINUS:
                        op = "__dec_pref";
                        break;
                    case oxideLexer.PLUSPLUS:
                        op = "__inc_pref";
                        break;
                    default:
                        throw ErrorHelper.InvalidToken(context);
                }

                var funcDef = _CurrentScope.FindFunction(op, new TypeDefinition[] { o1.TypeDef });
                _Data.Get(context).Symbol = funcDef;
                return new ResolveTypeResult(funcDef.AsFunction.TypeDef.Result);
            }
            else if (context.unary_operator() != null)
            {
                var o1 = Visit(context.cast_expression());

                string op;
                switch (context.unary_operator().op.Type)
                {
                    case oxideLexer.MINUS:
                        op = "__unary_minus";
                        break;
                    default:
                        throw ErrorHelper.InvalidToken(context);
                }

                var funcDef = _CurrentScope.FindFunction(op, new TypeDefinition[] { o1.TypeDef });
                _Data.Get(context).Symbol = funcDef;
                return new ResolveTypeResult(funcDef.AsFunction.TypeDef.Result);
            }

            return base.VisitUnary_expression(context);
        }

        public override ResolveTypeResult VisitPostfix_expression_primary(oxideParser.Postfix_expression_primaryContext context)
        {
            var priRes = Visit(context.primary_expression());

            if (priRes.ValueKind == ResolveTypeResult.Kind.SymbolName)
            {
                // at this point we do not know if symbol is variable or function
                // functions can have the same signature from variables. 
                // scope will ensure that variable gets priority
                var sym = _CurrentScope.FindFirst(priRes.SymbolName, x => true);
                if (sym.IsVar)
                {
                    _Data.Get(context).Symbol = sym;
                    return new ResolveTypeResult(sym.TypeDef);
                }
                else
                {
                    return priRes;
                }
            }
            else if (priRes.ValueKind == ResolveTypeResult.Kind.Symbol)
            {
                _Data.Get(context).Symbol = priRes.Symbol;
                return new ResolveTypeResult(priRes.Symbol.TypeDef);
            }
            else
            {
                return priRes;
            }
        }

        public override ResolveTypeResult VisitPostfix_expression_bracket(oxideParser.Postfix_expression_bracketContext context)
        {
            // get name (symbol) and index from expression
            var name = Visit(context.postfix_expression());
            var index = Visit(context.expression());
            return base.VisitPostfix_expression_bracket(context);
        }

        public override ResolveTypeResult VisitPostfix_expression_paren(oxideParser.Postfix_expression_parenContext context)
        {
            // get name (symbol) and parameters
            // there are two cases here. either first expression is a name in which case we lookup symbols
            // or it is a function reference in which case the type is pre-defined
            var nameRes = Visit(context.postfix_expression());
            TypeDefinition[] args = null;

            if (context.expression_list() != null)
            {
                args = context.expression_list().expression().Select(x => Visit(x).TypeDef).ToArray();
            }

            // TODO. handle reference case
            if(nameRes.ValueKind == ResolveTypeResult.Kind.SymbolName)
            {
                var sym = _CurrentScope.FindFunction(nameRes.SymbolName, args);
                _Data.Get(context).Symbol = sym;
                return new ResolveTypeResult(sym.TypeDef);
            }
            else
            {
                throw new InvalidOperationException("Must be function");
            }
        }

        /// <summary>
        /// Create symbols representing constant
        /// TODO. string constants should be registered in symbol table to get exposed in package
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public override ResolveTypeResult VisitConstant(oxideParser.ConstantContext context)
        {
            if (context.Start.Type == oxideLexer.IntegerConstant)
            {
                return new ResolveTypeResult(new ConstantSymbol(int.Parse(context.GetText()), BuiltInTypes.Int32.TypeDef));
            }
            else if (context.Start.Type == oxideLexer.FloatingConstant)
            {
                return new ResolveTypeResult(new ConstantSymbol(double.Parse(context.GetText()), BuiltInTypes.Double.TypeDef));
            }
            else if (context.Start.Type == oxideLexer.StringConstant)
            {
                return new ResolveTypeResult(new ConstantSymbol(context.GetText(), BuiltInTypes.String.TypeDef));
            }
            else
            {
                throw new InvalidOperationException("unknown syntax");
            }
        }

        public override ResolveTypeResult VisitPrimary_expression_ident(oxideParser.Primary_expression_identContext context)
        {
            string symName = NameScope.FromContext(context.ident_or_generic());
            return new ResolveTypeResult(symName);
        }

        public override ResolveTypeResult VisitPrimary_expression_paren(oxideParser.Primary_expression_parenContext context)
        {
            return Visit(context.expression());
        }

        public override ResolveTypeResult VisitPrimary_expression_tuple(oxideParser.Primary_expression_tupleContext context)
        {
            TypeDefinition[] argsType = new TypeDefinition[context.expression().Count];
            int idx = 0;

            foreach (var expr in context.expression())
            {
                argsType[idx++] = Visit(expr).TypeDef;
            }

            if (argsType.Length == 1)
            {
                return new ResolveTypeResult(argsType[0]);
            }
            else
            {
                return new ResolveTypeResult(argsType);
            }
        }
        public override ResolveTypeResult VisitPrimary_expression_tuple_single(oxideParser.Primary_expression_tuple_singleContext context)
        {
            TypeDefinition[] argsType = new TypeDefinition[1];
            argsType[0] = Visit(context.expression()).TypeDef;
            return new ResolveTypeResult(argsType);
        }

        public override ResolveTypeResult VisitTy_id(oxideParser.Ty_idContext context)
        {
            return _TyVisitor.VisitTy_id(context);
        }

        public override ResolveTypeResult VisitTy_generic(oxideParser.Ty_genericContext context)
        {
            return _TyVisitor.VisitTy_generic(context);
        }

        public override ResolveTypeResult VisitTy_variadic(oxideParser.Ty_variadicContext context)
        {
            return _TyVisitor.VisitTy_variadic(context);
        }
    }
}
