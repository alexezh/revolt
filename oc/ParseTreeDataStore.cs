using Antlr4.Runtime.Tree;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace oc
{
    public class ParseTreeDataStore
    {
        ParseTreeProperty<Item> _Data = new ParseTreeProperty<Item>();

        public class Item
        {
            public Symbol Symbol;
        }

        public Item Get(IParseTree node)
        {
            var item = _Data.Get(node);
            if (item != null)
                return item;

            item = new Item();
            _Data.Put(node, item);
            return item;
        }
    }
}
