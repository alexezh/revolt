﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace oc
{

    class BuiltInTypes
    {
        static public Symbol Void;
        static public Symbol Boolean;
        static public Symbol Int32;
        static public Symbol Int64;
        static public Symbol Double;
        static public Symbol String;

        public static void Populate(NameScope scope)
        {
            Void = AddPrimitiveType(scope, "void", PrimitiveTypeDefinition.Kind.Void);
            Boolean = AddPrimitiveType(scope, "bool", PrimitiveTypeDefinition.Kind.Boolean);
            Int32 = AddPrimitiveType(scope, "int32", PrimitiveTypeDefinition.Kind.Int32);
            Int64 = AddPrimitiveType(scope, "int64", PrimitiveTypeDefinition.Kind.Int64);
            Double = AddPrimitiveType(scope, "double", PrimitiveTypeDefinition.Kind.Double);
            String = AddPrimitiveType(scope, "string", PrimitiveTypeDefinition.Kind.String);

            AddFunction(scope, "__add", "int32", "int32;int32", new IntrinsicImplementation(AsmOpKind.Add));
            AddFunction(scope, "__add", "int64", "int64;int64", new IntrinsicImplementation(AsmOpKind.Add));
            AddFunction(scope, "__add", "double", "double;double", new IntrinsicImplementation(AsmOpKind.Add));

            AddFunction(scope, "__sub", "int32", "int32;int32", new IntrinsicImplementation(AsmOpKind.Sub));
            AddFunction(scope, "__sub", "int64", "int64;int64", new IntrinsicImplementation(AsmOpKind.Sub));
            AddFunction(scope, "__sub", "double", "double;double", new IntrinsicImplementation(AsmOpKind.Sub));

            AddFunction(scope, "__mul", "int32", "int32;int32", new IntrinsicImplementation(AsmOpKind.Mul));
            AddFunction(scope, "__mul", "int64", "int64;int64", new IntrinsicImplementation(AsmOpKind.Mul));
            AddFunction(scope, "__mul", "double", "double;double", new IntrinsicImplementation(AsmOpKind.Mul));

            AddFunction(scope, "__div", "int32", "int32;int32", new IntrinsicImplementation(AsmOpKind.Div));
            AddFunction(scope, "__div", "int64", "int64;int64", new IntrinsicImplementation(AsmOpKind.Div));
            AddFunction(scope, "__div", "double", "double;double", new IntrinsicImplementation(AsmOpKind.Div));

            AddFunction(scope, "__unary_minus", "int32", "int32", new IntrinsicImplementation(AsmOpKind.UnaryMinus));
            AddFunction(scope, "__unary_minus", "int64", "int64", new IntrinsicImplementation(AsmOpKind.UnaryMinus));
            AddFunction(scope, "__unary_minus", "double", "double", new IntrinsicImplementation(AsmOpKind.UnaryMinus));

            AddGenericFunction(scope, "__assign", "T", "T;T");
        }

        private static Symbol AddPrimitiveType(NameScope scope, string s, PrimitiveTypeDefinition.Kind kind)
        {
            return scope.Add(new Symbol(s, Symbol.Kind.Type, true, new PrimitiveTypeDefinition(kind)));
        }

        private static void AddFunction(NameScope scope, string name, string ret, string args, FunctionImplementation impl = null)
        {
            var argsType = args.Split(';').Select(x => scope.FindFirst(x, Symbol.Kind.Type).TypeDef);

            var symFunc = scope.Add(new FunctionSymbol(name, false, 
                FunctionTypeDefinition.Create(null, scope.FindFirst(ret, Symbol.Kind.Type).TypeDef, argsType.ToArray(), null)));

            symFunc.Impl = impl;
        }
        private static void AddGenericFunction(NameScope scope, string name, string ret, string args)
        {
            scope.Add(new FunctionSymbol(name, false, FunctionTypeDefinition.CreateGeneric(null, ret, args)));
        }
    }
}
