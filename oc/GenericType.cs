﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace oc
{
    /// <summary>
    /// represents generic type possibly with contraints
    /// </summary>
    public class GenericType : TypeDefinition
    {
        public GenericType(string name, bool variadic = false)
        {
            _Name = name;
            _Variadic = variadic;
        }

        public override string Name
        {
            get
            {
                return _Name;
            }
        }
        public override string Signature
        {
            get
            {
                return _Name;
            }
        }
        public override bool IsObject { get { throw new InvalidOperationException("generic types not instantiated"); } }

        public override bool Equals(System.Object obj)
        {
            GenericType p = obj as GenericType;
            if ((object)p == null)
            {
                return false;
            }

            return Equals((GenericType)obj);
        }

        /// <summary>
        /// generic type is always equal (for now)
        /// </summary>
        /// <param name="p"></param>
        /// <returns></returns>
        public bool Equals(GenericType p)
        {
            return true;
        }

        public override JToken AsJson()
        {
            throw new NotImplementedException();
        }

        public bool Match(TypeDefinition other)
        {
            // without constrains, generic maps any type
            return true;
        }

        public bool Match(TypeDefinition[] other, int startIdx, int endIdx)
        {
            if (!_Variadic)
                throw new InvalidOperationException("only works for variadic");

            return true;
        }

        public bool Variadic
        {
            get { return _Variadic; }
        }
        string _Name;
        bool _Variadic;
    }

    class GenericTypeHelper
    {
        /// <summary>
        /// finds type in genericArgs. If type being substituted, returns substitute type otherwise returns generic
        /// </summary>
        /// <param name="src"></param>
        /// <param name="orig"></param>
        /// <param name="subst"></param>
        /// <returns></returns>
        public static TypeDefinition Substitute(GenericType src, GenericType[] genericArgs, TypeDefinition[] subst)
        {
            for (int i = 0; i < genericArgs.Length; i++)
            {
                if (src == genericArgs[i])
                {
                    return (i < subst.Length) ? subst[i] : src;
                }
            }

            throw new InvalidOperationException("Type not found");
        }

        /// <summary>
        /// resolve source types to dest
        /// returns array the same size as source types with result
        /// </summary>
        /// <param name="_Args"></param>
        /// <param name="types"></param>
        /// <returns></returns>
        internal static bool ResolveTypes(TypeDefinition[] src, TypeDefinition[] dest, out TypeDefinition[] result)
        {
            result = null;
            TypeDefinition[] innerResult = new TypeDefinition[src.Length];

            if (src.Length == 0 && dest.Length == 0)
                return true;

            if ((src.Length != 0) ^ (dest.Length != 0))
                return false;

            if (!ResolveSrcIndex(src, 0, dest, 0, dest.Length, ref innerResult))
                return false;

            result = innerResult;
            return true;
        }

        private static bool ResolveSrcIndex(TypeDefinition[] src, int srcIdx, TypeDefinition[] dest, int destStart, int destEnd, ref TypeDefinition[] result)
        {
            if(srcIdx == src.Length && destStart == destEnd)
                return true;

            if(srcIdx == src.Length && destStart != destEnd)
                return false;

            if(src[srcIdx] is GenericType)
            {
                var varSrc = src[srcIdx] as GenericType;
                if(varSrc.Variadic)
                {
                    for(int end = destEnd; end > destStart; end--)
                    {
                        if(!varSrc.Match(dest, destStart, end))
                        {
                            continue;
                        }
                        else
                        {
                            result[srcIdx] = new TupleTypeDefinition(dest, destStart, end);
                            if(ResolveSrcIndex(src, srcIdx+1, dest, end, destEnd, ref result))
                                return true;
                        }
                    }

                    return false;
                }
                else
                {
                    if(!varSrc.Match(dest[destStart]))
                    {
                        return false;
                    }

                    result[srcIdx] = dest[destStart];
                    return ResolveSrcIndex(src, srcIdx+1, dest, destStart+1, destEnd, ref result);
                }
            }
            else
            {
                if(!src[srcIdx].Equals(dest[destStart]))
                {
                    return false;
                }

                result[srcIdx] = dest[destStart];
                return ResolveSrcIndex(src, srcIdx+1, dest, destStart+1, destEnd, ref result);
            }
        }
    }
}
