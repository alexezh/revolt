﻿using Antlr4.Runtime.Tree;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace oc
{
    public class AsmGenResult
    {
        public AsmGenResult()
        { 
        }

        public AsmGenResult(StorageLocation location)
        {
            Data = location;
        }

        public AsmGenResult(Symbol symbol)
        {
            Data = symbol;
        }

        public StorageLocation Location
        {
            get { return Data as StorageLocation; }
        }
        public Symbol Symbol
        {
            get { return Data as Symbol; }
        }

        private object Data;
    }

    public class AsmGenPhase : PhaseVisitor<AsmGenResult>
    {
        private AsmVarAllocator _VarAlloc = new AsmVarAllocator();
        private string _FunctionName;
        private ScriptImplementation _FunctionImpl;

        public AsmGenPhase(NameScopeMap scopes, ParseTreeDataStore store)
            : base(scopes, store)
        {
        }

        public override AsmGenResult VisitBinding_handler(oxideParser.Binding_handlerContext context)
        {
            return ProcessFunction(_Data.Get(context).Symbol, context.fn_declaration(), context.code_block());
        }

        public override AsmGenResult VisitItem_fn_implement(oxideParser.Item_fn_implementContext context)
        {
            return ProcessFunction(_Data.Get(context).Symbol, context.fn_declaration(), context.code_block());
        }

        private AsmGenResult ProcessFunction(Symbol sym, oxideParser.Fn_declarationContext fn_declarationContext, oxideParser.Code_blockContext code_blockContext)
        {
            if (sym.AsFunction.Impl != null)
                throw new InvalidOperationException("Internal error. Should be null");

            _FunctionImpl = new ScriptImplementation(sym.Name);
            _FunctionImpl.Add(AsmOp.MakeFunction(sym.Name));
            Visit(fn_declarationContext);
            Visit(code_blockContext);
            if (sym.AsFunction.TypeDef.Result.Equals(BuiltInTypes.Void.TypeDef))
            {
                _VarAlloc.FreeAllLocals(_FunctionImpl);
                _FunctionImpl.Add(AsmOp.MakeReturn());
            }
            sym.AsFunction.Impl = _FunctionImpl;
            _FunctionImpl = null;
            return new AsmGenResult();
        }

        public override AsmGenResult VisitReturn_statement(oxideParser.Return_statementContext context)
        {
            AsmGenResult res = null;
            if(context.expression() != null)
            {
                res = Visit(context.expression());
            }

            _VarAlloc.FreeAllLocals(_FunctionImpl);
            _FunctionImpl.Add(AsmOp.MakeReturn((res != null) ? res.Location : null));
            return new AsmGenResult();
        }

        public override AsmGenResult VisitSelection_statement(oxideParser.Selection_statementContext context)
        {
            base.VisitSelection_statement(context);
            var res = Visit(context.expression());
            var labelElse = _FunctionImpl.AllocLabel(_FunctionName);
            var labelEnd = _FunctionImpl.AllocLabel(_FunctionName);
            _FunctionImpl.Add(AsmOp.Make(AsmOpKind.Br, res, labelElse));
            Visit(context.branch_true);
            // if we have else, insert else block
            if(context.branch_false != null)
            {
                _FunctionImpl.Add(AsmOp.Make(AsmOpKind.Jump, labelEnd));
                _FunctionImpl.Add(AsmOp.Make(AsmOpKind.Label, labelElse + ':'));
                Visit(context.branch_false);
                _FunctionImpl.Add(AsmOp.Make(AsmOpKind.Label, labelEnd + ':'));
            }
            else
            {
                _FunctionImpl.Add(AsmOp.Make(AsmOpKind.Label, labelElse + ':'));
            }
            _VarAlloc.FreeTemp(_FunctionImpl, res.Location);
            return null;
        }

        public override AsmGenResult VisitAssignment_expression(oxideParser.Assignment_expressionContext context)
        {
            if (context.assignment_expression() != null)
            {
                var dest = Visit(context.unary_expression());
                var src = Visit(context.assignment_expression());

                if (dest.Location.LocType == LocationType.Constant)
                    throw new InvalidOperationException("Target location must be variable");

                _FunctionImpl.Add(AsmOp.Make(AsmOpKind.Store, dest, src));

                _VarAlloc.FreeTemp(_FunctionImpl, src.Location);
                _VarAlloc.FreeTemp(_FunctionImpl, dest.Location);

                return new AsmGenResult();
            }
            return base.VisitAssignment_expression(context);
        }

        public override AsmGenResult VisitConditional_expression(oxideParser.Conditional_expressionContext context)
        {
            // context.
            return base.VisitConditional_expression(context);
        }

        public override AsmGenResult VisitLogical_or_expression(oxideParser.Logical_or_expressionContext context)
        {
            if (context.logical_or_expression() != null)
            {
                return ProcessBinaryExpression(context, context.logical_or_expression(), context.logical_and_expression());
            }
            return base.VisitLogical_or_expression(context);
        }

        public override AsmGenResult VisitLogical_and_expression(oxideParser.Logical_and_expressionContext context)
        {
            if (context.logical_and_expression() != null)
            {
                return ProcessBinaryExpression(context, context.logical_and_expression(), context.inclusive_or_expression());
            }
            return base.VisitLogical_and_expression(context);
        }

        public override AsmGenResult VisitInclusive_or_expression(oxideParser.Inclusive_or_expressionContext context)
        {
            if (context.inclusive_or_expression() != null)
            {
                return ProcessBinaryExpression(context, context.inclusive_or_expression(), context.exclusive_or_expression());
            }
            return base.VisitInclusive_or_expression(context);
        }

        public override AsmGenResult VisitExclusive_or_expression(oxideParser.Exclusive_or_expressionContext context)
        {
            if (context.exclusive_or_expression() != null)
            {
                return ProcessBinaryExpression(context, context.exclusive_or_expression(), context.and_expression());
            }
            return base.VisitExclusive_or_expression(context);
        }

        public override AsmGenResult VisitAnd_expression(oxideParser.And_expressionContext context)
        {
 	         return base.VisitAnd_expression(context);
        }

        AsmGenResult ProcessBinaryExpression(IRuleNode context, IRuleNode t1, IRuleNode t2)
        {
            var sym = _Data.Get(context).Symbol;

            var r1 = Visit(t1).Location;
            var r2 = Visit(t2).Location;

            var rr = _VarAlloc.AllocVar(_FunctionImpl, sym.TypeDef);
            var funcSym = sym as FunctionSymbol;

            if (funcSym.Impl is IntrinsicImplementation)
            {
                _FunctionImpl.Add(AsmOp.Make(funcSym.Impl.AsIntrinsic.Kind, r1.QualifiedLocation, r2.QualifiedLocation, rr.QualifiedLocation));
            }
            else
            {
                throw new InvalidOperationException("Not implemented");
            }
            _VarAlloc.FreeTemp(_FunctionImpl, r1);
            _VarAlloc.FreeTemp(_FunctionImpl, r2);

            return new AsmGenResult(rr);
        }

        public override AsmGenResult VisitAdditive_expression(oxideParser.Additive_expressionContext context)
        {
            if (context.additive_expression() != null)
            {
                return ProcessBinaryExpression(context, context.additive_expression(), context.multiplicative_expression());
            }
            return base.VisitAdditive_expression(context);
        }

        public override AsmGenResult VisitMultiplicative_expression(oxideParser.Multiplicative_expressionContext context)
        {
            if (context.multiplicative_expression() != null)
            {
                return ProcessBinaryExpression(context, context.multiplicative_expression(), context.cast_expression());
            }

            return base.VisitMultiplicative_expression(context);
        }

        private delegate IRuleNode GetRuleNode();
        private AsmGenResult ProcessUnary(IRuleNode context, GetRuleNode getNode)
        {
            var sym = _Data.Get(context).Symbol as FunctionSymbol;
            var r1 = Visit(getNode()).Location;
            var rr = _VarAlloc.AllocVar(_FunctionImpl, sym.TypeDef);

            if (sym.Impl is IntrinsicImplementation)
            {
                _FunctionImpl.Add(AsmOp.Make(sym.Impl.AsIntrinsic.Kind, r1.QualifiedLocation, rr.QualifiedLocation));
            }
            else
            {
                throw new InvalidOperationException("Not implemented");
            }
            _VarAlloc.FreeTemp(_FunctionImpl, r1);

            return new AsmGenResult(rr);
        }

        public override AsmGenResult VisitUnary_expression(oxideParser.Unary_expressionContext context)
        {
            if (context.unary_expression() != null)
            {
                return ProcessUnary(context, () => context.unary_expression());
            }
            else if (context.unary_operator() != null)
            {
                return ProcessUnary(context, () => context.cast_expression());
            }

            return base.VisitUnary_expression(context);
        }

        public override AsmGenResult VisitPostfix_expression_paren(oxideParser.Postfix_expression_parenContext context)
        {
            var sym = _Data.Get(context).Symbol;

            if (sym == null)
            {
                throw new NotImplementedException("Handle case when we return function object");
            }

            StorageLocation[] locs = null;

            if (context.expression_list() != null)
            {
                locs = context.expression_list().expression().Select(x => Visit(x).Location).ToArray();
            }

            var varRes = _VarAlloc.AllocAcc(_FunctionImpl, sym.TypeDef);
            _FunctionImpl.Add(AsmOp.MakeCall(sym, locs));

            return new AsmGenResult(varRes);
        }

        public override AsmGenResult VisitLet_statement(oxideParser.Let_statementContext context)
        {
            var varSym = _Data.Get(context).Symbol;
            var val = Visit(context.assignment_expression());

            var varLoc = _VarAlloc.AllocVar(_FunctionImpl, varSym.TypeDef, false, varSym.Name);
            //self.varScopes[len(self.varScopes)-1].append(rv)

            _FunctionImpl.Add(AsmOp.MakeStoreOp(varSym.TypeDef, varLoc, val.Location, true));
            _VarAlloc.FreeTemp(_FunctionImpl, val.Location);

 	        return new AsmGenResult(varLoc);
        }

        public override AsmGenResult VisitPostfix_expression_primary(oxideParser.Postfix_expression_primaryContext context)
        {
            var sym = _Data.Get(context).Symbol;

            // if expression is symbol, we just create load operation
            // otherwise we eval children and return the result
            if (sym != null)
            {
                if (sym is VariableSymbol)
                {
                    var varSym = sym as VariableSymbol;
                    if (varSym.VarKind == VariableSymbol.VariableKind.Local || varSym.VarKind == VariableSymbol.VariableKind.Param)
                    {
                        return new AsmGenResult(_VarAlloc.FindVar(varSym.Name));
                    }
                    else if (varSym.VarKind == VariableSymbol.VariableKind.Global)
                    {
                        // return StorageLocation(LocationType.Global, location=ast.symbol.name)
                        throw new InvalidOperationException("Unsupported");
                    }
                    else
                    {
                        throw new InvalidOperationException("Unsupported");
                    }
                }
                else if (sym is ConstantSymbol)
                {
                    var constSym = sym as ConstantSymbol;
                    return new AsmGenResult(new StorageLocation(LocationType.Constant, constSym.AsString()));
                }
                else
                {
                    throw new InvalidOperationException("Unsupported");
                }
            }
            else
            {
                return Visit(context.primary_expression());
            }
        }

        public override AsmGenResult VisitPrimary_expression_paren(oxideParser.Primary_expression_parenContext context)
        {
            return base.Visit(context.expression());
        }

        public override AsmGenResult VisitPrimary_expression_constant(oxideParser.Primary_expression_constantContext context)
        {
            if (context.constant().Start.Type == oxideLexer.IntegerConstant)
            {
                return new AsmGenResult(new StorageLocation(LocationType.Constant, context.constant().GetText()));
            }
            else if (context.constant().Start.Type == oxideLexer.FloatingConstant)
            {
                return new AsmGenResult(new StorageLocation(LocationType.Constant, context.constant().GetText()));
            }
            else if (context.constant().Start.Type == oxideLexer.StringConstant)
            {
                return new AsmGenResult(new StorageLocation(LocationType.Constant, context.constant().GetText()));
            }
            else
            {
                throw new InvalidOperationException("unknown syntax");
            }
        }
    }
}
