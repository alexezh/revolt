﻿using Antlr4.Runtime.Tree;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace oc
{
    public class ParseTreeDataStore
    {
        ParseTreeProperty<Item> _Data = new ParseTreeProperty<Item>();

        public class Item
        {
            public Symbol Symbol;
        }

        public Item Get(IParseTree node)
        {
            var item = _Data.Get(node);
            if (item != null)
                return item;

            item = new Item();
            _Data.Put(node, item);
            return item;
        }
    }
    /*

        /// <summary>
        /// resolve all symbol names
        /// attaches symbol object for every ident
        /// </summary>
        class ResolveSymbolsPhase : PhaseVisitor<object>
        {
            public ResolveSymbolsPhase(NameScopeMap scopes, ParseTreeDataStore data)
                : base(scopes, data)
            {
            }

            public override object VisitStruct_field(oxideParser.Struct_fieldContext context)
            {
                base.VisitStruct_field(context);

                // symbol should be already resolved. Just set it on ID
                var sym = _Data.Get(context.ty()).Symbol;
                if(sym == null)
                    throw new InvalidOperationException("Type should be set");

                _Data.Get(context.ID()).Symbol = _Data.Get(context.ty()).Symbol;

                return null;
            }

            public override object VisitTy_id(oxideParser.Ty_idContext context)
            {
                _Data.Get(context).Symbol = _CurrentScope.FindFirst(context.GetText(), Symbol.Kind.Type);
                return null;
            }

            public override object VisitPostfix_expression_bracket(oxideParser.Postfix_expression_bracketContext context)
            {
                // get name (symbol) and index from expression
                var name = VisitChildren(context.postfix_expression());
                var index = VisitChildren(context.expression());
                return null;
            }

            public override object VisitPostfix_expression_paren(oxideParser.Postfix_expression_parenContext context)
            {
                // get name (symbol) and parameters
                if(context.)
                // there are two cases here. either first expression is a name in which case we lookup symbols
                // or it is a function reference in which case the type is pre-defined
                var nameRes = VisitChildren(context.postfix_expression());
                TypeDefinition[] args = null;

                if (context.expression_list() != null)
                {
                    args = context.expression_list().expression().Select(x => VisitChildren(x).TypeDef).ToArray();
                }

                if (nameRes.ValueKind == ResolveTypeResult.Kind.TypeDef)
                {
                    throw new InvalidOperationException("Must be function");
                }
                else if (nameRes.ValueKind == ResolveTypeResult.Kind.Symbol)
                {
                    return new ResolveTypeResult(_CurrentScope.FindFunction(nameRes.Symbol, args).First().TypeDef);
                }
                else
                {
                    throw new InvalidOperationException("Must be function");
                }
            }

            public override ResolveTypeResult VisitPrimary_expression_ident(oxideParser.Primary_expression_identContext context)
            {
                string sym = NameScope.FromContext(context.ident_or_generic());
                _Data.Get(context).Symbol = _CurrentScope.FindFirst(sym, Symbol.Kind.Var);

                return null;
            }


            public override void EnterStruct_field(oxideParser.Struct_fieldContext context)
            {
                var sym = _CurrentScope.FindFirst(NameScope.FromContext(context.ty()), Symbol.Kind.Type);
                _Data.Get(context).Symbol = sym;
                base.EnterStruct_field(context);
            }

            public override void EnterIdent_or_generic(oxideParser.Ident_or_genericContext context)
            {
                var sym = _CurrentScope.FindFirst(NameScope.FromContext(context), x => x == Symbol.Kind.Function || x == Symbol.Kind.Var);
                _Data.Get(context).Symbol = sym;

                base.EnterIdent_or_generic(context);
            }
 
            public override void EnterIdent(oxideParser.IdentContext context)
            {
                var sym = _CurrentScope.Get(NameScope.FromContext(context));
                _Data.Get(context).Symbol = sym;
                base.EnterIdent(context);
            }
  
        }

    */
}
