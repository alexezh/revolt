﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace oc
{
    /// <summary>
    /// tuple type; list of types without name
    /// </summary>
    public class TupleTypeDefinition : TypeDefinition
    {
        public TupleTypeDefinition(TypeDefinition[] src, int start, int end)
        {
            _Parts = new TypeDefinition[end - start];
            Array.Copy(src, start, _Parts, 0, end - start);
        }

        public override string Name
        {
            get
            {
                return string.Format("({0})", string.Join(",", _Parts.Select(x => x.Name)));
            }
        }
        public override string Signature
        {
            get
            {
                return Name;
            }
        }
        public override bool IsObject { get { return true; } }

        public override JToken AsJson()
        {
            return new JObject(new JProperty("tuple",
                new JArray(_Parts.Select(x => x.AsJson()))));
        }

        public override bool Equals(System.Object obj)
        {
            TupleTypeDefinition p = obj as TupleTypeDefinition;
            if ((object)p == null)
            {
                return false;
            }

            return Equals((TupleTypeDefinition)obj);
        }

        public bool Equals(TupleTypeDefinition p)
        {
            return ArrayHelper.Equals(_Parts, p._Parts);
        }

        TypeDefinition[] _Parts;
    }
}
