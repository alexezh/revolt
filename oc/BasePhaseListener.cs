﻿using Antlr4.Runtime.Tree;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace oc
{
    class BasePhaseListener : oxideBaseListener
    {
        NameScopeMap _Scopes;
        protected NameScope _CurrentScope;

        private void PushScope(IParseTree tree)
        {
            _CurrentScope = _Scopes.Get(tree);
            if (_CurrentScope == null)
                throw new InvalidOperationException("Scope must be defined");
        }
        private void PopScope()
        {
            _CurrentScope = _CurrentScope.Parent;
        }

        public BasePhaseListener(NameScopeMap scopes)
        {
            _Scopes = scopes;
        }

        public override void EnterModule(oxideParser.ModuleContext context)
        {
            PushScope(context);
            base.EnterModule(context);
        }

        public override void ExitModule(oxideParser.ModuleContext context)
        {
            base.ExitModule(context);
            PopScope();
        }

        public override void EnterItem_binding(oxideParser.Item_bindingContext context)
        {
            PushScope(context);
            base.EnterItem_binding(context);
        }

        public override void ExitItem_binding(oxideParser.Item_bindingContext context)
        {
            base.ExitItem_binding(context);
            PopScope();
        }

        public override void EnterItem_fn_declare(oxideParser.Item_fn_declareContext context)
        {
 	         base.EnterItem_fn_declare(context);
        }

        public override void ExitItem_fn_implement(oxideParser.Item_fn_implementContext context)
        {
            base.ExitItem_fn_implement(context);
            PopScope();
        }
    }
}
