﻿using Antlr4.Runtime.Tree;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace oc
{
    public class PhaseVisitor<T> : oxideBaseVisitor<T>
    {
        protected ParseTreeDataStore _Data;
        protected NameScopeMap _Scopes;
        protected NameScope _CurrentScope;

        public PhaseVisitor(NameScopeMap scopes, ParseTreeDataStore data)
        {
            _Data = data;
            _Scopes = scopes;
            _CurrentScope = _Scopes.Root;
        }

        protected void PushScope(IParseTree tree)
        {
            _CurrentScope = _Scopes.Get(tree);
            if (_CurrentScope == null)
                throw new InvalidOperationException("Scope must be defined");
        }
        protected void PopScope()
        {
            _CurrentScope = _CurrentScope.Parent;
        }

        public override T VisitModule(oxideParser.ModuleContext context)
        {
            PushScope(context);
            var ret = base.VisitModule(context);
            PopScope();
            return ret;
        }

        public override T VisitItem_binding(oxideParser.Item_bindingContext context)
        {
            PushScope(context);
            var ret = base.VisitItem_binding(context);
            PopScope();
            return ret;
        }

        public override T VisitItem_fn_declare(oxideParser.Item_fn_declareContext context)
        {
            PushScope(context);
            var ret = base.VisitItem_fn_declare(context);
            PopScope();
            return ret;
        }

        public override T VisitItem_fn_implement(oxideParser.Item_fn_implementContext context)
        {
            PushScope(context);
            var ret = base.VisitItem_fn_implement(context);
            PopScope();
            return ret;
        }
    }

}
