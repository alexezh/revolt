﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace oc
{
    public class Symbol
    {
        public Symbol(string name, Kind kind, bool pub, TypeDefinition typeDef = null)
        {
            _Kind = kind;
            _Pub = pub;
            Name = name;
            _TypeDef = typeDef;
        }

        public enum Kind
        {
            Type,
            Function,
            Var,
            Const,
            Namespace
        }

        public override int GetHashCode()
        {
            return Name.GetHashCode() ^ _Kind.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            Symbol s = (Symbol)obj;
            return Name.Equals(s.Name);
        }

        public TypeDefinition TypeDef
        {
            get { return _TypeDef;  }
            set { _TypeDef = value; }
        }

        public FunctionSymbol AsFunction
        {
            get
            {
                return (FunctionSymbol)this;
            }
        }

        public Kind SymbolKind
        {
            get { return _Kind; }
        }

        public bool IsPublic
        {
            get
            {
                return _Pub;
            }
        }

        public bool IsFunction
        {
            get
            {
                return _Kind == Kind.Function;
            }
        }

        public bool IsType
        {
            get
            {
                return _Kind == Kind.Type;
            }
        }

        internal bool IsVar
        {
            get
            {
                return _Kind == Kind.Var;
            }
        }

        public override string ToString()
        {
            return Name + _TypeDef.ToString();
        }

        public string Name;
        private bool _Pub;
        /// <summary>
        /// indicates if symbol is used and should be included into compiled code
        /// </summary>
        private bool _Used;
        private Kind _Kind;
        private TypeDefinition _TypeDef;

        // TODO: add place to do single instancing for function templates
        // If code is the same, we can then use union to represent data types
        // If number of changes is small, the code then can stay the same
    }


        /*
        public FunctionDefinition LookupDefintion(TypeSymbol[] args)
        {
            foreach (var def in Defs)
            {
                bool argsMatch = true;
                if (def.Args == null && args == null)
                {
                    break;
                }
                else
                {
                    if (def.Args == null || args == null)
                        continue;

                    if (def.Args.Length != args.Length)
                        continue;

                    for (int i = 0; i < def.Args.Length; i++)
                    {
                        if (!def.Args[i].Equals(args[i]))
                        {
                            argsMatch = false;
                            break;
                        }
                    }

                    if (argsMatch)
                    {
                        return def;
                    }
                }
            }

            return null;
        }

        internal static TypeDefinition LookupDefinition(Symbol symbol, TypeDefinition[] args)
        {
            var func = symbol as FunctionSymbol;
            if (func == null)
                throw new InvalidOperationException("symbol is not function");

            return func.LookupDefintion(args);
        }

        public static void VerifyDefinition(FunctionDefinition func, TypeDefinition[] args)
        {
            if (func.Args.Length != args.Length)
                throw new InvalidOperationException(string.Format("Invalid number of args. Provided {0}, expected {1}", args.Length, func.Args.Length));

            for (int i = 0; i < func.Args.Length; i++)
            {
                if (!func.Args[i].Equals(args[i]))
                    throw new InvalidOperationException(string.Format("Invalid parameter {0}. Provided {0}, expected {1}", i, args.Length, func.Args.Length));
            }
        }
        */

    public class NamespaceSymbol : Symbol
    {
        public NamespaceSymbol(string name)
            : base(name, Symbol.Kind.Namespace, true)
        { 
        }
    }

    public class FunctionSymbol : Symbol
    {
        public FunctionSymbol(string name, bool pub, TypeDefinition typeDef = null)
            : base(name, Symbol.Kind.Function, pub, typeDef)
        { 
        }

        public FunctionTypeDefinition TypeDef
        {
            get
            {
                return (FunctionTypeDefinition)(base.TypeDef);
            }
        }

        public FunctionImplementation Impl;
    }

    class VariableSymbol : Symbol
    {
        public enum VariableKind
        {
            Local,
            Global,
            Param,
        }

        public VariableSymbol(string name, VariableKind varKind, bool pub, TypeDefinition typeDef = null)
            : base(name, Symbol.Kind.Var, pub, typeDef)
        {
            VarKind = varKind;
        }

        public VariableKind VarKind;
    }

    class ConstantSymbol : Symbol
    {
        public ConstantSymbol(Int32 data, TypeDefinition typeDef)
            : base(null, Symbol.Kind.Const, false, typeDef)
        {
            _Data = data;
        }
        public ConstantSymbol(Double data, TypeDefinition typeDef)
            : base(null, Symbol.Kind.Const, false, typeDef)
        {
            _Data = data;
        }
        public ConstantSymbol(string data, TypeDefinition typeDef)
            : base(null, Symbol.Kind.Const, false, typeDef)
        {
            _Data = data;
        }

        public string AsString()
        {
            if (_Data is Int32)
            {
                return ((Int32)_Data).ToString();
            }
            else if (_Data is Double)
            {
                return ((Double)_Data).ToString();
            }
            else
            {
                throw new InvalidOperationException("Not supported");
            }
        }

        public Int32 Int32
        {
            get
            {
                return (Int32)_Data;
            }
        }
        public Int32 String
        {
            get
            {
                return (Int32)_Data;
            }
        }
        private object _Data;
    }
}
