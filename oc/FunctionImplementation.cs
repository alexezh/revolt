﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace oc
{
    public abstract class FunctionImplementation
    {
        public IntrinsicImplementation AsIntrinsic
        {
            get { return this as IntrinsicImplementation; }
        }
        public ScriptImplementation AsModule
        {
            get { return this as ScriptImplementation; }
        }
    }

    public class ScriptImplementation : FunctionImplementation
    {
        public string Label;
        int _NextLabel = 0;
        List<AsmOp> _Ops = new List<AsmOp>();

        public ScriptImplementation(string label)
        {
            Label = label;
        }

        public void Add(AsmOp op)
        {
            _Ops.Add(op);
        }

        public string AllocLabel(string ct)
        {
            _NextLabel += 1;
            return Label + '_' + _NextLabel.ToString();
        }

        public string BodyAsString
        {
            get
            {
                StringBuilder bld = new StringBuilder();
                foreach (var op in _Ops)
                {
                    if (op.IsInternal)
                        continue;

                    bld.AppendLine(op.ToString());
                }
                return bld.ToString();
            }
        }

        internal void Print()
        {
            foreach (var op in _Ops)
            {
                Console.WriteLine(op.ToString());
            }
        }
    }

    public class IntrinsicImplementation : FunctionImplementation
    {
        public AsmOpKind Kind;
        private AsmOpKind asmOpKind;

        public IntrinsicImplementation(AsmOpKind kind)
        {
            Kind = kind;
        }
    }

}
