﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace oc
{
    public class SymbolStore
    {
        public IEnumerable<Symbol> Values
        {
            get
            {
                IEnumerable<Symbol> symbols = new List<Symbol>();
                foreach (var bucket in _Symbols.Values)
                {
                    symbols = symbols.Concat(bucket);
                }
                return symbols;
            }
        }

        public IEnumerable<Symbol> Find(string s)
        {
            List<Symbol> bucket = null;
            if (!_Symbols.TryGetValue(s, out bucket))
            {
                return null;
            }

            return bucket;
        }

        public void Add(IEnumerable<Symbol> symbols)
        {
            foreach (var sym in symbols)
            {
                List<Symbol> bucket;
                if (!_Symbols.TryGetValue(sym.Name, out bucket))
                {
                    _Symbols.Add(sym.Name, bucket = new List<Symbol>());
                }

                bucket.Add(sym);
            }
        }

        public Symbol Add(Symbol sym)
        {
            List<Symbol> bucket;
            if (!_Symbols.TryGetValue(sym.Name, out bucket))
            {
                _Symbols.Add(sym.Name, bucket = new List<Symbol>());
            }

            bucket.Add(sym);

            return sym;
        }

        private Dictionary<string, List<Symbol>> _Symbols = new Dictionary<string, List<Symbol>>();
    }
}
