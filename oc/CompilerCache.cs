﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace oc
{
    /// <summary>
    /// constains information which compiler needs from imported files
    /// </summary>
    public class SourceFile
    {
        public string FileName;
        public string Id; // hash of all inputs
        public List<string> Deps;
        public IEnumerable<Symbol> Public;
        public string Impl;

    }

    class CompilerCache
    {
        Dictionary<string, SourceFile> _Files = new Dictionary<string, SourceFile>();

        public SourceFile Get(string name)
        {
            return _Files[name];
        }

        public SourceFile TryGet(string name)
        {
            SourceFile file;

            if (!_Files.TryGetValue(name, out file))
            {
                return null;
            }

            return file;
        }

        public void Add(string name, SourceFile file)
        {
            _Files.Add(name, file);
        }
    }
}
