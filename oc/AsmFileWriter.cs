﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace oc
{
    class AsmFileWriter
    {
        static public JArray Write(NameScopeMap names)
        {
            JArray result;
            result = new JArray(
                    new JObject(
                        new JProperty("bindings", new JArray(GetBindings(names)))),
                    new JObject(
                        new JProperty("types", new JArray(GetTypes(names)))),
                    new JObject(
                        new JProperty("functions", new JArray(GetFunctions(names))))
                );

            return result;
        }

        private static IEnumerable<JObject> GetBindings(NameScopeMap names)
        {
            return names.All.Where(x => x.IsType).Select(x => new JObject());
        }
        private static IEnumerable<JToken> GetTypes(NameScopeMap names)
        {
            return names.All.Where(x => x.IsType && !(x.TypeDef is PrimitiveTypeDefinition)).Select(x => x.TypeDef.AsJson());
        }

        static JObject MakeFunctionObject(Symbol x)
        {
            ScriptImplementation impl = x.AsFunction.Impl as ScriptImplementation;

            return new JObject(
                new JProperty("label", impl.Label),
                new JProperty("body", impl.BodyAsString));
        }

        private static IEnumerable<JObject> GetFunctions(NameScopeMap names)
        {
            // NameScopeMap is symbol table
            // We have to flatten the table by using scoped names
            // 
            return names.All.Where(x => x.IsFunction && x.AsFunction.Impl is ScriptImplementation).Select(MakeFunctionObject);
        }
    }
}
