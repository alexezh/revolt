﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace oc
{
    class PublicPhase
    {
        public SymbolStore Exports = new SymbolStore();
        private NameScopeMap _Scopes;

        public PublicPhase(NameScopeMap scopes)
        {
            _Scopes = scopes;
        }

        public void Process()
        {
            foreach (var scope in _Scopes.Values)
            {
                foreach (var sym in scope.All)
                {
                    if(sym.IsPublic)
                    {
                        Exports.Add(sym);
                    }
                }
            }
        }
    }
}
