# README #

Revolt is fast strongly typed scripting language for signal processing scenarios. One of the key scenario is following. An application running on a device has to process a sequence of events. The processing algorithm is not known by the time when application ships. A typical processing can involve matching one event with another, extracting data from events etc. At the end of processing part of data is send to the server for further processing.

Key features of revolt language are

* C-style syntax
* Immutable strings and tuples
* Automatic memory management

# Current status. 
Under development.

# Installation
To run compiler you need python 3.4 with few additional packages. Run following command to install packages

```
pip3 install mako ply

```

To run VM tests you need VS2013 or recent XCode. Load corresponding projects and you should be good to go. C++ code does not have dependencies from any external libraries apart from CRT