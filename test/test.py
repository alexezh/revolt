import os
import glob
from subprocess import call

for rev in glob.glob( os.path.join('./', '*.rev') ):
	fileName, fileExtension = os.path.splitext(rev)
	cmd = ['python3', '../compiler/rev.py', rev, fileName + '.reva']
	print('process {0}'.format(' '.join(cmd)))
	if call(cmd) == 255:
		raise Exception('Failed to compile {0}'.format(fileName))
